-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2018 年 03 月 19 日 07:16
-- 服务器版本: 5.5.8
-- PHP 版本: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `zhaosheng`
--

-- --------------------------------------------------------

--
-- 表的结构 `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `last_login_ip` varchar(32) NOT NULL DEFAULT '',
  `last_login_time` varchar(32) NOT NULL DEFAULT '',
  `state` tinyint(4) NOT NULL DEFAULT '0',
  `group_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `admin`
--

INSERT INTO `admin` (`id`, `user_name`, `password`, `last_login_ip`, `last_login_time`, `state`, `group_id`) VALUES
(6, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '127.0.0.1', '1521427497', 0, 0),
(7, 'myadmin', 'e10adc3949ba59abbe56e057f20f883e', '127.0.0.1', '1434165947', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `admin_access`
--

CREATE TABLE IF NOT EXISTS `admin_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `module` varchar(50) DEFAULT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `admin_access`
--


-- --------------------------------------------------------

--
-- 表的结构 `admin_node`
--

CREATE TABLE IF NOT EXISTS `admin_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `sort` smallint(6) unsigned DEFAULT NULL,
  `pid` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `admin_node`
--


-- --------------------------------------------------------

--
-- 表的结构 `admin_role`
--

CREATE TABLE IF NOT EXISTS `admin_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  FULLTEXT KEY `remark` (`remark`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `admin_role`
--


-- --------------------------------------------------------

--
-- 表的结构 `admin_role_user`
--

CREATE TABLE IF NOT EXISTS `admin_role_user` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `admin_role_user`
--

INSERT INTO `admin_role_user` (`role_id`, `user_id`) VALUES
(0, '6'),
(0, '7');

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_sort` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `catalog` text NOT NULL,
  `description` text NOT NULL,
  `datetime` varchar(150) NOT NULL DEFAULT '',
  `positions` varchar(500) NOT NULL DEFAULT '',
  `siteid` varchar(255) NOT NULL DEFAULT '',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `hits_ips` text NOT NULL COMMENT '点击量IP',
  `favor` int(11) NOT NULL DEFAULT '0',
  `favor_ips` text NOT NULL COMMENT '点赞IP记录',
  `big_thumb` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`article_id`, `article_sort`, `title`, `keywords`, `content`, `catalog`, `description`, `datetime`, `positions`, `siteid`, `cat_id`, `hits`, `hits_ips`, `favor`, `favor_ips`, `big_thumb`) VALUES
(1, 1, 'Successfully attend the concrete show of Indonesia', '', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', 'a:0:{}', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', '1505310269', '', '1', 17, 1, 'a:1:{i:0;s:9:"127.0.0.1";}', 0, '', '/Public/uploads/pics/2017-09/59b936389940e.jpg'),
(2, 1, 'Successfully attend the concrete show of Indonesia', '', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', 'a:0:{}', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', '1505310269', '', '1', 17, 1, 'a:1:{i:0;s:9:"127.0.0.1";}', 0, '', '/Public/uploads/pics/2017-09/59b936389940e.jpg'),
(3, 1, 'Successfully attend the concrete show of Indonesia', '', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', 'a:0:{}', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', '1505310269', '', '1', 17, 0, '', 0, '', '/Public/uploads/pics/2017-09/59b936389940e.jpg'),
(4, 1, 'Successfully attend the concrete show of Indonesia', '', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', 'a:0:{}', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', '1505310269', '', '1', 17, 0, '', 0, '', '/Public/uploads/pics/2017-09/59b936389940e.jpg'),
(5, 1, 'Successfully attend the concrete show of Indonesia2 Successfully attend the concrete show of Indonesia2', '', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', 'a:0:{}', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes.', '1505355383', '', '1', 17, 0, '', 0, '', '/Public/uploads/pics/2017-09/59b936389940e.jpg'),
(6, 6, 'Company Profile', '', '<p>\r\n	<img src="/Public/uploads/pics/2017-09/59ba275791583.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<p style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;background-color:#FFFFFF;">\r\n		Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes in researching and manufacturing sodium lignosulfonate. With rich domestic marketing experience, Our products are hot sale in India, South Korea, Saudi Arabia, etc. Our hongfu brand sodium lignosulfonate HF-6, HF-7, HF-8 ,HF-10 have a good reputation among customers. In March, 2014, Foshan Junda Import &amp; Export Company Limited was established which is engaged in supplying more concrete admixture raw material to our customers around the world. The three main products we are trading and supplying are: sodium naphthalene sulphonate, sodium gluconate and polycarboxylate. We choose our vendors with professional technical team’s guiding. We have 4 technical engineers who specialize in construction chemical especially concrete admixture raw material. At the same time, they work for our concrete batching plant called Luohe ZhongJian Concrete Factory located in Henan province. Luohe ZhongJian Concrete factory engaged in producing ready-mix concrete. In September 2014, ZunXiang Luxury Car Sales and Service Center was established which is located in Baiyun, Guangzhou, Guangdong province.\r\n	</p>\r\n<br />\r\n<br />\r\n	<p style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:30px;background-color:#FFFFFF;">\r\n		We go for outstanding INNOVATION capabilities\r\n	</p>\r\n<br />\r\n<br />\r\n	<p style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;background-color:#FFFFFF;">\r\n		Safety is a fundamental value of our Group culture Sustainable development is fully integrated into our management and industrial processes Green conception of our solutions participating to more responsible construction modes Performance and consistency of our products all over the world\r\n	</p>\r\n</p>', 'a:0:{}', '', '1505372006', '', '1', 9, 0, '', 0, '', ''),
(7, 7, 'Corporate culture', '', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Corporate culture</span>', 'a:0:{}', '', '1505372185', '', '1', 10, 0, '', 0, '', ''),
(8, 8, 'Enterprise real show', '', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">Enterprise real show</span>', 'a:0:{}', '', '1505372548', '', '1', 11, 0, '', 0, '', ''),
(9, 9, 'Enterprise advantage', '', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">-Enterprise advantage</span>', 'a:0:{}', '', '1505372674', '', '1', 12, 0, '', 0, '', ''),
(10, 10, 'Maoming Hongfu Chemical Company Limited ', '', '<p>\r\n	<img src="/Public/uploads/pics/2017-09/59ba2c34cd8a6.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#F5F5F5;">Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes in researching and manufacturing sodium lignosulfonate. With rich domestic marketing experience, Our products are hot sale in India, South Korea,Saudi Arabia,etc.Our hongfu brand sodium lignosulfonate HF-6, HF-7, HF-8 ,HF-10 have a good reputation among customers. In March, 2014, Foshan Junda Import &amp; Export Company Limited.</span>\r\n</p>', 'a:0:{}', '', '1505373240', '', '1', 13, 1, 'a:1:{i:0;s:9:"127.0.0.1";}', 0, '', ''),
(11, 11, '联系方式1', '', '<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Foshan Office</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">:Room 706-707, Block 3, No. 15, Jihua No.6 Road,GreenLand Financial Centre, Chancheng District, Foshan, China.&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Zip</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">:528000&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Tel</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">:86-0757-83272552&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Mobile</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">: +86-18575782322 Contact: Mr. Jason Chen&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Mobile</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">: +86-18575782325 Contact: Ms. Paige Deng&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Email</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">: lignin@lhhfchem.com&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Factory add</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">：Middle Haihe Road, Luohe, Henan Province, China.</span>', 'a:0:{}', '', '1505378959', '', '1', 1, 0, '', 0, '', ''),
(12, 11, '联系方式2', '', '<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Foshan Office</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">:Room 706-707, Block 3, No. 15, Jihua No.6 Road,GreenLand Financial Centre, Chancheng District, Foshan, China.&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Zip</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">:528000&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Tel</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">:86-0757-83272552&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Mobile</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">: +86-18575782322 Contact: Mr. Jason Chen&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Mobile</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">: +86-18575782325 Contact: Ms. Paige Deng&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Email</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">: lignin@lhhfchem.com&nbsp;</span><br />\r\n<span style="font-weight:700;color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">Factory add</span><span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#FFFFFF;">：Middle Haihe Road, Luohe, Henan Province, China.</span>', 'a:0:{}', '', '1505378959', '', '1', 1, 0, '', 0, '', '');

-- --------------------------------------------------------

--
-- 表的结构 `article_link`
--

CREATE TABLE IF NOT EXISTS `article_link` (
  `article_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_link_sort` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `big_thumb` varchar(255) NOT NULL DEFAULT '',
  `href` varchar(255) NOT NULL DEFAULT '',
  `datetime` varchar(150) NOT NULL DEFAULT '',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `siteid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`article_link_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `article_link`
--

INSERT INTO `article_link` (`article_link_id`, `article_link_sort`, `title`, `big_thumb`, `href`, `datetime`, `cat_id`, `siteid`) VALUES
(11, 11, 'Calcium Lignosulfonate3', '/Public/uploads/pics/2017-09/59ba3b69bb095.jpg', 'https://www.baidu.com/', '1505377268', 20, '1'),
(10, 10, 'Calcium Lignosulfonate2', '/Public/uploads/pics/2017-09/59ba3b4deb1e7.jpg', 'https://www.baidu.com/', '1505377263', 20, '1'),
(9, 9, 'Calcium Lignosulfonate1', '/Public/uploads/pics/2017-09/59ba3b21a9f24.jpg', 'https://www.baidu.com/', '1505377257', 20, '1'),
(6, 6, 'english-公告', '', 'https://www.baidu.com/', '1505122005', 196, '2'),
(7, 7, 'english-年报', '', 'https://www.baidu.com/', '1505122098', 197, '2');

-- --------------------------------------------------------

--
-- 表的结构 `art_position`
--

CREATE TABLE IF NOT EXISTS `art_position` (
  `art_position_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_position_sort` int(11) NOT NULL DEFAULT '0',
  `pos_name` varchar(255) NOT NULL DEFAULT '',
  `siteid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`art_position_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `art_position`
--

INSERT INTO `art_position` (`art_position_id`, `art_position_sort`, `pos_name`, `siteid`) VALUES
(1, 2, '首页产品', '1');

-- --------------------------------------------------------

--
-- 表的结构 `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_sort` int(11) NOT NULL DEFAULT '0',
  `cat_name` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `is_show` tinyint(4) DEFAULT '0',
  `is_single` tinyint(4) DEFAULT '0',
  `parent_id` int(11) DEFAULT '0',
  `cate_tpl` text NOT NULL,
  `list_tpl` text NOT NULL,
  `show_tpl` text NOT NULL,
  `single_tpl` text NOT NULL,
  `siteid` varchar(255) NOT NULL DEFAULT '',
  `module_id` int(11) DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `sort` varchar(255) NOT NULL DEFAULT '',
  `eng_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `category`
--

INSERT INTO `category` (`category_id`, `category_sort`, `cat_name`, `thumb`, `is_show`, `is_single`, `parent_id`, `cate_tpl`, `list_tpl`, `show_tpl`, `single_tpl`, `siteid`, `module_id`, `title`, `keywords`, `description`, `sort`, `eng_name`) VALUES
(1, 10, 'Contact', '', 0, 0, 0, '', '', '', 'single_contact.html', '1', 13, '', '', '', '', ''),
(2, 3, 'Product', '', 0, 1, 0, '', 'list_product.html', '', '', '1', 20, '', '', '', '', ''),
(3, 2, 'News', '', 0, 1, 0, 'category_list_news.html', 'list_news.html', '', '', '1', 13, '', '', '', '', ''),
(4, 4, 'Product_desc', '', 1, 1, 0, '', 'list_show_product.html', '', '', '1', 20, '', '', '', '', ''),
(5, 5, 'News_desc', '', 1, 1, 0, '', 'list_show_news.html', '', '', '1', 0, '', '', '', '5', ''),
(6, 6, 'Subsidiaries', '', 1, 1, 0, '', 'list_subsidiaries.html', '', '', '1', 0, '', '', '', '', ''),
(7, 7, 'Career', '', 1, 1, 0, '', 'list_career.html', '', '', '1', 13, '', '', '', '', ''),
(8, 1, 'Group', '', 0, 0, 0, '', '', '', '', '1', 13, '', '', '', '', ''),
(9, 9, 'Company profile', '', 0, 0, 8, '', '', '', 'single_show_news.html', '1', 13, '', '', '', '', ''),
(10, 10, 'Corporate culture', '', 0, 0, 8, '', '', '', 'single_show_news.html', '1', 13, '', '', '', '', ''),
(11, 11, 'Enterprise real show', '', 0, 0, 8, '', '', '', 'single_show_news.html', '1', 13, '', '', '', '', ''),
(12, 12, 'Enterprise advantage', '', 0, 0, 8, '', '', '', 'single_show_news.html', '1', 13, '', '', '', '', ''),
(13, 13, 'Subsidiaries', '', 0, 1, 8, '', 'list_subsidiaries.html', 'show_list_subsidiaries.html', '', '1', 41, '', '', '', '', ''),
(14, 14, 'Career', '', 0, 1, 8, '', 'list_career.html', '', '', '1', 32, '', '', '', '', ''),
(15, 1, 'Sodium Lignosulfonate', '', 0, 1, 2, '', 'list_product.html', 'show_product.html', '', '1', 20, '', '', '', '', ''),
(16, 16, 'Calcium Lignosulfonate', '', 0, 1, 2, '', 'list_product.html', 'show_product.html', '', '1', 20, '', '', '', '', ''),
(17, 17, 'Business News', '', 0, 1, 3, '', 'list_news.html', 'show_news.html', '', '1', 13, '', '', '', '', ''),
(18, 18, 'Industry news', '', 0, 1, 3, '', 'list_news.html', 'show_news.html', '', '1', 13, '', '', '', '', ''),
(19, 19, 'Expo News', '', 0, 1, 3, 'category_list_news.html', 'list_news.html', 'show_news.html', '', '1', 13, '', '', '', '', ''),
(20, 20, '首页banner', '', 1, 1, 0, '', '', '', '', '1', 40, '', '', '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `cm_list`
--

CREATE TABLE IF NOT EXISTS `cm_list` (
  `list_id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(255) NOT NULL COMMENT '联动菜单名称',
  `list_desc` text NOT NULL COMMENT '简述',
  PRIMARY KEY (`list_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `cm_list`
--

INSERT INTO `cm_list` (`list_id`, `list_name`, `list_desc`) VALUES
(2, '所属省份', '所属省份'),
(4, '户籍镇街', '户籍镇街'),
(5, '毕业学校镇街', '毕业学校镇街'),
(6, '镇街学校', '镇街学校'),
(7, '毕业班级', '毕业班级');

-- --------------------------------------------------------

--
-- 表的结构 `cm_list_item`
--

CREATE TABLE IF NOT EXISTS `cm_list_item` (
  `list_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `item_desc` varchar(155) NOT NULL,
  `item_parent` int(11) NOT NULL,
  `item_thumb` varchar(255) NOT NULL,
  `item_sort` int(11) NOT NULL,
  PRIMARY KEY (`list_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- 转存表中的数据 `cm_list_item`
--

INSERT INTO `cm_list_item` (`list_item_id`, `list_id`, `item_desc`, `item_parent`, `item_thumb`, `item_sort`) VALUES
(1, 1, '文章一级别分类', 0, '', 0),
(2, 1, '文章二级分类', 1, '', 0),
(3, 1, '文章3级分类', 2, '', 0),
(4, 1, '文章3级分类', 2, '', 0),
(5, 1, '文章12分类', 1, '', 0),
(6, 2, '广东', 0, '', 0),
(7, 2, '北京', 0, '', 0),
(8, 2, '北京1', 7, '', 0),
(9, 4, '顺德大良', 0, '', 0),
(10, 4, '顺德容桂', 0, '', 0),
(11, 4, '顺德伦教', 0, '', 0),
(12, 4, '顺德勒流', 0, '', 0),
(13, 4, '顺德北滘', 0, '', 0),
(14, 4, '顺德龙江', 0, '', 0),
(15, 4, '顺德乐从', 0, '', 0),
(16, 4, '顺德陈村', 0, '', 0),
(17, 4, '顺德杏坛', 0, '', 0),
(18, 4, '顺德均安', 0, '', 0),
(19, 4, '佛山南海', 0, '', 0),
(20, 4, '佛山禅城', 0, '', 0),
(21, 4, '佛山高明', 0, '', 0),
(22, 4, '佛山三水', 0, '', 0),
(23, 5, '顺德大良', 0, '', 0),
(24, 5, '顺德容桂', 0, '', 0),
(25, 5, '顺德伦教', 0, '', 0),
(26, 5, '顺德北滘', 0, '', 0),
(27, 5, '顺德陈村', 0, '', 0),
(28, 5, '顺德乐从', 0, '', 0),
(29, 5, '顺德勒流', 0, '', 0),
(30, 5, '顺德龙江', 0, '', 0),
(31, 5, '顺德杏坛', 0, '', 0),
(32, 5, '顺德均安', 0, '', 0),
(33, 6, '顺德德胜小学', 0, '', 0),
(34, 6, '顺德一中附小', 0, '', 0),
(35, 7, '1', 0, '', 0),
(36, 7, '2', 0, '', 0),
(37, 7, '3', 0, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `cm_table`
--

CREATE TABLE IF NOT EXISTS `cm_table` (
  `table_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(256) NOT NULL COMMENT '表名称',
  `description` varchar(255) DEFAULT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  `list_button` text NOT NULL COMMENT '列表按钮',
  `search_option` text NOT NULL COMMENT '搜索设置',
  PRIMARY KEY (`table_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- 转存表中的数据 `cm_table`
--

INSERT INTO `cm_table` (`table_id`, `table_name`, `description`, `pid`, `list_button`, `search_option`) VALUES
(11, 'website', '站点', 0, 'a:2:{i:0;a:2:{s:4:"href";s:20:"Website/Website_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:19:"Website/Website_del";s:4:"desc";s:6:"删除";}}', ''),
(13, 'article', '内容', 15, 'a:2:{i:0;a:2:{s:4:"href";s:20:"Article/Article_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:19:"Article/Article_del";s:4:"desc";s:6:"删除";}}', 'a:2:{i:0;a:2:{s:10:"tab_col_id";s:2:"82";s:5:"match";s:4:"like";}i:1;a:2:{s:10:"tab_col_id";s:2:"83";s:5:"match";s:4:"like";}}'),
(14, 'web_baseinfo', '站点基础信息', 0, 'a:2:{i:0;a:2:{s:4:"href";s:30:"Web_baseinfo/Web_baseinfo_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:29:"Web_baseinfo/Web_baseinfo_del";s:4:"desc";s:6:"删除";}}', ''),
(15, 'category', '栏目', 0, 'a:3:{i:0;a:3:{s:11:"otherParams";a:0:{}s:4:"desc";s:6:"编辑";s:4:"href";s:22:"Category/Category_edit";}i:1;a:3:{s:11:"otherParams";a:0:{}s:4:"desc";s:6:"删除";s:4:"href";s:21:"Category/Category_del";}i:2;a:3:{s:11:"otherParams";a:0:{}s:4:"desc";s:12:"文章管理";s:4:"href";s:21:"Content/module2action";}}', ''),
(18, 'art_position', '推荐位', 0, 'a:2:{i:0;a:2:{s:4:"href";s:30:"Art_position/Art_position_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:29:"Art_position/Art_position_del";s:4:"desc";s:6:"删除";}}', ''),
(20, 'product', '组合秤产品', 15, 'a:2:{i:0;a:2:{s:4:"href";s:20:"Product/Product_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:19:"Product/Product_del";s:4:"desc";s:6:"删除";}}', 'a:3:{i:0;a:2:{s:10:"tab_col_id";s:3:"121";s:5:"match";s:4:"like";}i:1;a:2:{s:10:"tab_col_id";s:3:"321";s:5:"match";s:4:"like";}i:2;a:2:{s:10:"tab_col_id";s:3:"338";s:5:"match";s:2:"eq";}}'),
(21, 'friend_links', '在线咨询', 15, 'a:2:{i:0;a:3:{s:11:"otherParams";a:1:{i:0;a:2:{s:3:"key";s:6:"cat_id";s:5:"value";s:6:"cat_id";}}s:4:"desc";s:6:"编辑";s:4:"href";s:30:"Friend_links/Friend_links_edit";}i:1;a:3:{s:11:"otherParams";a:0:{}s:4:"desc";s:6:"删除";s:4:"href";s:29:"Friend_links/Friend_links_del";}}', 'a:2:{i:0;a:2:{s:10:"tab_col_id";s:3:"306";s:5:"match";s:2:"eq";}i:1;a:2:{s:10:"tab_col_id";s:3:"307";s:5:"match";s:2:"eq";}}'),
(22, 'others', '留言', 15, 'a:2:{i:0;a:2:{s:4:"href";s:18:"Others/Others_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:17:"Others/Others_del";s:4:"desc";s:6:"删除";}}', ''),
(41, 'Sub', '子公司', 15, 'a:2:{i:0;a:2:{s:4:"href";s:12:"Sub/Sub_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:11:"Sub/Sub_del";s:4:"desc";s:6:"删除";}}', ''),
(28, 'language', '语言字典', 0, 'a:2:{i:0;a:2:{s:4:"href";s:22:"Language/Language_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:21:"Language/Language_del";s:4:"desc";s:6:"删除";}}', 'a:2:{i:0;a:2:{s:10:"tab_col_id";s:3:"234";s:5:"match";s:4:"like";}i:1;a:2:{s:10:"tab_col_id";s:3:"232";s:5:"match";s:4:"like";}}'),
(32, 'job', '工作岗位', 15, 'a:2:{i:0;a:2:{s:4:"href";s:12:"Job/Job_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:11:"Job/Job_del";s:4:"desc";s:6:"删除";}}', ''),
(48, 'student', '学生', 0, 'a:2:{i:0;a:2:{s:4:"href";s:20:"Student/Student_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:19:"Student/Student_del";s:4:"desc";s:6:"删除";}}', ''),
(40, 'article_link', '图文链接', 15, 'a:2:{i:0;a:2:{s:4:"href";s:30:"Article_link/Article_link_edit";s:4:"desc";s:6:"编辑";}i:1;a:2:{s:4:"href";s:29:"Article_link/Article_link_del";s:4:"desc";s:6:"删除";}}', '');

-- --------------------------------------------------------

--
-- 表的结构 `cm_table_column`
--

CREATE TABLE IF NOT EXISTS `cm_table_column` (
  `tab_col_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) NOT NULL COMMENT '字段所属table',
  `column_name` varchar(255) NOT NULL COMMENT '字段（name）',
  `column_type` varchar(100) NOT NULL COMMENT '字段类型',
  `is_add` tinyint(4) NOT NULL COMMENT '0添加不显示，1显示',
  `is_edit` tinyint(4) NOT NULL COMMENT '编辑0不显示，1显示',
  `is_list` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否列表',
  `is_search` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否搜索字段',
  `column_default_value` text NOT NULL,
  `column_desc` varchar(255) NOT NULL DEFAULT '',
  `column_help` varchar(255) NOT NULL,
  `column_error_message` varchar(255) NOT NULL,
  `column_attrs` text NOT NULL COMMENT '表单配置属性',
  `list_attr` varchar(500) NOT NULL DEFAULT '',
  `cm_table_column_sort` float NOT NULL COMMENT '字段显示排序',
  `is_validate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0为不需要，1为需要',
  `min_length` int(11) NOT NULL DEFAULT '0' COMMENT '最小长度',
  `max_length` int(11) NOT NULL DEFAULT '999999' COMMENT '最大长度',
  `validate_regexp` varchar(255) NOT NULL COMMENT '验证所用正则',
  `is_excel_in` int(11) NOT NULL DEFAULT '0' COMMENT '是否为Excel导入，0为否',
  `is_excel_empty_in` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否允许Excel空导入，默认为0：否',
  `is_excel_match` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为Excel导出，0为否',
  `is_excel_out` int(11) NOT NULL DEFAULT '0' COMMENT '是否为Excel导出，0为否',
  `excel_match_method` varchar(50) NOT NULL COMMENT 'excel匹配方式',
  `excel_in_map` text NOT NULL COMMENT '映射关系',
  `excel_in_sort` tinyint(4) NOT NULL DEFAULT '10' COMMENT 'excel导入字段顺序',
  `excel_match_logic` varchar(50) NOT NULL COMMENT '匹配逻辑',
  `excel_out_map` text NOT NULL COMMENT '导出映射关系',
  `excel_out_sort` tinyint(4) NOT NULL DEFAULT '10' COMMENT '导出顺序',
  PRIMARY KEY (`tab_col_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=559 ;

--
-- 转存表中的数据 `cm_table_column`
--

INSERT INTO `cm_table_column` (`tab_col_id`, `table_id`, `column_name`, `column_type`, `is_add`, `is_edit`, `is_list`, `is_search`, `column_default_value`, `column_desc`, `column_help`, `column_error_message`, `column_attrs`, `list_attr`, `cm_table_column_sort`, `is_validate`, `min_length`, `max_length`, `validate_regexp`, `is_excel_in`, `is_excel_empty_in`, `is_excel_match`, `is_excel_out`, `excel_match_method`, `excel_in_map`, `excel_in_sort`, `excel_match_logic`, `excel_out_map`, `excel_out_sort`) VALUES
(65, 11, 'website_id', 'primaryKey', 1, 1, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(66, 11, 'webname', 'singleErea', 1, 1, 1, 0, '', '站点名称', '站点名称', '', '', '', 66, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(67, 11, 'weburl', 'singleErea', 1, 1, 1, 0, '', '站点模板目录', '站点目录', '', '', '', 102, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(69, 13, 'article_id', 'primaryKey', 0, 1, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(70, 14, 'web_baseinfo_id', 'primaryKey', 1, 1, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(72, 14, 'info_key', 'singleErea', 1, 1, 1, 0, '', '信息键', '信息键', '', '', '', 72, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(73, 14, 'info_val', 'singleErea', 1, 1, 1, 0, '', '信息值', '信息值', '', '', '', 73, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(75, 15, 'category_id', 'primaryKey', 1, 1, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(76, 15, 'cat_name', 'singleErea', 1, 1, 1, 0, '', '栏目名称', '栏目名称', '', '', '', 76, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(78, 15, 'thumb', 'singlePic', 1, 1, 1, 0, '', '缩略图', '缩略图', '', '', '', 81, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(79, 15, 'is_show', 'radio', 1, 1, 1, 0, '', '显示', '显示', '', 'a:2:{s:4:"type";i:1;s:5:"lists";a:2:{i:0;a:2:{s:5:"value";s:1:"0";s:4:"desc";s:3:"是";}i:1;a:2:{s:5:"value";s:1:"1";s:4:"desc";s:3:"否";}}}', '', 147, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(80, 15, 'is_single', 'radio', 1, 1, 0, 0, '', '是否单页', '是否单页', '', 'a:2:{s:4:"type";i:1;s:5:"lists";a:2:{i:0;a:2:{s:5:"value";s:1:"0";s:4:"desc";s:3:"是";}i:1;a:2:{s:5:"value";s:1:"1";s:4:"desc";s:3:"否";}}}', '', 146, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(82, 13, 'title', 'singleErea', 1, 1, 1, 1, '', '标题', '标题', '', '', '', 83, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(83, 13, 'keywords', 'singleErea', 1, 1, 0, 1, '', '关键词', '关键词', '', '', '', 218, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(86, 13, 'content', 'edictor', 1, 1, 0, 0, '', '内容', '内容', '内容', '', '', 219, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(87, 13, 'catalog', 'multiPic', 1, 1, 0, 0, '', '画册', '画册', '', 'a:1:{s:9:"pic_attrs";a:5:{i:0;a:4:{s:3:"key";s:4:"name";s:4:"desc";s:12:"图片名称";s:9:"attr_type";s:1:"1";s:4:"sort";s:1:"0";}i:1;a:4:{s:3:"key";s:3:"alt";s:4:"desc";s:9:"图片ALT";s:9:"attr_type";s:1:"1";s:4:"sort";s:1:"1";}i:2;a:4:{s:3:"key";s:4:"sort";s:4:"desc";s:12:"图片排序";s:9:"attr_type";s:1:"1";s:4:"sort";s:1:"2";}i:3;a:4:{s:3:"key";s:4:"href";s:4:"desc";s:12:"图片链接";s:9:"attr_type";s:1:"1";s:4:"sort";s:1:"3";}i:4;a:4:{s:3:"key";s:7:"content";s:4:"desc";s:12:"图片描述";s:9:"attr_type";s:1:"3";s:4:"sort";s:1:"4";}}}', '', 88, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(88, 13, 'description', 'multiErea', 1, 1, 0, 0, '', '描述', '描述', '', '', '', 101, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(89, 13, 'datetime', 'hidden_date', 1, 1, 0, 0, '', '时间', '时间', '', '1', '', 114, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(93, 18, 'art_position_id', 'primaryKey', 1, 1, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(94, 18, 'pos_name', 'singleErea', 1, 1, 1, 0, '', '推荐位置', '推荐位置', '', '', '', 94, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(96, 13, 'positions', 'checkedbox', 1, 1, 0, 0, '', '推荐位', '推荐位', '', 'a:5:{s:4:"type";s:1:"0";s:9:"value_key";s:15:"art_position_id";s:10:"value_desc";s:8:"pos_name";s:8:"data_sql";s:30:"SELECT *  FROM  `art_position`";s:5:"lists";a:1:{i:0;a:2:{s:5:"value";s:0:"";s:4:"desc";s:0:"";}}}', '', 172, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(97, 13, 'siteid', 'session', 1, 1, 0, 0, '', '所属站点', '所属站点', '', '', '', 113, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(98, 18, 'siteid', 'session', 1, 1, 0, 0, '', '所属站点', '所属站点', '', '', '', 98, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(99, 15, 'siteid', 'session', 1, 1, 0, 0, '', '所属站点', '所属站点', '', '', '', 148, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(100, 14, 'siteid', 'session', 1, 1, 0, 0, '', '所属站点', '所属站点', '', '', '', 100, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(101, 13, 'cat_id', 'foreignKey', 1, 1, 0, 0, '', '所属分类', '所属分类', '', 'a:2:{s:10:"table_name";s:8:"category";s:11:"column_name";s:11:"category_id";}', '', 168, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(102, 11, 'siteUrl', 'singleErea', 1, 1, 0, 0, '', '站点地址', '站点地址', '', '', '', 67, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(104, 13, 'hits', 'special', 0, 1, 0, 0, '', '点击量', '点击量', '', '', '', 105, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(105, 13, 'favor', 'special', 0, 0, 0, 0, '', '点赞量', '点赞量', '', '', '', 104, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(115, 20, 'product_id', 'primaryKey', 0, 0, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(116, 21, 'friend_links_id', 'primaryKey', 0, 0, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(117, 22, 'others_id', 'primaryKey', 1, 1, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(120, 15, 'module_id', 'select', 1, 1, 0, 0, '', '所属模型', '所属模型', '', 'a:6:{s:4:"type";s:1:"0";s:9:"value_key";s:8:"table_id";s:10:"value_desc";s:10:"table_name";s:8:"data_sql";s:26:"SELECT *  FROM  `cm_table`";s:7:"list_id";s:1:"0";s:5:"lists";a:1:{i:0;a:2:{s:5:"value";s:0:"";s:4:"desc";s:0:"";}}}', '', 167, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(121, 20, 'title', 'singleErea', 1, 1, 1, 1, '', '产品名称', '产品名称', '标题必填(2到30字)', '', '', 122, 1, 4, 60, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(122, 20, 'thumb', 'singlePic', 1, 1, 1, 0, '', '缩略图', '建议大小224x224', '', '', '', 205, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(123, 20, 'description', 'multiErea', 1, 1, 0, 0, '', '描述', '每一行用句号“。”隔开', '', '', '', 207, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(124, 20, 'content', 'edictor', 1, 1, 0, 0, '', '产品介绍', '内容', '', '', '', 321, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(125, 20, 'siteid', 'session', 1, 1, 0, 0, '', '所属站点', '所属站点', '', '', '', 324, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(455, 20, 'performance', 'edictor', 1, 1, 0, 0, '', '主要性能', '', '', '', '', 455, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(128, 20, 'positions', 'checkedbox', 1, 1, 0, 0, '', '推荐位', '推荐位', '', 'a:5:{s:4:"type";s:1:"0";s:9:"value_key";s:15:"art_position_id";s:10:"value_desc";s:8:"pos_name";s:8:"data_sql";s:30:"SELECT *  FROM  `art_position`";s:5:"lists";a:1:{i:0;a:2:{s:5:"value";s:0:"";s:4:"desc";s:0:"";}}}', '', 322, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(129, 22, 'title', 'singleErea', 1, 1, 1, 0, '', '姓名', '标题', '', '', '', 131, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(131, 22, 'description', 'multiErea', 1, 1, 0, 0, '', '描述', '描述', '', '', '', 150, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(133, 22, 'siteid', 'session', 1, 1, 0, 0, '', '所属站点', '所属站点', '', '', '', 193, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(252, 22, 'product_category', 'singleErea', 1, 1, 1, 0, '', '产品系列', '产品系列', '产品系列', '', '', 252, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(251, 22, 'country', 'singleErea', 1, 1, 1, 0, '', '国家', '国家', '', '', '', 251, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(248, 22, 'address2', 'singleErea', 1, 1, 0, 0, '', '联系地址2', '联系地址2', '联系地址2', '', '', 248, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(249, 22, 'city', 'singleErea', 1, 1, 0, 0, '', '城市', '城市', '城市', '', '', 249, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(250, 22, 'zip', 'singleErea', 1, 1, 0, 0, '', '邮编', '邮编', '邮编', '', '', 250, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(146, 15, 'title', 'singleErea', 1, 1, 0, 0, '', 'title', 'title', '', '', '', 80, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(147, 15, 'keywords', 'singleErea', 1, 1, 0, 0, '', 'Keywords', 'Keywords', '', '', '', 99, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(148, 15, 'description', 'multiErea', 1, 1, 0, 0, '', 'Description', 'Description', '', '', '', 120, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(150, 22, 'cat_id', 'foreignKey', 1, 1, 0, 0, '', '所属分类', '所属分类', '', 'a:2:{s:10:"table_name";s:8:"category";s:11:"column_name";s:11:"category_id";}', '', 196, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(166, 15, 'sort', 'singleErea', 1, 1, 0, 0, '', '排序', '排序', '', '', '', 166, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(167, 15, 'eng_name', 'singleErea', 0, 0, 0, 0, '', '英文名称', '英文名称', '', '', '', 79, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(324, 20, 'display', 'singleErea', 1, 1, 0, 0, '', '显示器', '显示器', '', '', '', 223, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(186, 21, 'thumb', 'singlePic', 1, 1, 1, 0, '', '缩略图', '缩略图', '', '', '', 185, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(187, 21, 'url', 'singleErea', 1, 1, 1, 0, '', '链接地址', '链接地址', '', '', '', 186, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(185, 21, 'name', 'singleErea', 1, 1, 1, 0, '', '名称', '名称', '', '', '', 172, 1, 0, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(174, 15, 'category_sort', 'singleErea', 1, 1, 0, 0, '', '排序', '排序', '', '', '', 172, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(175, 13, 'article_sort', 'singleErea', 1, 1, 1, 0, '', '排序', '排序', '', '', '', 112, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(176, 20, 'product_sort', 'singleErea', 1, 1, 1, 0, '', '排序', '排序', '', '', '', 323, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(178, 22, 'others_sort', 'singleErea', 0, 0, 0, 0, '', '排序', '排序', '', '', '', 210, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(459, 41, 'Sub_sort', 'singleErea', 1, 1, 0, 0, '', '排序', '排序', '', '', '', 459, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(456, 20, 'technical', 'edictor', 1, 1, 0, 0, '', '技术特点', '', '', '', '', 456, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(457, 20, 'operation', 'edictor', 1, 1, 0, 0, '', '操作方法', '', '', '', '', 457, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(458, 41, 'sub_id', 'primaryKey', 0, 0, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(246, 22, 'industry', 'singleErea', 1, 1, 0, 0, '', '行业', '行业', '行业', '', '', 246, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(247, 22, 'company', 'singleErea', 1, 1, 1, 0, '', '公司名称', '公司名称', '公司名称', '', '', 247, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(188, 21, 'description', 'multiErea', 1, 1, 0, 0, '', '备注', '', '', '', '', 187, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(189, 21, 'friend_links_sort', 'singleErea', 1, 1, 0, 0, '', '排序', '排序', '', '', '', 188, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(191, 14, 'web_baseinfo_sort', 'singleErea', 1, 1, 0, 0, '', '排序', '排序', '', '', '', 172, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(192, 11, 'website_sort', 'singleErea', 1, 1, 0, 0, '', '排序', '排序', '', '', '', 172, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(253, 22, 'product_name', 'singleErea', 1, 1, 0, 0, '', '产品名称', '产品名称', '产品名称', '', '', 253, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(196, 22, 'tell', 'singleErea', 1, 1, 1, 0, '', 'tell', '', '', '', '', 133, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(219, 13, 'big_thumb', 'singlePic', 1, 1, 1, 0, '', '缩略图', '片', '', '', '', 97, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(225, 20, 'catalog', 'multiPic', 1, 1, 0, 0, '', '产品图册', '所有图片大小相同（713x370）', '所有图片大小相同（713x370）', 'a:1:{s:9:"pic_attrs";a:5:{i:0;a:4:{s:3:"key";s:4:"name";s:4:"desc";s:6:"型号";s:9:"attr_type";s:1:"1";s:4:"sort";s:1:"0";}i:1;a:4:{s:3:"key";s:3:"alt";s:4:"desc";s:11:"图片title";s:9:"attr_type";s:1:"1";s:4:"sort";s:1:"1";}i:2;a:4:{s:3:"key";s:4:"sort";s:4:"desc";s:6:"排序";s:9:"attr_type";s:1:"1";s:4:"sort";s:1:"2";}i:3;a:4:{s:3:"key";s:4:"href";s:4:"desc";s:6:"链接";s:9:"attr_type";s:1:"1";s:4:"sort";s:1:"3";}i:4;a:4:{s:3:"key";s:7:"content";s:4:"desc";s:12:"图片描述";s:9:"attr_type";s:1:"3";s:4:"sort";s:1:"4";}}}', '', 206, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(226, 22, 'mail', 'singleErea', 1, 1, 0, 0, '', '邮箱', '', '', '', '', 172, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(227, 22, 'address', 'singleErea', 1, 1, 0, 0, '', '联系地址', '', '', '', '', 227, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(229, 28, 'language_id', 'primaryKey', 0, 0, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(230, 28, 'language_sort', 'singleErea', 1, 1, 1, 0, '', '排序', '排序', '', '', '', 230, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(234, 28, 'key_index', 'singleErea', 1, 0, 1, 1, '', '键位', '语言键位（与中文一样）', '键位不能为空', '', '', 232, 1, 0, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(232, 28, 'chinese', 'singleErea', 1, 1, 1, 1, '', '中文', '中文', '中文不能为空', '', '', 233, 1, 0, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(233, 28, 'english', 'singleErea', 1, 1, 1, 0, '', '英文', '英文', '英文不能为空', '', '', 234, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(263, 32, 'job_id', 'primaryKey', 0, 0, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(264, 32, 'job_sort', 'singleErea', 1, 1, 0, 0, '99', '排序', '排序', '', '', '', 264, 1, 0, 999999, 'intege1', 0, 0, 0, 0, '', '', 10, '', '', 10),
(303, 32, 'title', 'singleErea', 1, 1, 1, 0, '', '职位名称', '职位名称', '职位名称必填', '', '', 266, 0, 2, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(266, 32, 'datetime', 'hidden_date', 1, 1, 0, 0, '', '发布时间', '发布时间', '发布时间', '1', '', 303, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(267, 32, 'qualifications', 'singleErea', 0, 0, 0, 0, '', '学历', '学历', '学历必填', '', '', 267, 0, 0, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(272, 32, 'description', 'edictor', 1, 1, 0, 0, '', '岗位要求', '岗位要求', '岗位要求', '', '', 272, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(271, 32, 'adress', 'singleErea', 0, 1, 0, 0, '', '工作地点', '工作地点', '工作地点必填', '', '', 271, 0, 0, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(273, 32, 'cat_id', 'foreignKey', 1, 1, 0, 0, '', '分类ID', '', '', 'a:2:{s:10:"table_name";s:8:"category";s:11:"column_name";s:11:"category_id";}', '', 273, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(544, 48, 'graduate_class', 'select', 1, 1, 0, 0, '', '毕业班级', '', '', 'a:7:{s:4:"type";s:1:"2";s:9:"value_key";s:0:"";s:10:"value_desc";s:0:"";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"7";s:5:"lists";a:1:{i:0;a:2:{s:5:"value";s:0:"";s:4:"desc";s:0:"";}}}', '', 544, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(543, 48, 'graduate_school', 'select', 1, 1, 0, 0, '', '毕业学校', '', '', 'a:7:{s:4:"type";s:1:"2";s:9:"value_key";s:0:"";s:10:"value_desc";s:0:"";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"6";s:5:"lists";a:1:{i:0;a:2:{s:5:"value";s:0:"";s:4:"desc";s:0:"";}}}', '', 543, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(539, 48, 'photo', 'singlePic', 1, 1, 0, 0, '', '照片', '', '', '', '', 539, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(540, 48, 'address', 'singleErea', 1, 1, 0, 0, '', '详细住址', '', '', '', '', 540, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(541, 48, 'graduate_province', 'select', 1, 1, 0, 0, '', '毕业地区', '', '', 'a:7:{s:4:"type";s:1:"1";s:9:"value_key";s:5:"value";s:10:"value_desc";s:4:"desc";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"0";s:5:"lists";a:2:{i:0;a:2:{s:5:"value";s:1:"1";s:4:"desc";s:12:"顺德区内";}i:1;a:2:{s:5:"value";s:1:"2";s:4:"desc";s:12:"顺德区外";}}}', '', 541, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(542, 48, 'graduate_street', 'select', 1, 1, 0, 0, '', '毕业街道', '', '', 'a:7:{s:4:"type";s:1:"2";s:9:"value_key";s:0:"";s:10:"value_desc";s:0:"";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"5";s:5:"lists";a:1:{i:0;a:2:{s:5:"value";s:0:"";s:4:"desc";s:0:"";}}}', '', 542, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(538, 48, 'street_desc', 'singleErea', 1, 1, 0, 0, '', '请输入户籍所在地', '', '', '', '', 538, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(537, 48, 'street', 'select', 1, 1, 0, 0, '', '镇街', '', '', 'a:7:{s:4:"type";s:1:"2";s:9:"value_key";s:0:"";s:10:"value_desc";s:0:"";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"4";s:5:"lists";a:1:{i:0;a:2:{s:5:"value";s:0:"";s:4:"desc";s:0:"";}}}', '', 537, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(533, 48, 'birth_date', 'date', 1, 1, 0, 0, '', '出生日期', '', '', '', '', 533, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(534, 48, 'healthy', 'singleErea', 1, 1, 0, 0, '', '健康状况', '', '', '', '', 534, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(535, 48, 'identity', 'singleErea', 1, 1, 0, 0, '', '身份证', '', '', '', '', 535, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(536, 48, 'is_in_foshan', 'radio', 1, 1, 0, 0, '', '户籍所在地', '', '', 'a:2:{s:4:"type";i:1;s:5:"lists";a:2:{i:0;a:2:{s:5:"value";s:1:"1";s:4:"desc";s:9:"佛山内";}i:1;a:2:{s:5:"value";s:1:"2";s:4:"desc";s:9:"佛山外";}}}', '', 536, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(287, 32, 'need_job', 'singleErea', 0, 0, 0, 0, '', '应聘职位', '应聘职位', '应聘职位', '', '', 287, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(532, 48, 'sex', 'radio', 1, 1, 0, 0, '', '性别', '', '', 'a:2:{s:4:"type";i:1;s:5:"lists";a:2:{i:0;a:2:{s:5:"value";s:1:"1";s:4:"desc";s:3:"男";}i:1;a:2:{s:5:"value";s:1:"2";s:4:"desc";s:3:"女";}}}', '', 532, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(531, 48, 'name', 'singleErea', 1, 1, 0, 0, '', '姓名', '', '', '', '', 531, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(530, 48, 'student_sort', 'singleErea', 1, 1, 0, 0, '', '排序', '排序', '', '', '', 530, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(529, 48, 'student_id', 'primaryKey', 0, 0, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(293, 21, 'siteid', 'session', 1, 1, 0, 0, '', '所属站点', '所属站点', '所属站点', '', '', 293, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(302, 11, 'thumb', 'singlePic', 1, 1, 1, 0, '', '国旗缩略图', '国旗缩略图', '', '', '', 302, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(306, 21, 'part', 'select', 1, 1, 1, 1, '', '联系方式所属事业部', '联系方式所属事业部', '', 'a:7:{s:4:"type";s:1:"1";s:9:"value_key";s:5:"value";s:10:"value_desc";s:4:"desc";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"0";s:5:"lists";a:4:{i:0;a:2:{s:5:"value";s:9:"组合秤";s:4:"desc";s:9:"组合秤";}i:1;a:2:{s:5:"value";s:9:"失重秤";s:4:"desc";s:9:"失重秤";}i:2;a:2:{s:5:"value";s:9:"金检器";s:4:"desc";s:9:"金检器";}i:3;a:2:{s:5:"value";s:9:"选别秤";s:4:"desc";s:9:"选别秤";}}}', '', 306, 1, 1, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(307, 21, 'font_back', 'select', 1, 1, 1, 1, '', '售前售后', '', '', 'a:7:{s:4:"type";s:1:"1";s:9:"value_key";s:5:"value";s:10:"value_desc";s:4:"desc";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"0";s:5:"lists";a:2:{i:0;a:2:{s:5:"value";s:6:"售前";s:4:"desc";s:6:"售前";}i:1;a:2:{s:5:"value";s:6:"售后";s:4:"desc";s:6:"售后";}}}', '', 307, 1, 1, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(309, 21, 'cat_id', 'foreignKey', 1, 1, 0, 0, '', '所属分类', 'cat_id', 'cat_id', 'a:2:{s:10:"table_name";s:8:"category";s:11:"column_name";s:11:"category_id";}', '', 309, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(460, 41, 'title', 'singleErea', 1, 1, 1, 0, '', '公司名称', '', '', '', '', 460, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(461, 41, 'content', 'edictor', 1, 1, 0, 0, '', '公司描述', '', '', '', '', 461, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(462, 41, 'advantage', 'edictor', 1, 1, 0, 0, '', '公司优势', '', '', '', '', 462, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(463, 41, 'environment', 'singlePic', 1, 1, 0, 0, '', '公司环境图', '', '', '', '', 465, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(464, 41, 'cat_id', 'foreignKey', 1, 1, 0, 0, '', '外键', '', '', 'a:2:{s:10:"table_name";s:8:"category";s:11:"column_name";s:11:"category_id";}', '', 464, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(465, 41, 'thumb', 'singlePic', 1, 1, 1, 0, '', '公司缩略图', '', '', '', '', 463, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(466, 20, 'cat_id', 'foreignKey', 1, 1, 0, 0, '', '关联', '', '', 'a:2:{s:10:"table_name";s:8:"category";s:11:"column_name";s:11:"category_id";}', '', 466, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(433, 40, 'article_link_id', 'primaryKey', 0, 0, 1, 0, '', '主键（自增）', '', '', '', '', 0, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(434, 40, 'article_link_sort', 'singleErea', 1, 1, 0, 0, '', '排序', '排序', '', '', '', 434, 0, 0, 999999, '', 0, 0, 0, 0, '', '', 10, '', '', 10),
(435, 40, 'title', 'singleErea', 1, 1, 1, 0, '', '标题', '标题', '标题', '', '', 435, 0, 2, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(436, 40, 'big_thumb', 'singlePic', 1, 1, 1, 0, '', '缩略图', '', '', '', '', 436, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(437, 40, 'href', 'singleErea', 1, 1, 0, 0, '', '链接', '链接', '链接', '', '', 437, 1, 2, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(438, 40, 'datetime', 'hidden_date', 1, 1, 0, 0, '', '时间', '', '', '1', '', 438, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(439, 40, 'cat_id', 'foreignKey', 1, 1, 0, 0, '', '所属分类', '所属分类', '', 'a:2:{s:10:"table_name";s:8:"category";s:11:"column_name";s:11:"category_id";}', '', 439, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(451, 11, 'lang', 'singleErea', 1, 1, 0, 0, '', '语言', '', '', '', '', 451, 1, 2, 999999, 'notempty', 0, 0, 0, 0, '', '', 10, '', '', 10),
(453, 40, 'siteid', 'session', 1, 1, 0, 0, '', 'zhandian', '', '', '', '', 453, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(454, 32, 'siteid', 'session', 1, 1, 0, 0, '', 'zhandian', '', '', '', '', 454, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(556, 48, 'student_desc', 'multiErea', 1, 1, 0, 0, '', '学生自述', '', '', '', '', 556, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(557, 48, 'add_phone', 'singleErea', 1, 1, 0, 0, '', '验证手机号码', '', '', '', '', 557, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(558, 48, 'add_openid', 'singleErea', 1, 1, 0, 0, '', '微信open_id', '', '', '', '', 558, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(545, 48, 'graduate_school_desc', 'singleErea', 1, 1, 0, 0, '', '学校全名', '', '', '', '', 545, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(546, 48, 'register_num', 'singleErea', 1, 1, 0, 0, '', '全国学籍号', '', '', '', '', 546, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(547, 48, 'father', 'singleErea', 1, 1, 0, 0, '', '父亲姓名', '', '', '', '', 547, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(548, 48, 'father_tell', 'singleErea', 1, 1, 0, 0, '', '父亲联系方式', '', '', '', '', 548, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(549, 48, 'father_address', 'singleErea', 1, 1, 0, 0, '', '父亲工作单位', '', '', '', '', 549, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(550, 48, 'mother', 'singleErea', 1, 1, 0, 0, '', '母亲姓名', '', '', '', '', 550, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(551, 48, 'mother_tell', 'singleErea', 1, 1, 0, 0, '', '母亲联系方式', '', '', '', '', 551, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(552, 48, 'mother_address', 'singleErea', 1, 1, 0, 0, '', '母亲工作单位', '', '', '', '', 552, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(553, 48, 'school_idea', 'select', 1, 1, 0, 0, '', '办学理念认可', '', '', 'a:7:{s:4:"type";s:1:"1";s:9:"value_key";s:5:"value";s:10:"value_desc";s:4:"desc";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"0";s:5:"lists";a:3:{i:0;a:2:{s:5:"value";s:1:"1";s:4:"desc";s:12:"非常认同";}i:1;a:2:{s:5:"value";s:1:"2";s:4:"desc";s:6:"认同";}i:2;a:2:{s:5:"value";s:1:"3";s:4:"desc";s:9:"不认同";}}}', '', 553, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(554, 48, 'first_choice', 'select', 1, 1, 0, 0, '', '第一志愿', '', '', 'a:7:{s:4:"type";s:1:"1";s:9:"value_key";s:5:"value";s:10:"value_desc";s:4:"desc";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"0";s:5:"lists";a:2:{i:0;a:2:{s:5:"value";s:1:"1";s:4:"desc";s:9:"国内部";}i:1;a:2:{s:5:"value";s:1:"2";s:4:"desc";s:9:"国际部";}}}', '', 554, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10),
(555, 48, 'second_choice', 'select', 1, 1, 0, 0, '', '第二志愿', '', '', 'a:7:{s:4:"type";s:1:"1";s:9:"value_key";s:5:"value";s:10:"value_desc";s:4:"desc";s:12:"value_parent";s:0:"";s:8:"data_sql";s:0:"";s:7:"list_id";s:1:"0";s:5:"lists";a:2:{i:0;a:2:{s:5:"value";s:1:"1";s:4:"desc";s:9:"国内部";}i:1;a:2:{s:5:"value";s:1:"2";s:4:"desc";s:9:"国际部";}}}', '', 555, 0, 0, 999999, '0', 0, 0, 0, 0, '', '', 10, '', '', 10);

-- --------------------------------------------------------

--
-- 表的结构 `friend_links`
--

CREATE TABLE IF NOT EXISTS `friend_links` (
  `friend_links_id` int(11) NOT NULL AUTO_INCREMENT,
  `friend_links_sort` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `siteid` varchar(255) NOT NULL DEFAULT '',
  `part` varchar(50) DEFAULT '',
  `font_back` varchar(50) DEFAULT '0',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`friend_links_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- 转存表中的数据 `friend_links`
--

INSERT INTO `friend_links` (`friend_links_id`, `friend_links_sort`, `name`, `thumb`, `url`, `description`, `siteid`, `part`, `font_back`, `cat_id`) VALUES
(17, 17, '失重秤售后', '/Public/uploads/pics/2017-08/599a820c5573f.jpg', '', '', '1', '失重秤', '售后', 183),
(16, 1, '失重秤售前', '/Public/uploads/pics/2017-08/599a8191335bf.jpg', '', '', '1', '失重秤', '售前', 183),
(20, 18, '组合秤售前1', '/Public/uploads/pics/2017-08/59a4c32180e8f.jpg', '', '', '1', '组合秤', '售前', 183),
(21, 21, '组合秤售后1', '/Public/uploads/pics/2017-08/59a4c38ba9adf.jpg', '', '', '1', '组合秤', '售后', 183),
(22, 22, '选别秤售前', '/Public/uploads/pics/2017-08/599bdc251be51.jpg', '', '', '1', '选别秤', '售前', 183),
(23, 23, '组合秤售前2', '/Public/uploads/pics/2017-08/59a4c334e7e82.jpg', '', '', '1', '组合秤', '售前', 183),
(24, 24, '组合秤售前3', '/Public/uploads/pics/2017-08/59a4c3647baa3.jpg', '', '', '1', '组合秤', '售前', 183),
(25, 25, '组合秤售前4', '/Public/uploads/pics/2017-08/59a4c37ab4e2a.jpg', '', '', '1', '组合秤', '售前', 183),
(26, 26, '组合秤售后2', '/Public/uploads/pics/2017-08/59a4c39c74461.jpg', '', '', '1', '组合秤', '售后', 183),
(27, 27, '组合秤售后3', '/Public/uploads/pics/2017-08/59a4c3b574461.jpg', '', '', '1', '组合秤', '售后', 183),
(28, 28, '组合秤售后4', '/Public/uploads/pics/2017-08/59a4c3c583885.jpg', '', '', '1', '组合秤', '售后', 183),
(29, 29, '金检器售前', '/Public/uploads/pics/2017-08/59a4c3e72837e.jpg', '', '', '1', '金检器', '售前', 183),
(30, 30, '金检器售后', '/Public/uploads/pics/2017-08/59a4c409b9e46.jpg', '', '', '1', '金检器', '售后', 183);

-- --------------------------------------------------------

--
-- 表的结构 `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_sort` int(11) NOT NULL DEFAULT '0',
  `datetime` varchar(150) NOT NULL DEFAULT '',
  `qualifications` varchar(255) NOT NULL DEFAULT '',
  `adress` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `need_job` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `siteid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `job`
--

INSERT INTO `job` (`job_id`, `job_sort`, `datetime`, `qualifications`, `adress`, `description`, `cat_id`, `need_job`, `title`, `siteid`) VALUES
(8, 99, '1505375867', '', '', '<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Time：2017/05/19&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Place：Guangdong, Shunde, Beijiao&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Requirement：&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">1、College degree or above, good command of English in listening, speaking，reading and writing, major in mechanical or China World Trade Center is preferred;&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">2、Familiar with overseas market environment and market operation mode, able to develop overseas market independently;</span>', 14, '', 'financial accounting', '1'),
(9, 99, '1505375867', '', '', '<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Time：2017/05/19&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Place：Guangdong, Shunde, Beijiao&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Requirement：&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">1、College degree or above, good command of English in listening, speaking，reading and writing, major in mechanical or China World Trade Center is preferred;&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">2、Familiar with overseas market environment and market operation mode, able to develop overseas market independently;</span>', 14, '', 'financial accounting', '1'),
(10, 99, '1505375867', '', '', '<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Time：2017/05/19&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Place：Guangdong, Shunde, Beijiao&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">Requirement：&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">1、College degree or above, good command of English in listening, speaking，reading and writing, major in mechanical or China World Trade Center is preferred;&nbsp;</span><br />\r\n<span style="color:#333333;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:25.6px;background-color:#FFFFFF;">2、Familiar with overseas market environment and market operation mode, able to develop overseas market independently;</span>', 14, '', 'financial accounting', '1');

-- --------------------------------------------------------

--
-- 表的结构 `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_sort` int(11) NOT NULL DEFAULT '0',
  `chinese` varchar(255) NOT NULL DEFAULT '',
  `english` varchar(255) NOT NULL DEFAULT '',
  `key_index` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94 ;

--
-- 转存表中的数据 `language`
--

INSERT INTO `language` (`language_id`, `language_sort`, `chinese`, `english`, `key_index`) VALUES
(1, 5, '失重秤', 'Loss-in-weight', '失重秤'),
(2, 2, '组合秤', 'combined balance', '组合秤'),
(3, 3, '金检器', 'Gold detector', '金检器'),
(4, 4, '选别秤', 'Alternative name', '选别称'),
(5, 5, '售前', 'Pre-sale', '售前'),
(6, 6, '售后', 'customer service', '售后'),
(7, 7, '缩小搜索范围', '', '缩小搜索范围'),
(8, 8, '名称', 'name', '名称'),
(9, 9, '型号', '', '型号'),
(10, 10, '代系', '', '代系'),
(11, 11, '斗数', '', '斗数'),
(12, 12, '称重范围', '', '称重范围'),
(13, 13, '最小刻度', '', '最小刻度'),
(14, 14, '最高速度', '', '最高速度'),
(15, 15, '料斗容量', '', '料斗容量'),
(16, 16, '显示器', '', '显示器'),
(17, 17, '电源要求', '', '电源要求'),
(18, 18, '应用范围', '', '应用范围'),
(19, 19, '控制器', '', '控制器'),
(20, 20, '称重单元', '', '称重单元'),
(21, 21, '齿轮箱', '', '齿轮箱'),
(22, 22, '螺杆驱动电机', '', '螺杆驱动电机'),
(23, 23, '水平搅拌驱动电机', '', '水平搅拌驱动电机'),
(24, 24, '标准电源', '', '标准电源'),
(25, 25, '喂料螺杆', '', '喂料螺杆'),
(26, 26, '水平搅拌', '', '水平搅拌'),
(27, 27, '垂直搅拌', '', '垂直搅拌'),
(28, 28, '进料口', '', '进料口'),
(29, 29, '进料口软连接', '', '进料口软连接'),
(30, 30, '垂直出料口软连接', '', '垂直出料口软连接'),
(31, 31, '排气孔', '', '排气孔'),
(32, 32, '料斗', '', '料斗'),
(33, 33, '材料特质', '', '材料特质'),
(34, 34, '堆積密度（kg/L）', '', '堆積密度（kg/L）'),
(35, 35, '流动性', '', '流动性'),
(36, 36, '腐蚀性', '', '腐蚀性'),
(37, 37, '流量范围(kg/h)', '', '流量范围(kg/h)'),
(38, 38, '批次范围(g)', '', '批次范围(g)'),
(39, 39, '易燃', '', '易燃'),
(40, 40, '防爆需求', '', '防爆需求'),
(42, 41, '显示屏', '', '显示屏'),
(43, 43, '开口尺寸', '', '开口尺寸'),
(44, 44, '可通过区域', '', '可通过区域'),
(45, 45, '裸机精度', '', '裸机精度'),
(46, 46, '物料传送方向', '', '物料传送方向'),
(47, 47, '包装类型', '', '包装类型'),
(48, 48, '重量范围', '', '重量范围'),
(49, 49, '输送带类型', '', '输送带类型'),
(50, 50, '剔除方式', '', '剔除方式'),
(51, 51, '称量精度范围', '', '称量精度范围'),
(52, 52, '称重速度', '', '称重速度'),
(53, 53, '称重带尺寸', '', '称重带尺寸'),
(54, 54, '目标重量', '', '目标重量'),
(55, 55, '输送带速度', '', '输送带速度'),
(56, 56, '预设数量', '', '预设数量'),
(57, 57, '可选剔除装置', '', '可选剔除装置'),
(58, 58, '电源规格', '', '电源规格'),
(59, 59, '包装尺寸', '', '包装尺寸'),
(60, 60, '包装重量', '', '包装重量'),
(61, 61, '防水等级', '', '防水等级'),
(62, 62, '操作界面', '', '操作界面'),
(63, 63, '详细参数', '', '详细参数'),
(64, 64, '产品介绍', '', '产品介绍'),
(65, 65, '产品图片', '', '产品图片'),
(66, 66, '最大称重', '', '最大称重'),
(67, 67, '秤重精度', '', '秤重精度'),
(69, 68, '螺杆直径', '', '螺杆直径'),
(70, 70, '皮带驱动电机', '', '皮带驱动电机'),
(71, 71, '皮带宽度', '', '皮带宽度'),
(72, 72, '振动器', '', '振动器'),
(73, 73, '振动驱动器', '', '振动驱动器'),
(74, 74, '振动槽宽度', '', '振动槽宽度'),
(75, 75, '带有静电', '', '带有静电'),
(76, 76, '重置', '', '重置'),
(77, 77, '首页', 'Home', '首页'),
(78, 78, '搜索', 'Search', '搜索'),
(79, 79, '关于海川', 'About', '关于海川'),
(80, 80, '首页', 'Home', '首页'),
(81, 81, '搜索', 'Search', '搜索'),
(82, 82, '海川简介', 'Overview', '海川简介'),
(83, 83, '新闻中心', 'News', '新闻中心'),
(84, 84, '产品项目部', 'Product part', '产品项目部'),
(85, 85, '选择机器', 'Select Pro', '选择机器'),
(86, 86, '案例中心', 'Case', '案例中心'),
(87, 87, '客户服务', 'Service', '客户服务'),
(88, 88, '服务网络', 'Agency', '服务网络'),
(89, 89, '联系我们', 'Contact us', '联系我们'),
(90, 90, '投资者关系', 'Investor Rela', '投资者关系'),
(91, 91, '了解更多', 'more', '了解更多'),
(92, 92, '栏目banner', 'Category banner', '栏目banner'),
(93, 93, '首页轮播图', 'Index Slider', '首页轮播图');

-- --------------------------------------------------------

--
-- 表的结构 `lib_explode`
--

CREATE TABLE IF NOT EXISTS `lib_explode` (
  `pos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `val` varchar(255) NOT NULL,
  `beloong_table_name` varchar(255) NOT NULL DEFAULT '' COMMENT '表名称',
  `beloong_table_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属表主键',
  PRIMARY KEY (`pos`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='实现explode临时表' AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `lib_explode`
--


-- --------------------------------------------------------

--
-- 表的结构 `others`
--

CREATE TABLE IF NOT EXISTS `others` (
  `others_id` int(11) NOT NULL AUTO_INCREMENT,
  `others_sort` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `siteid` varchar(255) NOT NULL DEFAULT '',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `tell` varchar(255) NOT NULL DEFAULT '',
  `mail` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `industry` varchar(255) NOT NULL DEFAULT '',
  `company` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `zip` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `product_category` varchar(255) NOT NULL DEFAULT '',
  `product_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`others_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=71 ;

--
-- 转存表中的数据 `others`
--

INSERT INTO `others` (`others_id`, `others_sort`, `title`, `description`, `siteid`, `cat_id`, `tell`, `mail`, `address`, `industry`, `company`, `address2`, `city`, `zip`, `country`, `product_category`, `product_name`) VALUES
(61, 61, '11', '', '1', 0, '44', ' ', '66', '22', '55', '77', '88', '99', '11', '金检器', '123'),
(62, 62, 'gggg', 'dfasdfasdf', '1', 0, 'dd', ' ', 'sdfasd', 'sdas', 'asdfsadf', 'fasdfasdf', 'asdf', 'sadf', 'asdfsdf', '组合秤', 'asdfs'),
(63, 63, 'hhhh', 'hhh', '1', 0, 'hhh', ' ', 'hhh', 'hhh', 'hhhhh', 'hhh', 'hhh', 'hh', 'hhh', '组合秤', 'hhh'),
(64, 64, '111', 'HENHAO ', '1', 0, '13702487989', '22@qq.com', ' ', 'IT', '飞扬公司', ' ', ' ', ' ', 'ZHOGNGUO ', '组合秤', 'FSDF323-0'),
(65, 65, '111', '123', '1', 0, '13702487589', '22@uu.com', ' ', '123', '123123', ' ', ' ', ' ', '123123', '组合秤', '123'),
(66, 66, '222', '6666', '1', 0, '444', '5555', ' ', ' ', '111', ' ', ' ', ' ', '333', ' ', ' '),
(67, 67, 'zzz', '123123', '1', 0, 'zzzzz', 'zzzzz', ' ', ' ', 'zz', ' ', ' ', ' ', 'zzzz', '15', ' '),
(68, 68, 'ttt', '123', '1', 0, 'ttttttt', 'ttttttt', ' ', ' ', 'ttt', ' ', ' ', ' ', 'ttttt', '', ' '),
(69, 69, '123123', 'asdf', '1', 0, 'asdf', 'sadf', ' ', ' ', '123', ' ', ' ', ' ', '123', 'Sodium Lignosulfonate', ' '),
(70, 70, '123', '123', '1', 0, '123', '123', ' ', ' ', '123', ' ', ' ', ' ', '123', 'Sodium Lignosulfonate', 'Calcium-Lignosulfonate3,Calcium-Lignosulfonate1');

-- --------------------------------------------------------

--
-- 表的结构 `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_sort` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `content` text NOT NULL,
  `siteid` varchar(255) NOT NULL DEFAULT '',
  `positions` varchar(500) NOT NULL DEFAULT '',
  `catalog` text NOT NULL,
  `display` varchar(255) NOT NULL DEFAULT '',
  `performance` text NOT NULL,
  `technical` text NOT NULL,
  `operation` text NOT NULL,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `product`
--

INSERT INTO `product` (`product_id`, `product_sort`, `title`, `thumb`, `description`, `content`, `siteid`, `positions`, `catalog`, `display`, `performance`, `technical`, `operation`, `cat_id`) VALUES
(1, 1, 'Calcium-Lignosulfonate3', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', '11111', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '', 'a:0:{}', '', '', '', '', 15),
(5, 1, 'Calcium-Lignosulfonate2', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', '11111', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '', 'a:0:{}', '', '', '', '', 15),
(6, 1, 'Calcium-Lignosulfonate1', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', '11111', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '', 'a:0:{}', '', '', '', '', 15),
(7, 1, 'Calcium-Lignosulfonate3', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', '11111', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '1', 'a:0:{}', '', '', '', '', 15),
(8, 1, 'Calcium-Lignosulfonate3', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', '11111', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '1', 'a:0:{}', '', '', '', '', 15),
(9, 1, 'Calcium-Lignosulfonate3', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', '11111', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '1', 'a:0:{}', '', '', '', '', 15),
(10, 1, 'Calcium-Lignosulfonate3', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', 'The innovative CHRYSO® additives and admixtures are dedicated to the Construction professionals.\r\n                        They allow to:\r\n                        facilitate CEMENT production and enhance performance,\r\n                        improve CONCRETE performance, production,workability, aesthetics and sustainability,supply CONSTRUCTION SYSTEMS for complete building general construction, concrete repair, waterproofing and protection systems for new construction and rehabilitation & maintenance,\r\n                        enhance INDUSTRIAL APPLICATIONS performance and production (for plaster applications and oilfield).', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '1', 'a:4:{i:0;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f61ba3b12.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"1";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:1;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f6231dec9.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"2";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:2;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f6233e229.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"3";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:3;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f623594be.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"4";s:4:"href";s:0:"";s:7:"content";s:0:"";}}', '', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span>', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span>', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span>', 15),
(11, 1, 'Calcium-Lignosulfonate3', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', 'The innovative CHRYSO® additives and admixtures are dedicated to the Construction professionals.\r\n                        They allow to:\r\n                        facilitate CEMENT production and enhance performance,\r\n                        improve CONCRETE performance, production,workability, aesthetics and sustainability,supply CONSTRUCTION SYSTEMS for complete building general construction, concrete repair, waterproofing and protection systems for new construction and rehabilitation & maintenance,\r\n                        enhance INDUSTRIAL APPLICATIONS performance and production (for plaster applications and oilfield).', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '1', 'a:4:{i:0;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f61ba3b12.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"1";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:1;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f6231dec9.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"2";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:2;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f6233e229.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"3";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:3;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f623594be.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"4";s:4:"href";s:0:"";s:7:"content";s:0:"";}}', '', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span>', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span>', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span>', 16);
INSERT INTO `product` (`product_id`, `product_sort`, `title`, `thumb`, `description`, `content`, `siteid`, `positions`, `catalog`, `display`, `performance`, `technical`, `operation`, `cat_id`) VALUES
(12, 1, 'Calcium-Lignosulfonate3', '/Public/uploads/pics/2017-09/59b9eadda9290.jpg', 'The innovative CHRYSO® additives and admixtures are dedicated to the Construction professionals.\r\n                        They allow to:\r\n                        facilitate CEMENT production and enhance performance,\r\n                        improve CONCRETE performance, production,workability, aesthetics and sustainability,supply CONSTRUCTION SYSTEMS for complete building general construction, concrete repair, waterproofing and protection systems for new construction and rehabilitation & maintenance,\r\n                        enhance INDUSTRIAL APPLICATIONS performance and production (for plaster applications and oilfield).', 'Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1Calcium Lignosulfonate1', '1', '1', 'a:4:{i:0;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f61ba3b12.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"1";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:1;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f6231dec9.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"2";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:2;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f6233e229.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"3";s:4:"href";s:0:"";s:7:"content";s:0:"";}i:3;a:6:{s:3:"url";s:46:"/Public/uploads/pics/2017-09/59b9f623594be.jpg";s:4:"name";s:8:"product1";s:3:"alt";s:6:"测试";s:4:"sort";s:1:"4";s:4:"href";s:0:"";s:7:"content";s:0:"";}}', '', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">主要性能</span>', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#F3F3F3;">技术特点</span>', '<span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span><span style="font-family:黑体, 宋体, Arial;font-size:14px;line-height:18.2px;background-color:#FFFFFF;">操作方法</span>', 16);

-- --------------------------------------------------------

--
-- 表的结构 `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_sort` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `sex` tinyint(4) DEFAULT '0',
  `birth_date` varchar(150) NOT NULL DEFAULT '',
  `healthy` varchar(255) NOT NULL DEFAULT '',
  `identity` varchar(255) NOT NULL DEFAULT '',
  `is_in_foshan` tinyint(4) DEFAULT '0',
  `street` int(11) DEFAULT '0',
  `street_desc` varchar(255) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `graduate_province` int(11) DEFAULT '0',
  `graduate_street` int(11) DEFAULT '0',
  `graduate_school` int(11) DEFAULT '0',
  `graduate_class` int(11) DEFAULT '0',
  `graduate_school_desc` varchar(255) NOT NULL DEFAULT '',
  `register_num` varchar(255) NOT NULL DEFAULT '',
  `father` varchar(255) NOT NULL DEFAULT '',
  `father_tell` varchar(255) NOT NULL DEFAULT '',
  `father_address` varchar(255) NOT NULL DEFAULT '',
  `mother` varchar(255) NOT NULL DEFAULT '',
  `mother_tell` varchar(255) NOT NULL DEFAULT '',
  `mother_address` varchar(255) NOT NULL DEFAULT '',
  `school_idea` int(11) DEFAULT '0',
  `first_choice` int(11) DEFAULT '0',
  `second_choice` int(11) DEFAULT '0',
  `student_desc` text NOT NULL,
  `add_phone` varchar(255) NOT NULL DEFAULT '',
  `add_openid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `student`
--

INSERT INTO `student` (`student_id`, `student_sort`, `name`, `sex`, `birth_date`, `healthy`, `identity`, `is_in_foshan`, `street`, `street_desc`, `photo`, `address`, `graduate_province`, `graduate_street`, `graduate_school`, `graduate_class`, `graduate_school_desc`, `register_num`, `father`, `father_tell`, `father_address`, `mother`, `mother_tell`, `mother_address`, `school_idea`, `first_choice`, `second_choice`, `student_desc`, `add_phone`, `add_openid`) VALUES
(1, 0, 'fdfdf', 0, '', '', '', 0, 0, '', '', '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(2, 0, 'fdfdf', 0, '', '', '', 0, 0, '', '', '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `sub`
--

CREATE TABLE IF NOT EXISTS `sub` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `Sub_sort` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `advantage` text NOT NULL,
  `environment` varchar(255) NOT NULL DEFAULT '',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `sub`
--

INSERT INTO `sub` (`sub_id`, `Sub_sort`, `title`, `content`, `advantage`, `environment`, `cat_id`, `thumb`) VALUES
(1, 0, 'Maoming Hongfu Chemical Company Limited', '<p>\r\n	<img src="/Public/uploads/pics/2017-09/59ba2e79eaf00.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<span style="color:#666666;font-family:''Segoe UI'', ''Lucida Grande'', Helvetica, Arial, ''Microsoft YaHei'', FreeSans, Arimo, ''Droid Sans'', ''wenquanyi micro hei'', ''Hiragino Sans GB'', ''Hiragino Sans GB W3'', FontAwesome, sans-serif;font-size:16px;line-height:30px;background-color:#F5F5F5;">Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes in researching and manufacturing sodium lignosulfonate. With rich domestic marketing experience, Our products are hot sale in India, South Korea,Saudi Arabia,etc.Our hongfu brand sodium lignosulfonate HF-6, HF-7, HF-8 ,HF-10 have a good reputation among customers. In March, 2014, Foshan Junda Import &amp; Export Company Limited.</span> \r\n</p>', 'Luohe Hongfu Chemical Company Limited is the leading company of Hongfu Group,which was established in 2007and specializes in researching and manufacturing sodium lignosulfonate. With rich domestic marketing experience, Our products are hot sale in India, South Korea, Saudi Arabia, etc. Our hongfu brand sodium lignosulfonate HF-6, HF-7, HF-8 ,HF-10 have a good reputation among customers. In March, 20142', '/Public/uploads/pics/2017-09/59ba2ea6d93c6.jpg', 13, '/Public/uploads/pics/2017-09/59ba2ee63b443.jpg'),
(2, 0, 'Luohe Hongfu Chemical Company Limited', '', '', '', 13, '/Public/uploads/pics/2017-09/59ba30bc042f6.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `website`
--

CREATE TABLE IF NOT EXISTS `website` (
  `website_id` int(11) NOT NULL AUTO_INCREMENT,
  `website_sort` int(11) NOT NULL DEFAULT '0',
  `webname` varchar(255) NOT NULL DEFAULT '',
  `weburl` varchar(255) NOT NULL DEFAULT '',
  `siteUrl` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `lang` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`website_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `website`
--

INSERT INTO `website` (`website_id`, `website_sort`, `webname`, `weburl`, `siteUrl`, `thumb`, `lang`) VALUES
(1, 1431670216, '中文', 'default', 'http://local-haichuan', '/Public/uploads/pics/2017-07/5976bf4db7d4f.png', 'chinese'),
(2, 1432015785, 'english', 'default', 'http://local-haichuan', '/Public/uploads/pics/2017-09/59b2c40f7270e.png', 'english');

-- --------------------------------------------------------

--
-- 表的结构 `web_baseinfo`
--

CREATE TABLE IF NOT EXISTS `web_baseinfo` (
  `web_baseinfo_id` int(11) NOT NULL AUTO_INCREMENT,
  `web_baseinfo_sort` int(11) NOT NULL DEFAULT '0',
  `info_key` varchar(255) NOT NULL DEFAULT '',
  `info_val` varchar(255) NOT NULL DEFAULT '',
  `siteid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`web_baseinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `web_baseinfo`
--

INSERT INTO `web_baseinfo` (`web_baseinfo_id`, `web_baseinfo_sort`, `info_key`, `info_val`, `siteid`) VALUES
(1, 1431999484, 'title', '佛山市宏渤陶瓷有限公司 HONGBO CERAMICS CO.,LTD.', '1'),
(2, 5, 'description', '佛山市宏渤陶瓷有限公司专注于为全世界的瓷砖采购商提供优质的产品和服务以及满足客户的广泛需求。 宏渤陶瓷有限公司自成立以来秉承诚信经营和与时俱进的理念，在多次技术更新与改良后我司已能够满足产品的多样性以及给客户提供多元化的选择。如抛光砖、全抛釉、仿古砖，微晶石都远销欧洲，美洲，非洲，中东，东南亚以及东欧等国家。', '1'),
(3, 1431999505, 'keywords', '佛山陶瓷，陶瓷进出口，抛光砖，仿古砖，微晶石，全抛釉，水泥砖，佛山抛光砖，室内外瓷片。', '1'),
(4, 4, 'title', '佛山市宏渤陶瓷有限公司 HONGBO CERAMICS CO.,LTD.', '3'),
(5, 5, 'description', '佛山市宏渤陶瓷有限公司专注于为全世界的瓷砖采购商提供优质的产品和服务以及满足客户的广泛需求。 宏渤陶瓷有限公司自成立以来秉承诚信经营和与时俱进的理念，在多次技术更新与改良后我司已能够满足产品的多样性以及给客户提供多元化的选择。如抛光砖、全抛釉、仿古砖，微晶石都远销欧洲，美洲，非洲，中东，东南亚以及东欧等国家。', '3'),
(6, 6, 'keywords', '佛山陶瓷，陶瓷进出口，抛光砖，仿古砖，微晶石，全抛釉，水泥砖，佛山抛光砖，室内外瓷片。', '3');
