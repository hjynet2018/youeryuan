<?php
/**
 * TOP API: alibaba.mydata.self.keyword.date.get request
 * 
 * @author auto create
 * @since 1.0, 2016.05.26
 */
class AlibabaMydataSelfKeywordDateGetRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "alibaba.mydata.self.keyword.date.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
