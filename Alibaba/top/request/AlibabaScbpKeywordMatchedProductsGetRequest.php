<?php
/**
 * TOP API: alibaba.scbp.keyword.matched.products.get request
 * 
 * @author auto create
 * @since 1.0, 2017.10.12
 */
class AlibabaScbpKeywordMatchedProductsGetRequest
{
	/** 
	 * 已购买的关键词
	 **/
	private $adKeyword;
	
	private $apiParas = array();
	
	public function setAdKeyword($adKeyword)
	{
		$this->adKeyword = $adKeyword;
		$this->apiParas["ad_keyword"] = $adKeyword;
	}

	public function getAdKeyword()
	{
		return $this->adKeyword;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.keyword.matched.products.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->adKeyword,"adKeyword");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
