<?php
/**
 * TOP API: alibaba.scbp.ad.keyword.batchdelete request
 * 
 * @author auto create
 * @since 1.0, 2017.08.17
 */
class AlibabaScbpAdKeywordBatchdeleteRequest
{
	/** 
	 * 关键词Id列表
	 **/
	private $keywordIdList;
	
	private $apiParas = array();
	
	public function setKeywordIdList($keywordIdList)
	{
		$this->keywordIdList = $keywordIdList;
		$this->apiParas["keyword_id_list"] = $keywordIdList;
	}

	public function getKeywordIdList()
	{
		return $this->keywordIdList;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.keyword.batchdelete";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkMaxListSize($this->keywordIdList,20,"keywordIdList");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
