<?php
/**
 * TOP API: alibaba.scbp.account.status.update request
 * 
 * @author auto create
 * @since 1.0, 2016.02.29
 */
class AlibabaScbpAccountStatusUpdateRequest
{
	/** 
	 * on:开启,off:暂停
	 **/
	private $status;
	
	private $apiParas = array();
	
	public function setStatus($status)
	{
		$this->status = $status;
		$this->apiParas["status"] = $status;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.account.status.update";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->status,"status");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
