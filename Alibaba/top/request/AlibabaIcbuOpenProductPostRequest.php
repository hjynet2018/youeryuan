<?php
/**
 * TOP API: alibaba.icbu.open.product.post request
 * 
 * @author auto create
 * @since 1.0, 2015.08.31
 */
class AlibabaIcbuOpenProductPostRequest
{
	/** 
	 * product input param
	 **/
	private $paramProductPost;
	
	private $apiParas = array();
	
	public function setParamProductPost($paramProductPost)
	{
		$this->paramProductPost = $paramProductPost;
		$this->apiParas["param_product_post"] = $paramProductPost;
	}

	public function getParamProductPost()
	{
		return $this->paramProductPost;
	}

	public function getApiMethodName()
	{
		return "alibaba.icbu.open.product.post";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
