<?php
/**
 * TOP API: alibaba.scbp.ad.keyword.tag.update request
 * 
 * @author auto create
 * @since 1.0, 2016.02.29
 */
class AlibabaScbpAdKeywordTagUpdateRequest
{
	/** 
	 * 关键词ID列表
	 **/
	private $keywordIdList;
	
	/** 
	 * 关键词分组ID,不传表示取消关键词的分组
	 **/
	private $tagIdList;
	
	private $apiParas = array();
	
	public function setKeywordIdList($keywordIdList)
	{
		$this->keywordIdList = $keywordIdList;
		$this->apiParas["keyword_id_list"] = $keywordIdList;
	}

	public function getKeywordIdList()
	{
		return $this->keywordIdList;
	}

	public function setTagIdList($tagIdList)
	{
		$this->tagIdList = $tagIdList;
		$this->apiParas["tag_id_list"] = $tagIdList;
	}

	public function getTagIdList()
	{
		return $this->tagIdList;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.keyword.tag.update";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->keywordIdList,"keywordIdList");
		RequestCheckUtil::checkMaxListSize($this->keywordIdList,50,"keywordIdList");
		RequestCheckUtil::checkMaxListSize($this->tagIdList,50,"tagIdList");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
