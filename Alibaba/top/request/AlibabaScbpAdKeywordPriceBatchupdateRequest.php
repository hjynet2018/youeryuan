<?php
/**
 * TOP API: alibaba.scbp.ad.keyword.price.batchupdate request
 * 
 * @author auto create
 * @since 1.0, 2017.10.12
 */
class AlibabaScbpAdKeywordPriceBatchupdateRequest
{
	/** 
	 * 系统自动生成
	 **/
	private $keywordUpdateDtoList;
	
	private $apiParas = array();
	
	public function setKeywordUpdateDtoList($keywordUpdateDtoList)
	{
		$this->keywordUpdateDtoList = $keywordUpdateDtoList;
		$this->apiParas["keyword_update_dto_list"] = $keywordUpdateDtoList;
	}

	public function getKeywordUpdateDtoList()
	{
		return $this->keywordUpdateDtoList;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.keyword.price.batchupdate";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
