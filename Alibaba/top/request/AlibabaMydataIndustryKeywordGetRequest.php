<?php
/**
 * TOP API: alibaba.mydata.industry.keyword.get request
 * 
 * @author auto create
 * @since 1.0, 2017.10.17
 */
class AlibabaMydataIndustryKeywordGetRequest
{
	/** 
	 * 查询词列表，模糊匹配最多3个，精确匹配最多10个
	 **/
	private $keywords;
	
	/** 
	 * 查询选项
	 **/
	private $properties;
	
	private $apiParas = array();
	
	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;
		$this->apiParas["keywords"] = $keywords;
	}

	public function getKeywords()
	{
		return $this->keywords;
	}

	public function setProperties($properties)
	{
		$this->properties = $properties;
		$this->apiParas["properties"] = $properties;
	}

	public function getProperties()
	{
		return $this->properties;
	}

	public function getApiMethodName()
	{
		return "alibaba.mydata.industry.keyword.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->keywords,"keywords");
		RequestCheckUtil::checkMaxListSize($this->keywords,3,"keywords");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
