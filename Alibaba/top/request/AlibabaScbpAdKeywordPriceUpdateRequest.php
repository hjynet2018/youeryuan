<?php
/**
 * TOP API: alibaba.scbp.ad.keyword.price.update request
 * 
 * @author auto create
 * @since 1.0, 2017.08.17
 */
class AlibabaScbpAdKeywordPriceUpdateRequest
{
	/** 
	 * 只能取ascci字符
	 **/
	private $adKeyword;
	
	/** 
	 * 关键词价格单位元，一位小数
	 **/
	private $priceStr;
	
	private $apiParas = array();
	
	public function setAdKeyword($adKeyword)
	{
		$this->adKeyword = $adKeyword;
		$this->apiParas["ad_keyword"] = $adKeyword;
	}

	public function getAdKeyword()
	{
		return $this->adKeyword;
	}

	public function setPriceStr($priceStr)
	{
		$this->priceStr = $priceStr;
		$this->apiParas["price_str"] = $priceStr;
	}

	public function getPriceStr()
	{
		return $this->priceStr;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.keyword.price.update";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->adKeyword,"adKeyword");
		RequestCheckUtil::checkNotNull($this->priceStr,"priceStr");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
