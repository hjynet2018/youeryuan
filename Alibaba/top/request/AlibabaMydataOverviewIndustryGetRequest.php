<?php
/**
 * TOP API: alibaba.mydata.overview.industry.get request
 * 
 * @author auto create
 * @since 1.0, 2016.05.25
 */
class AlibabaMydataOverviewIndustryGetRequest
{
	/** 
	 * 系统自动生成
	 **/
	private $dateRange;
	
	private $apiParas = array();
	
	public function setDateRange($dateRange)
	{
		$this->dateRange = $dateRange;
		$this->apiParas["date_range"] = $dateRange;
	}

	public function getDateRange()
	{
		return $this->dateRange;
	}

	public function getApiMethodName()
	{
		return "alibaba.mydata.overview.industry.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
