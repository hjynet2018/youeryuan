<?php
/**
 * TOP API: alibaba.scbp.effect.product.report request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpEffectProductReportRequest
{
	/** 
	 * ProductQuery
	 **/
	private $p4pProductReportQuery;
	
	private $apiParas = array();
	
	public function setP4pProductReportQuery($p4pProductReportQuery)
	{
		$this->p4pProductReportQuery = $p4pProductReportQuery;
		$this->apiParas["p4p_product_report_query"] = $p4pProductReportQuery;
	}

	public function getP4pProductReportQuery()
	{
		return $this->p4pProductReportQuery;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.effect.product.report";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
