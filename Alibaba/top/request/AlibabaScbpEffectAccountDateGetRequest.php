<?php
/**
 * TOP API: alibaba.scbp.effect.account.date.get request
 * 
 * @author auto create
 * @since 1.0, 2015.08.06
 */
class AlibabaScbpEffectAccountDateGetRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "alibaba.scbp.effect.account.date.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
