<?php
/**
 * TOP API: alibaba.scbp.product.preferential.update request
 * 
 * @author auto create
 * @since 1.0, 2016.02.29
 */
class AlibabaScbpProductPreferentialUpdateRequest
{
	/** 
	 * 关键词ID
	 **/
	private $keywordId;
	
	/** 
	 * 产品ID
	 **/
	private $productId;
	
	/** 
	 * Y:设置优推,N:取消优推
	 **/
	private $status;
	
	private $apiParas = array();
	
	public function setKeywordId($keywordId)
	{
		$this->keywordId = $keywordId;
		$this->apiParas["keyword_id"] = $keywordId;
	}

	public function getKeywordId()
	{
		return $this->keywordId;
	}

	public function setProductId($productId)
	{
		$this->productId = $productId;
		$this->apiParas["product_id"] = $productId;
	}

	public function getProductId()
	{
		return $this->productId;
	}

	public function setStatus($status)
	{
		$this->status = $status;
		$this->apiParas["status"] = $status;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.product.preferential.update";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->keywordId,"keywordId");
		RequestCheckUtil::checkNotNull($this->productId,"productId");
		RequestCheckUtil::checkNotNull($this->status,"status");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
