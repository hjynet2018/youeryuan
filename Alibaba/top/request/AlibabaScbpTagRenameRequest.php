<?php
/**
 * TOP API: alibaba.scbp.tag.rename request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpTagRenameRequest
{
	/** 
	 * 新分组名
	 **/
	private $newTagName;
	
	/** 
	 * 需要重命名的关键词分组名
	 **/
	private $tagName;
	
	private $apiParas = array();
	
	public function setNewTagName($newTagName)
	{
		$this->newTagName = $newTagName;
		$this->apiParas["new_tag_name"] = $newTagName;
	}

	public function getNewTagName()
	{
		return $this->newTagName;
	}

	public function setTagName($tagName)
	{
		$this->tagName = $tagName;
		$this->apiParas["tag_name"] = $tagName;
	}

	public function getTagName()
	{
		return $this->tagName;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.tag.rename";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->newTagName,"newTagName");
		RequestCheckUtil::checkNotNull($this->tagName,"tagName");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
