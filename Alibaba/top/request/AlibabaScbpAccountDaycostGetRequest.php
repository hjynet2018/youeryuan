<?php
/**
 * TOP API: alibaba.scbp.account.daycost.get request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpAccountDaycostGetRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "alibaba.scbp.account.daycost.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
