<?php
/**
 * TOP API: alibaba.scbp.account.quickstatus.update request
 * 
 * @author auto create
 * @since 1.0, 2016.03.02
 */
class AlibabaScbpAccountQuickstatusUpdateRequest
{
	/** 
	 * on:开启,off:暂停
	 **/
	private $paramString;
	
	private $apiParas = array();
	
	public function setParamString($paramString)
	{
		$this->paramString = $paramString;
		$this->apiParas["param_string"] = $paramString;
	}

	public function getParamString()
	{
		return $this->paramString;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.account.quickstatus.update";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->paramString,"paramString");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
