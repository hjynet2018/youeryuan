<?php
/**
 * TOP API: alibaba.scbp.tag.add request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpTagAddRequest
{
	/** 
	 * 分组名称，最多允许创建100个
	 **/
	private $tagName;
	
	private $apiParas = array();
	
	public function setTagName($tagName)
	{
		$this->tagName = $tagName;
		$this->apiParas["tag_name"] = $tagName;
	}

	public function getTagName()
	{
		return $this->tagName;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.tag.add";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->tagName,"tagName");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
