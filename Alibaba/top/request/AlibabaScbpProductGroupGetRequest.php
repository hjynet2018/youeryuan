<?php
/**
 * TOP API: alibaba.scbp.product.group.get request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpProductGroupGetRequest
{
	/** 
	 * 产品分组标识，null表示查询第一层分组
	 **/
	private $groupId;
	
	private $apiParas = array();
	
	public function setGroupId($groupId)
	{
		$this->groupId = $groupId;
		$this->apiParas["group_id"] = $groupId;
	}

	public function getGroupId()
	{
		return $this->groupId;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.product.group.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
