<?php
/**
 * TOP API: alibaba.scbp.account.status.get request
 * 
 * @author auto create
 * @since 1.0, 2016.02.29
 */
class AlibabaScbpAccountStatusGetRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "alibaba.scbp.account.status.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
