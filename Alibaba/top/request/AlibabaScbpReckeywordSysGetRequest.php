<?php
/**
 * TOP API: alibaba.scbp.reckeyword.sys.get request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpReckeywordSysGetRequest
{
	/** 
	 * RecKeywordQuery
	 **/
	private $queryDto;
	
	private $apiParas = array();
	
	public function setQueryDto($queryDto)
	{
		$this->queryDto = $queryDto;
		$this->apiParas["query_dto"] = $queryDto;
	}

	public function getQueryDto()
	{
		return $this->queryDto;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.reckeyword.sys.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
