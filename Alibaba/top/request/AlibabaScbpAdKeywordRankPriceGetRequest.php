<?php
/**
 * TOP API: alibaba.scbp.ad.keyword.rank.price.get request
 * 
 * @author auto create
 * @since 1.0, 2017.10.12
 */
class AlibabaScbpAdKeywordRankPriceGetRequest
{
	/** 
	 * 关键词
	 **/
	private $keyword;
	
	private $apiParas = array();
	
	public function setKeyword($keyword)
	{
		$this->keyword = $keyword;
		$this->apiParas["keyword"] = $keyword;
	}

	public function getKeyword()
	{
		return $this->keyword;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.keyword.rank.price.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->keyword,"keyword");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
