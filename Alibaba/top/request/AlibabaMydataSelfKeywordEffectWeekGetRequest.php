<?php
/**
 * TOP API: alibaba.mydata.self.keyword.effect.week.get request
 * 
 * @author auto create
 * @since 1.0, 2016.05.25
 */
class AlibabaMydataSelfKeywordEffectWeekGetRequest
{
	/** 
	 * 要查询的数据周期
	 **/
	private $dateRange;
	
	/** 
	 * 查询选项
	 **/
	private $properties;
	
	private $apiParas = array();
	
	public function setDateRange($dateRange)
	{
		$this->dateRange = $dateRange;
		$this->apiParas["date_range"] = $dateRange;
	}

	public function getDateRange()
	{
		return $this->dateRange;
	}

	public function setProperties($properties)
	{
		$this->properties = $properties;
		$this->apiParas["properties"] = $properties;
	}

	public function getProperties()
	{
		return $this->properties;
	}

	public function getApiMethodName()
	{
		return "alibaba.mydata.self.keyword.effect.week.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
