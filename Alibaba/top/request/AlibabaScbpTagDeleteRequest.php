<?php
/**
 * TOP API: alibaba.scbp.tag.delete request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpTagDeleteRequest
{
	/** 
	 * 关键词分组名
	 **/
	private $tagName;
	
	private $apiParas = array();
	
	public function setTagName($tagName)
	{
		$this->tagName = $tagName;
		$this->apiParas["tag_name"] = $tagName;
	}

	public function getTagName()
	{
		return $this->tagName;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.tag.delete";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->tagName,"tagName");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
