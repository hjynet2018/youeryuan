<?php
/**
 * TOP API: alibaba.mydata.self.keyword.effect.month.get request
 * 
 * @author auto create
 * @since 1.0, 2016.05.25
 */
class AlibabaMydataSelfKeywordEffectMonthGetRequest
{
	/** 
	 * 查询选项
	 **/
	private $properties;
	
	private $apiParas = array();
	
	public function setProperties($properties)
	{
		$this->properties = $properties;
		$this->apiParas["properties"] = $properties;
	}

	public function getProperties()
	{
		return $this->properties;
	}

	public function getApiMethodName()
	{
		return "alibaba.mydata.self.keyword.effect.month.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
