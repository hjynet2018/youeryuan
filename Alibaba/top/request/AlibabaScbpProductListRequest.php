<?php
/**
 * TOP API: alibaba.scbp.product.list request
 * 
 * @author auto create
 * @since 1.0, 2017.10.11
 */
class AlibabaScbpProductListRequest
{
	/** 
	 * 产品分组标识
	 **/
	private $groupId;
	
	/** 
	 * 产品分页查询，每页个数，最大值20
	 **/
	private $perPageSize;
	
	/** 
	 * 第几页
	 **/
	private $toPage;
	
	private $apiParas = array();
	
	public function setGroupId($groupId)
	{
		$this->groupId = $groupId;
		$this->apiParas["group_id"] = $groupId;
	}

	public function getGroupId()
	{
		return $this->groupId;
	}

	public function setPerPageSize($perPageSize)
	{
		$this->perPageSize = $perPageSize;
		$this->apiParas["per_page_size"] = $perPageSize;
	}

	public function getPerPageSize()
	{
		return $this->perPageSize;
	}

	public function setToPage($toPage)
	{
		$this->toPage = $toPage;
		$this->apiParas["to_page"] = $toPage;
	}

	public function getToPage()
	{
		return $this->toPage;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.product.list";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->groupId,"groupId");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
