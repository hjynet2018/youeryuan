<?php
/**
 * TOP API: alibaba.scbp.effect.keyword.list request
 * 
 * @author auto create
 * @since 1.0, 2016.04.14
 */
class AlibabaScbpEffectKeywordListRequest
{
	/** 
	 * IKeywordQuery
	 **/
	private $p4pKeywordReportQuery;
	
	private $apiParas = array();
	
	public function setP4pKeywordReportQuery($p4pKeywordReportQuery)
	{
		$this->p4pKeywordReportQuery = $p4pKeywordReportQuery;
		$this->apiParas["p4p_keyword_report_query"] = $p4pKeywordReportQuery;
	}

	public function getP4pKeywordReportQuery()
	{
		return $this->p4pKeywordReportQuery;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.effect.keyword.list";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
