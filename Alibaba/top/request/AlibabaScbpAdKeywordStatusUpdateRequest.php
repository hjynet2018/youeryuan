<?php
/**
 * TOP API: alibaba.scbp.ad.keyword.status.update request
 * 
 * @author auto create
 * @since 1.0, 2015.09.11
 */
class AlibabaScbpAdKeywordStatusUpdateRequest
{
	/** 
	 * 只能取ascci字符
	 **/
	private $adKeyword;
	
	/** 
	 * 只能去in_promotion或者stopped
	 **/
	private $status;
	
	private $apiParas = array();
	
	public function setAdKeyword($adKeyword)
	{
		$this->adKeyword = $adKeyword;
		$this->apiParas["ad_keyword"] = $adKeyword;
	}

	public function getAdKeyword()
	{
		return $this->adKeyword;
	}

	public function setStatus($status)
	{
		$this->status = $status;
		$this->apiParas["status"] = $status;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.keyword.status.update";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->adKeyword,"adKeyword");
		RequestCheckUtil::checkMaxLength($this->adKeyword,64,"adKeyword");
		RequestCheckUtil::checkNotNull($this->status,"status");
		RequestCheckUtil::checkMaxLength($this->status,16,"status");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
