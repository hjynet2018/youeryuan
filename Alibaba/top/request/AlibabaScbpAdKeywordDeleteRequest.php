<?php
/**
 * TOP API: alibaba.scbp.ad.keyword.delete request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpAdKeywordDeleteRequest
{
	/** 
	 * 要删除的关键词
	 **/
	private $adKeyword;
	
	private $apiParas = array();
	
	public function setAdKeyword($adKeyword)
	{
		$this->adKeyword = $adKeyword;
		$this->apiParas["ad_keyword"] = $adKeyword;
	}

	public function getAdKeyword()
	{
		return $this->adKeyword;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.keyword.delete";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->adKeyword,"adKeyword");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
