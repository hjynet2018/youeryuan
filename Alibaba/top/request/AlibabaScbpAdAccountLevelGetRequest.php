<?php
/**
 * TOP API: alibaba.scbp.ad.account.level.get request
 * 
 * @author auto create
 * @since 1.0, 2017.08.18
 */
class AlibabaScbpAdAccountLevelGetRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.account.level.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
