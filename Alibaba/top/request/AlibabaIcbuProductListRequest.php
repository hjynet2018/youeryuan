<?php
/**
 * TOP API: alibaba.icbu.product.list request
 * 
 * @author auto create
 * @since 1.0, 2017.07.27
 */
class AlibabaIcbuProductListRequest
{
	/** 
	 * 类目ID
	 **/
	private $categoryId;
	
	/** 
	 * 当前页
	 **/
	private $currentPage;
	
	/** 
	 * 商品语种，目前只支持ENGLISH
	 **/
	private $language;
	
	/** 
	 * 每页大小，最大30
	 **/
	private $pageSize;
	
	/** 
	 * 商品名称，支持模糊匹配
	 **/
	private $subject;
	
	private $apiParas = array();
	
	public function setCategoryId($categoryId)
	{
		$this->categoryId = $categoryId;
		$this->apiParas["category_id"] = $categoryId;
	}

	public function getCategoryId()
	{
		return $this->categoryId;
	}

	public function setCurrentPage($currentPage)
	{
		$this->currentPage = $currentPage;
		$this->apiParas["current_page"] = $currentPage;
	}

	public function getCurrentPage()
	{
		return $this->currentPage;
	}

	public function setLanguage($language)
	{
		$this->language = $language;
		$this->apiParas["language"] = $language;
	}

	public function getLanguage()
	{
		return $this->language;
	}

	public function setPageSize($pageSize)
	{
		$this->pageSize = $pageSize;
		$this->apiParas["page_size"] = $pageSize;
	}

	public function getPageSize()
	{
		return $this->pageSize;
	}

	public function setSubject($subject)
	{
		$this->subject = $subject;
		$this->apiParas["subject"] = $subject;
	}

	public function getSubject()
	{
		return $this->subject;
	}

	public function getApiMethodName()
	{
		return "alibaba.icbu.product.list";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->language,"language");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
