<?php
/**
 * TOP API: alibaba.buyer.companyprofile.find request
 * 
 * @author auto create
 * @since 1.0, 2015.07.30
 */
class AlibabaBuyerCompanyprofileFindRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "alibaba.buyer.companyprofile.find";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
