<?php
/**
 * TOP API: alibaba.member.basicprofile.find request
 * 
 * @author auto create
 * @since 1.0, 2015.07.30
 */
class AlibabaMemberBasicprofileFindRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "alibaba.member.basicprofile.find";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
