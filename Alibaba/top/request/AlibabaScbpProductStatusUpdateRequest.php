<?php
/**
 * TOP API: alibaba.scbp.product.status.update request
 * 
 * @author auto create
 * @since 1.0, 2016.02.29
 */
class AlibabaScbpProductStatusUpdateRequest
{
	/** 
	 * 产品ID列表
	 **/
	private $productIdList;
	
	/** 
	 * enabled:开启,disabled:暂停
	 **/
	private $status;
	
	private $apiParas = array();
	
	public function setProductIdList($productIdList)
	{
		$this->productIdList = $productIdList;
		$this->apiParas["product_id_list"] = $productIdList;
	}

	public function getProductIdList()
	{
		return $this->productIdList;
	}

	public function setStatus($status)
	{
		$this->status = $status;
		$this->apiParas["status"] = $status;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.product.status.update";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->productIdList,"productIdList");
		RequestCheckUtil::checkMaxListSize($this->productIdList,50,"productIdList");
		RequestCheckUtil::checkNotNull($this->status,"status");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
