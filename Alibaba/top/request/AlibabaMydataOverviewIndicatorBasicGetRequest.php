<?php
/**
 * TOP API: alibaba.mydata.overview.indicator.basic.get request
 * 
 * @author auto create
 * @since 1.0, 2016.05.25
 */
class AlibabaMydataOverviewIndicatorBasicGetRequest
{
	/** 
	 * 要查询的数据周期
	 **/
	private $dateRange;
	
	/** 
	 * 要查询的行业信息
	 **/
	private $industry;
	
	private $apiParas = array();
	
	public function setDateRange($dateRange)
	{
		$this->dateRange = $dateRange;
		$this->apiParas["date_range"] = $dateRange;
	}

	public function getDateRange()
	{
		return $this->dateRange;
	}

	public function setIndustry($industry)
	{
		$this->industry = $industry;
		$this->apiParas["industry"] = $industry;
	}

	public function getIndustry()
	{
		return $this->industry;
	}

	public function getApiMethodName()
	{
		return "alibaba.mydata.overview.indicator.basic.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
