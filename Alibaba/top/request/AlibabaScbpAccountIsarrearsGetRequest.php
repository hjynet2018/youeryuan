<?php
/**
 * TOP API: alibaba.scbp.account.isarrears.get request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpAccountIsarrearsGetRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "alibaba.scbp.account.isarrears.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
