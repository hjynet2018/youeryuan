<?php
/**
 * TOP API: alibaba.scbp.effect.account.list request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpEffectAccountListRequest
{
	/** 
	 * AccountQuery
	 **/
	private $p4pAccountReportQuery;
	
	private $apiParas = array();
	
	public function setP4pAccountReportQuery($p4pAccountReportQuery)
	{
		$this->p4pAccountReportQuery = $p4pAccountReportQuery;
		$this->apiParas["p4p_account_report_query"] = $p4pAccountReportQuery;
	}

	public function getP4pAccountReportQuery()
	{
		return $this->p4pAccountReportQuery;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.effect.account.list";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
