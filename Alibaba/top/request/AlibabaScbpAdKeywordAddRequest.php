<?php
/**
 * TOP API: alibaba.scbp.ad.keyword.add request
 * 
 * @author auto create
 * @since 1.0, 2015.08.10
 */
class AlibabaScbpAdKeywordAddRequest
{
	/** 
	 * 加入的词
	 **/
	private $adKeyword;
	
	/** 
	 * 词的出价
	 **/
	private $priceStr;
	
	/** 
	 * 分组名
	 **/
	private $tagName;
	
	private $apiParas = array();
	
	public function setAdKeyword($adKeyword)
	{
		$this->adKeyword = $adKeyword;
		$this->apiParas["ad_keyword"] = $adKeyword;
	}

	public function getAdKeyword()
	{
		return $this->adKeyword;
	}

	public function setPriceStr($priceStr)
	{
		$this->priceStr = $priceStr;
		$this->apiParas["price_str"] = $priceStr;
	}

	public function getPriceStr()
	{
		return $this->priceStr;
	}

	public function setTagName($tagName)
	{
		$this->tagName = $tagName;
		$this->apiParas["tag_name"] = $tagName;
	}

	public function getTagName()
	{
		return $this->tagName;
	}

	public function getApiMethodName()
	{
		return "alibaba.scbp.ad.keyword.add";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->adKeyword,"adKeyword");
		RequestCheckUtil::checkNotNull($this->priceStr,"priceStr");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
