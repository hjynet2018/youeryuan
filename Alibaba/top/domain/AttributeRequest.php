<?php

/**
 * 类目属性request对象
 * @author auto create
 */
class AttributeRequest
{
	
	/** 
	 * 属性id列表，若填写，只返回给定属性id信息
	 **/
	public $attr_id;
	
	/** 
	 * 必填；发布类目id
	 **/
	public $cat_id;	
}
?>