<?php

/**
 * 查询选项
 * @author auto create
 */
class HotKeywordProperties
{
	
	/** 
	 * 返回数据分页大小
	 **/
	public $limit;
	
	/** 
	 * 返回数据分页偏移
	 **/
	public $offset;
	
	/** 
	 * 排序字段，company_cnt:卖家竞争度；showwin_cnt:橱窗数；srh_pv_this_mon:搜索热度
	 **/
	public $order_by_field;
	
	/** 
	 * 排序方式，可选值：asc,desc
	 **/
	public $order_by_mode;
	
	/** 
	 * 是否进行精确匹配，没有匹配内容时不返回对应项目
	 **/
	public $precise_match;	
}
?>