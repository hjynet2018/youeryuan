<?php

/**
 * TopP4pProductDto
 * @author auto create
 */
class TopProductDto
{
	
	/** 
	 * 产品ID
	 **/
	public $product_id;
	
	/** 
	 * 产品推广状态，取值[disabled,enabled]
	 **/
	public $status;
	
	/** 
	 * 产品标题，最大长度256个字符
	 **/
	public $subject;	
}
?>