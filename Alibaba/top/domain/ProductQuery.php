<?php

/**
 * ProductQuery
 * @author auto create
 */
class ProductQuery
{
	
	/** 
	 * 开始时间 当inteval=7或30的时候 不需要填写
	 **/
	public $begin_date;
	
	/** 
	 * 结束时间 当inteval=7或30的时候 不需要填写
	 **/
	public $end_date;
	
	/** 
	 * 区间 只能为1 7 30
	 **/
	public $inteval;
	
	/** 
	 * 每页行数
	 **/
	public $per_page_size;
	
	/** 
	 * 产品ID
	 **/
	public $product_id;
	
	/** 
	 * 第几页
	 **/
	public $to_page;	
}
?>