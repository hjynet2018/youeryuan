<?php

/**
 * groups
 * @author auto create
 */
class PhotoAlbumGroup
{
	
	/** 
	 * id
	 **/
	public $id;
	
	/** 
	 * level1
	 **/
	public $level1;
	
	/** 
	 * level2
	 **/
	public $level2;
	
	/** 
	 * level3
	 **/
	public $level3;
	
	/** 
	 * name
	 **/
	public $name;	
}
?>