<?php

/**
 * 要查询的数据周期
 * @author auto create
 */
class DateRange
{
	
	/** 
	 * 数据周期结束时间（含）
	 **/
	public $end_date;
	
	/** 
	 * 数据周期开始时间（含）
	 **/
	public $start_date;	
}
?>