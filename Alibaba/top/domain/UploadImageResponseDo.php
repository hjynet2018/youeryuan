<?php

/**
 * 上传图片响应对象
 * @author auto create
 */
class UploadImageResponseDo
{
	
	/** 
	 * 生成的图片名称
	 **/
	public $file_name;
	
	/** 
	 * 生成的图片全路径URL
	 **/
	public $photobank_url;	
}
?>