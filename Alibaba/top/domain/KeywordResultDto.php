<?php

/**
 * 关键词信息
 * @author auto create
 */
class KeywordResultDto
{
	
	/** 
	 * 底价，单位元，保留一位小数, 例如3.5表示3.5元
	 **/
	public $base_price;
	
	/** 
	 * 购买竞争度[1-6]
	 **/
	public $buy_count;
	
	/** 
	 * 点击量
	 **/
	public $click_cnt;
	
	/** 
	 * 平均点击花费，单位元，保留两位小数, 例如3.75表示3.75元
	 **/
	public $click_cost_avg;
	
	/** 
	 * 花费，单位元，保留两位小数, 例如3.75表示3.75元
	 **/
	public $cost;
	
	/** 
	 * 点击率，百分比，保留两位小数，例如3.75表示3.75%
	 **/
	public $ctr;
	
	/** 
	 * 关键词id
	 **/
	public $id;
	
	/** 
	 * 曝光量
	 **/
	public $impression_cnt;
	
	/** 
	 * 和关键词有匹配且处于推广中的产品的个数
	 **/
	public $match_count;
	
	/** 
	 * 推广时长，单位小时，保留一位小数，例如13.5表示13.5小时，小于1小时返回&lt;1
	 **/
	public $online_time;
	
	/** 
	 * 出价，单位元，保留一位小数, 例如3.5表示3.5元
	 **/
	public $price;
	
	/** 
	 * 推广评分星级[0-5]
	 **/
	public $qs_star;
	
	/** 
	 * 搜索热度[1-6]
	 **/
	public $search_count;
	
	/** 
	 * 关键词推广状态,取值stopped和in_promotion
	 **/
	public $status;
	
	/** 
	 * 关键词所属分组名称列表
	 **/
	public $tag_list;
	
	/** 
	 * 关键词
	 **/
	public $word;
	
	/** 
	 * 关键词类型，recomm_word：系统推荐
	 **/
	public $word_type;	
}
?>