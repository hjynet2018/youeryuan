<?php

/**
 * 修改失败关键词列表
 * @author auto create
 */
class KeywordErrorResultDto
{
	
	/** 
	 * keywordId
	 **/
	public $keyword_id;
	
	/** 
	 * reason
	 **/
	public $reason;
	
	/** 
	 * value
	 **/
	public $value;	
}
?>