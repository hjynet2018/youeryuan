<?php

/**
 * RecKeywordDto
 * @author auto create
 */
class RecKeywordDto
{
	
	/** 
	 * 同行平均出价，单位元，一位小数
	 **/
	public $avg_price;
	
	/** 
	 * 底价，单位元，一位小数
	 **/
	public $base_price;
	
	/** 
	 * 购买竞争度[1-6]
	 **/
	public $buy_count;
	
	/** 
	 * 客户是否已经将该词添加到外贸直通车后台的出价列表中，取值'Y‘和'N’
	 **/
	public $is_added;
	
	/** 
	 * 与关键词匹配且处于推广中的产品的数量
	 **/
	public $match_count;
	
	/** 
	 * 关键词的推广评分[0-5]
	 **/
	public $qs_star;
	
	/** 
	 * 搜索热度[1-6]
	 **/
	public $search_count;
	
	/** 
	 * 词
	 **/
	public $word;
	
	/** 
	 * 推荐词类型
	 **/
	public $word_type;	
}
?>