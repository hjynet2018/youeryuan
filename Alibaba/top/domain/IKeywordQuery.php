<?php

/**
 * IKeywordQuery
 * @author auto create
 */
class IKeywordQuery
{
	
	/** 
	 * 开始时间 当inteval=7或者30的时候不需要填写
	 **/
	public $begin_date;
	
	/** 
	 * 结束时间 当inteval=7或者30的时候不需要填写
	 **/
	public $end_date;
	
	/** 
	 * 区间 只能为1 7 30
	 **/
	public $inteval;
	
	/** 
	 * 关键词
	 **/
	public $keyword;
	
	/** 
	 * 每页行数
	 **/
	public $per_page_size;
	
	/** 
	 * 第几页
	 **/
	public $to_page;	
}
?>