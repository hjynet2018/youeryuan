<?php

/**
 * 产品图片
 * @author auto create
 */
class ProductImage
{
	
	/** 
	 * 主图图片信息列表
	 **/
	public $image_file_list;
	
	/** 
	 * 图片是否有水印
	 **/
	public $image_watermark;
	
	/** 
	 * 水印样式
	 **/
	public $water_mark_style;	
}
?>