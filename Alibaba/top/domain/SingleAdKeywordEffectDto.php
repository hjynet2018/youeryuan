<?php

/**
 * SingleAdKeywordEffectDto
 * @author auto create
 */
class SingleAdKeywordEffectDto
{
	
	/** 
	 * 点击量
	 **/
	public $click_cnt;
	
	/** 
	 * 平均点击花费
	 **/
	public $click_cost_avg;
	
	/** 
	 * 单位元，保留两位小数
	 **/
	public $cost;
	
	/** 
	 * 百分比，保留两位小数，例如3.75表示3.75%
	 **/
	public $ctr;
	
	/** 
	 * 曝光
	 **/
	public $impression_cnt;
	
	/** 
	 * 关键词
	 **/
	public $keyword;
	
	/** 
	 * 推广时长 单元小时 一位小数
	 **/
	public $online_time;
	
	/** 
	 * 日期
	 **/
	public $stat_date;	
}
?>