<?php

/**
 * 词效果
 * @author auto create
 */
class EffectEntity
{
	
	/** 
	 * Top10平均点击
	 **/
	public $avg_sum_click_cnt;
	
	/** 
	 * Top10平均曝光
	 **/
	public $avg_sum_show_cnt;
	
	/** 
	 * 点击率，1代表100%
	 **/
	public $ctr;
	
	/** 
	 * 卖家竞争度
	 **/
	public $gs_tp_member_set_cnt;
	
	/** 
	 * 是否为外贸直通车推广词（离线）
	 **/
	public $is_p4p_kw;
	
	/** 
	 * 关键词
	 **/
	public $keyword;
	
	/** 
	 * 搜索热度
	 **/
	public $search_pv_index;
	
	/** 
	 * 点击量
	 **/
	public $sum_click_cnt;
	
	/** 
	 * 外贸直通车点击
	 **/
	public $sum_p4p_click_cnt;
	
	/** 
	 * 外贸直通车曝光
	 **/
	public $sum_p4p_show_cnt;
	
	/** 
	 * 曝光量
	 **/
	public $sum_show_cnt;	
}
?>