<?php

/**
 * 要查询的行业信息
 * @author auto create
 */
class Industry
{
	
	/** 
	 * 行业描述
	 **/
	public $industry_desc;
	
	/** 
	 * 行业ID
	 **/
	public $industry_id;
	
	/** 
	 * 是否主营行业
	 **/
	public $main_category;	
}
?>