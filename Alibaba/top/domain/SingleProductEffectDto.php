<?php

/**
 * SingleProductEffectDto
 * @author auto create
 */
class SingleProductEffectDto
{
	
	/** 
	 * 点击数
	 **/
	public $click_cnt;
	
	/** 
	 * 平均点击花费
	 **/
	public $click_cost_avg;
	
	/** 
	 * 单位元，保留两位小数, 例如3.75表示3.75元
	 **/
	public $cost;
	
	/** 
	 * 百分比，保留两位小数，例如3.75表示3.75%
	 **/
	public $ctr;
	
	/** 
	 * 曝光
	 **/
	public $impression_cnt;
	
	/** 
	 * 产品ID
	 **/
	public $product_id;
	
	/** 
	 * 日期
	 **/
	public $stat_date;
	
	/** 
	 * 产品title
	 **/
	public $subject;	
}
?>