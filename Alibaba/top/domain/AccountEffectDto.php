<?php

/**
 * AccountEffectDto
 * @author auto create
 */
class AccountEffectDto
{
	
	/** 
	 * 点击量
	 **/
	public $click_cnt;
	
	/** 
	 * 平均点击花费
	 **/
	public $click_cost_avg;
	
	/** 
	 * 话费 单位元
	 **/
	public $cost;
	
	/** 
	 * 点击率
	 **/
	public $ctr;
	
	/** 
	 * 曝光
	 **/
	public $impression_cnt;
	
	/** 
	 * 单位小时，保留一位小数，例如13.5表示13.5小时
	 **/
	public $online_time;
	
	/** 
	 * 报告时间
	 **/
	public $stat_date;	
}
?>