<?php

/**
 * 查询选项
 * @author auto create
 */
class MyKeywordProperties
{
	
	/** 
	 * 是否是外贸直通车推广词。可选值：ALL-所有；YES-只选择外贸直通车推广词；NO-排除外贸直通车推广词
	 **/
	public $is_p4p;
	
	/** 
	 * 要查询的关键词，不填时查收所有词
	 **/
	public $keyword;
	
	/** 
	 * 是否已设置为关键词。可选值：ALL-所有；YES-设置为关键词；NO-没有设置为关键词
	 **/
	public $keywords_in_use;
	
	/** 
	 * 是否有曝光。可选值：ALL-所有；YES-有曝光；NO-没有曝光
	 **/
	public $keywords_viewed;
	
	/** 
	 * 返回数据分页大小
	 **/
	public $limit;
	
	/** 
	 * 返回数据分页偏移
	 **/
	public $offset;
	
	/** 
	 * 排序字段，sumShowCnt-曝光量；sumClickCnt-点击量；ctr-点击率；sumP4pShowCnt-外贸直通车曝光；sumP4pClickCnt-外贸直通车点击；searchPvIndex-搜索热度；gsTpMemberSetCnt-卖家竞争度；avgSumShowCnt-Top10平均曝光；avgSumClickCnt-Top10平均点击；
	 **/
	public $order_by_field;
	
	/** 
	 * 排序方式，可选值：asc,desc
	 **/
	public $order_by_mode;	
}
?>