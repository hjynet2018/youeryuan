<?php

/**
 * 商品详细信息
 * @author auto create
 */
class AlibabaProductBriefResponse
{
	
	/** 
	 * 分组ID
	 **/
	public $group_id;
	
	/** 
	 * 分组名称
	 **/
	public $group_name;
	
	/** 
	 * 商品ID
	 **/
	public $id;
	
	/** 
	 * 关键词
	 **/
	public $keywords;
	
	/** 
	 * 语种
	 **/
	public $language;
	
	/** 
	 * 商品的主图
	 **/
	public $main_image;
	
	/** 
	 * 混淆后的商品ID
	 **/
	public $product_id;
	
	/** 
	 * 商品类型
	 **/
	public $product_type;
	
	/** 
	 * 商品状态
	 **/
	public $status;
	
	/** 
	 * 商品名称
	 **/
	public $subject;	
}
?>