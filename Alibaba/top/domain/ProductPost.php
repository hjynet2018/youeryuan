<?php

/**
 * 商品开放平台入参
 * @author auto create
 */
class ProductPost
{
	
	/** 
	 * 用户ID
	 **/
	public $account_id;
	
	/** 
	 * 用户AliId
	 **/
	public $ali_id;
	
	/** 
	 * AppKey
	 **/
	public $app_key;
	
	/** 
	 * 类目ID
	 **/
	public $category_id;
	
	/** 
	 * 产品描述
	 **/
	public $description;
	
	/** 
	 * 扩展信息, 如ICVID
	 **/
	public $extra_context;
	
	/** 
	 * 产品组ID
	 **/
	public $group_id;
	
	/** 
	 * 关键字
	 **/
	public $keywords;
	
	/** 
	 * 产品ID
	 **/
	public $product_id;
	
	/** 
	 * 商品图片对象
	 **/
	public $product_image;
	
	/** 
	 * 商品交易信息对象
	 **/
	public $product_trade;
	
	/** 
	 * 商品属性对象
	 **/
	public $properties;
	
	/** 
	 * 产品主题
	 **/
	public $subject;	
}
?>