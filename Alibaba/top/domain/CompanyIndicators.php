<?php

/**
 * 公司询盘流量指标
 * @author auto create
 */
class CompanyIndicators
{
	
	/** 
	 * 点击
	 **/
	public $clk;
	
	/** 
	 * 点击率
	 **/
	public $clk_rate;
	
	/** 
	 * 反馈（询盘）
	 **/
	public $fb;
	
	/** 
	 * 曝光
	 **/
	public $imps;
	
	/** 
	 * 最近30天询盘一次回复率
	 **/
	public $reply;
	
	/** 
	 * 访客
	 **/
	public $visitor;	
}
?>