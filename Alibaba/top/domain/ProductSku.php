<?php

/**
 * 商品SKU定义
 * @author auto create
 */
class ProductSku
{
	
	/** 
	 * 商品属性和属性值
	 **/
	public $attributes;
	
	/** 
	 * 单个SKU详细定义
	 **/
	public $exclude_skus;
	
	/** 
	 * 单个SKU详细定义
	 **/
	public $special_skus;	
}
?>