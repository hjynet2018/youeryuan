<?php

/**
 * 属性下的值
 * @author auto create
 */
class SkuAttributeValue
{
	
	/** 
	 * 自定义的属性值名称
	 **/
	public $custom_value_name;
	
	/** 
	 * 自定义的图片URL
	 **/
	public $image_url;
	
	/** 
	 * 默认的展示样式
	 **/
	public $mark_info;
	
	/** 
	 * 默认的属性值名称
	 **/
	public $system_value_name;
	
	/** 
	 * 属性值ID
	 **/
	public $value_id;	
}
?>