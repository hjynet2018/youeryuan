<?php

/**
 * TopP4pMatchedProductDto
 * @author auto create
 */
class TopMatchedProductDto
{
	
	/** 
	 * 是否强制绑定
	 **/
	public $is_force_match;
	
	/** 
	 * Y:产品推广中,N:产品未推广
	 **/
	public $is_offer;
	
	/** 
	 * 是否设置优先推广
	 **/
	public $is_preferential;
	
	/** 
	 * 产品ID
	 **/
	public $product_id;
	
	/** 
	 * 推广评分星级取值[0-5]
	 **/
	public $qs_star;
	
	/** 
	 * 产品标题
	 **/
	public $subject;	
}
?>