<?php

/**
 * AdKeywordEffectDto
 * @author auto create
 */
class AdKeywordEffectDto
{
	
	/** 
	 * 点击量
	 **/
	public $click_cnt;
	
	/** 
	 * 平均点击花费
	 **/
	public $click_cost_avg;
	
	/** 
	 * 单位元，保留两位小数, 例如3.75表示3.75元
	 **/
	public $cost;
	
	/** 
	 * 百分比，保留两位小数，例如3.75表示3.75%
	 **/
	public $ctr;
	
	/** 
	 * 曝光
	 **/
	public $impression_cnt;
	
	/** 
	 * 关键词
	 **/
	public $keyword;
	
	/** 
	 * 单位小时，保留一位小数，例如13.5表示13.5小时
	 **/
	public $online_time;	
}
?>