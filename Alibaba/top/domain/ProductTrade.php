<?php

/**
 * 商品交易
 * @author auto create
 */
class ProductTrade
{
	
	/** 
	 * 发货期限
	 **/
	public $consignment_term;
	
	/** 
	 * 最小起订量补充信息
	 **/
	public $min_order_other;
	
	/** 
	 * 最小起订量数量
	 **/
	public $min_order_quantity;
	
	/** 
	 * 最小起订量计量单位，枚举值
	 **/
	public $min_order_unit;
	
	/** 
	 * 金额类型，枚举值
	 **/
	public $money_type;
	
	/** 
	 * 常规包装
	 **/
	public $packaging_desc;
	
	/** 
	 * 其他付款方式
	 **/
	public $payment_method_other;
	
	/** 
	 * 付款方式，枚举值
	 **/
	public $payment_methods;
	
	/** 
	 * 港口
	 **/
	public $port;
	
	/** 
	 * FOB价格 最大值
	 **/
	public $price_range_max;
	
	/** 
	 * FOB价格 最小值
	 **/
	public $price_range_min;
	
	/** 
	 * FOB价格 计量单位，枚举值
	 **/
	public $price_unit;
	
	/** 
	 * 供货能力补充信息
	 **/
	public $supply_other;
	
	/** 
	 * 供应周期
	 **/
	public $supply_period;
	
	/** 
	 * 供货能力
	 **/
	public $supply_quantity;
	
	/** 
	 * 计量单位，枚举值
	 **/
	public $supply_unit;	
}
?>