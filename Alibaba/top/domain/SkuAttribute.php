<?php

/**
 * SKU使用的属性
 * @author auto create
 */
class SkuAttribute
{
	
	/** 
	 * 属性ID
	 **/
	public $attribute_id;
	
	/** 
	 * 属性名称
	 **/
	public $attribute_name;
	
	/** 
	 * 属性下的值
	 **/
	public $values;	
}
?>