<?php

/**
 * QuickKeywordEffectDto
 * @author auto create
 */
class QuickKeywordEffectDto
{
	
	/** 
	 * 点击
	 **/
	public $click_cnt;
	
	/** 
	 * 花费
	 **/
	public $cost;
	
	/** 
	 * 币种 RMB为人民币
	 **/
	public $cost_type;
	
	/** 
	 * 曝光
	 **/
	public $impression_cnt;
	
	/** 
	 * 产品ID列表,逗号分割
	 **/
	public $product_id_list;
	
	/** 
	 * 日期
	 **/
	public $stat_date;
	
	/** 
	 * 关键词
	 **/
	public $word;	
}
?>