<?php

/**
 * TopP4pProductGroupDto
 * @author auto create
 */
class TopProductGroupDto
{
	
	/** 
	 * 产品分组标识
	 **/
	public $group_id;
	
	/** 
	 * 产品分组名称
	 **/
	public $group_name;
	
	/** 
	 * 是否是叶子分组，即没有子分组
	 **/
	public $leaf;	
}
?>