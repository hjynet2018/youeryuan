<?php

/**
 * TagGroup
 * @author auto create
 */
class TagGroup
{
	
	/** 
	 * 关键词数
	 **/
	public $count;
	
	/** 
	 * 分组ID，不对外暴露
	 **/
	public $id;
	
	/** 
	 * 产品经理推词用，不对外暴露
	 **/
	public $is_read;
	
	/** 
	 * 分组名称
	 **/
	public $name;	
}
?>