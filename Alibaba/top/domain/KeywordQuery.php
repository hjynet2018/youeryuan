<?php

/**
 * KeywordQuery
 * @author auto create
 */
class KeywordQuery
{
	
	/** 
	 * 指定查询开始的具体日期，最多往前推4周
	 **/
	public $from_date;
	
	/** 
	 * 按最近1天，7天，30天查询
	 **/
	public $inteval;
	
	/** 
	 * 是否精确查询
	 **/
	public $is_exact;
	
	/** 
	 * 按关键词查询
	 **/
	public $keyword;
	
	/** 
	 * 每页个数
	 **/
	public $per_page_size;
	
	/** 
	 * 按星级查询,可取0,1,2,3,4,5
	 **/
	public $qs_star;
	
	/** 
	 * 按关键词状态查询，只能取值stopped和in_promotion
	 **/
	public $status;
	
	/** 
	 * 按关键词分组名称查询
	 **/
	public $tag_name;
	
	/** 
	 * 指定第几页
	 **/
	public $to_page;	
}
?>