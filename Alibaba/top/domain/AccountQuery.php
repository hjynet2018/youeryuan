<?php

/**
 * AccountQuery
 * @author auto create
 */
class AccountQuery
{
	
	/** 
	 * 开始时间 inteval=7或30不用指定
	 **/
	public $begin_date;
	
	/** 
	 * 结束时间 inteval=7或30不用指定
	 **/
	public $end_date;
	
	/** 
	 * 区间 1代表指定时间查询 7代表最近7天 30代表最近30天
	 **/
	public $inteval;
	
	/** 
	 * 每页行数
	 **/
	public $per_page_size;
	
	/** 
	 * 第几页
	 **/
	public $to_page;	
}
?>