<?php

/**
 * 商品SKU
 * @author auto create
 */
class ProductSkuResponse
{
	
	/** 
	 * SKU使用的属性
	 **/
	public $sku_attributes;
	
	/** 
	 * SKU定义
	 **/
	public $skus;	
}
?>