<?php

/**
 * 批发物流集合
 * @author auto create
 */
class ListTemplateAPIResult
{
	
	/** 
	 * 运费模板集合
	 **/
	public $items;
	
	/** 
	 * 运费模板总数
	 **/
	public $total;	
}
?>