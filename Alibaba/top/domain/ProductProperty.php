<?php

/**
 * 产品属性
 * @author auto create
 */
class ProductProperty
{
	
	/** 
	 * 属性名称
	 **/
	public $attr_name;
	
	/** 
	 * 属性名ID
	 **/
	public $attr_name_id;
	
	/** 
	 * 属性值
	 **/
	public $attr_value;
	
	/** 
	 * 属性ID
	 **/
	public $attr_value_id;
	
	/** 
	 * 时间间隔
	 **/
	public $inverval;
	
	/** 
	 * 单位
	 **/
	public $unit;	
}
?>