<?php

/**
 * 单个商品详情
 * @author auto create
 */
class AlibabaProductResponse
{
	
	/** 
	 * 商品属性
	 **/
	public $attributes;
	
	/** 
	 * 类目ID
	 **/
	public $category_id;
	
	/** 
	 * 商品详情描述
	 **/
	public $description;
	
	/** 
	 * Y为上架状态
	 **/
	public $display;
	
	/** 
	 * 产品更新时间
	 **/
	public $gmt_modified;
	
	/** 
	 * 商品分组ID
	 **/
	public $group_id;
	
	/** 
	 * 关键词
	 **/
	public $keywords;
	
	/** 
	 * 语种
	 **/
	public $language;
	
	/** 
	 * 商品的主图
	 **/
	public $main_image;
	
	/** 
	 * 产品负责人
	 **/
	public $owner_member;
	
	/** 
	 * 产品负责人显示名，由firstname和lastname拼接组成
	 **/
	public $owner_member_display_name;
	
	/** 
	 * 产品ID
	 **/
	public $product_id;
	
	/** 
	 * 商品SKU
	 **/
	public $product_sku;
	
	/** 
	 * 商品类型
	 **/
	public $product_type;
	
	/** 
	 * 询盘商品交易信息
	 **/
	public $sourcing_trade;
	
	/** 
	 * 商品状态
	 **/
	public $status;
	
	/** 
	 * 商品名称
	 **/
	public $subject;
	
	/** 
	 * 在线批发商品交易信息
	 **/
	public $wholesale_trade;	
}
?>