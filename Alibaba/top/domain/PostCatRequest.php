<?php

/**
 * 发布类目查询request
 * @author auto create
 */
class PostCatRequest
{
	
	/** 
	 * 发布类目id,必须大于等于0， 如果为0，则查询所有一级类目
	 **/
	public $cat_id;	
}
?>