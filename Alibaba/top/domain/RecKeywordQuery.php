<?php

/**
 * RecKeywordQuery
 * @author auto create
 */
class RecKeywordQuery
{
	
	/** 
	 * 搜索词
	 **/
	public $keyword;
	
	/** 
	 * 排序，不用
	 **/
	public $order_str;
	
	/** 
	 * 每页行数
	 **/
	public $per_page_size;
	
	/** 
	 * 推荐词类型，暂不支持
	 **/
	public $rec_type;
	
	/** 
	 * 返回个数，不用
	 **/
	public $return_count;
	
	/** 
	 * 第几页
	 **/
	public $to_page;
	
	/** 
	 * 总个数，不用
	 **/
	public $total_item;	
}
?>