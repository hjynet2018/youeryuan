<?php

class ContentAction extends MythinkAction {
	
	
	
	public function _initialize(){
		
		exit;
		
		/*
		if(!empty($_REQUEST['siteid'])){
			
			$_SESSION['siteid'] = $_REQUEST['siteid'];
			
			$res = CMS_M('website')->where('website_id='.get_site_id())->find();
			
			$_SESSION['tplPath'] = 'Templates/'.trim($res['weburl']);
			//AAA($_SESSION );
		}else{
			
			$res = CMS_M('website')->where('website_id='.get_site_id())->find();
			$_SESSION['tplPath'] = 'Templates/'.trim($res['weburl']);
			$_SESSION['siteUrl'] = siteUrl(get_site_id());
		}
		*/
		
		//检查网站必要全局参数
		if(empty($_SESSION['font_siteid']) || empty($_SESSION['font_lang'])){
			$res = CMS_M('website')->where('website_id=1')->find();
			$_SESSION['tplPath'] = 'Templates/'.trim($res['weburl']);
			$_SESSION['font_siteid'] =$res['website_id'];
			$_SESSION['font_lang'] =$res['lang'];
		}
		//站点地址
		$this->assign('siteUrl',$_SESSION['siteUrl']);
		
		//面包屑导航
		$this->assign('site_nav',site_nav());
		
		$this->assign('defaultPath','./Admin/Tpl/Templates/default');
		
		//加载所有语言
		parent::_initialize();
		
		$this->header();
		$this->footer();
	}
	
    public function header(){
		
		//$where['siteid']	=	get_site_id2();
		
		$res = CMS_M('web_baseinfo')->where($where)->select();
		//AAA($res);
		foreach($res as $key=>$val){
			
			if($val['info_key']=='title'){
				
				$title = $val['info_val'];	
				$id 	= id();
				$cat_id = cat_id();
				
				if(!empty($cat_id)){
					$all_parent = array();
					get_single_parent($cat_id,$all_parent);
					krsort($all_parent);
					foreach($all_parent as $single_Parent){
						if($single_Parent['title']==''){
							$title = $single_Parent['cat_name'].' - '.$title;
						}else{
							$title = $single_Parent['title'].' - '.$title;
						}
					}
					
				}
				
				if(!empty($cat_id) && !empty($id)){
					
					$top_parent_id = top_parent($cat_id);
					$res = CMS_M('category')->where('category_id='.$top_parent_id)->find();
				
					$table = CMS_M('cm_table')->where('table_id='.$res['module_id'])->find();
					$table_name = $table['table_name'];
					
					$table_m = CMS_M($table_name);
					$table_m_pk = CMS_M($table_name)->getPk();
					$art = CMS_M($table_name)->where($table_m_pk.'='.$id)->find();
					if($art['title']!=''){
						$title = $art['title'].' - '.$title;
					}
				}
				
				$this->assign('seo_'.$val['info_key'],$val['info_val']);
			}else{
				
				$this->assign('seo_'.$val['info_key'],$val['info_val']);
			}
			
		}
		
		
	}
	
	public function footer(){
		
		//getByModules(13);
	}
	
	public function product(){
		$this->display('Templates/default/product');
	}
	
	
	
	public function index(){
		
		/*
		if($this->is_phone()){
			
			$_SESSION['siteid'] = 2;
			
			$res = CMS_M('website')->where('website_id='.get_site_id())->find();
		
			$_SESSION['tplPath'] = 'Templates/'.trim($res['weburl']);
			
			$_SESSION['siteUrl'] = siteUrl(get_site_id());
			
		}else{
				
		}*/
		
		//AAA($_SESSION);
		// if(!empty($_REQUEST['siteid'])){
			
			// $_SESSION['siteid'] = $_REQUEST['siteid'];
			
			// $res = CMS_M('website')->where('website_id='.get_site_id())->find();
			
			// $_SESSION['tplPath'] = 'Templates/'.trim($res['weburl']);
			//AAA($_SESSION );
		// }
		
		if(!empty($_REQUEST['siteid'])){
			
			$_SESSION['font_siteid'] = $_REQUEST['siteid'];
			
			$res = CMS_M('website')->where('website_id='.get_site_id2())->find();
			
			$_SESSION['tplPath'] = 'Templates/'.trim($res['weburl']);
			$_SESSION['font_lang'] = $res['lang'];
			//AAA($_SESSION );
		}
		
		//$this->showTpl('index - 02-pro');
		//$this->showTpl('index - 04 -contact');
		//$this->showTpl('index - 01');
		//$this->showTpl('index -03-brand');
		$this->showTpl('index');
	}
	

	
	public function lists(){
		//cat2article(22);
		$cat_id = empty($_REQUEST['cat_id'])?'0':$_REQUEST['cat_id'];
		
		if($cat_id==0){
			
			throw new Exception('CatId Error!!');
		}
		
		$catInfo = CMS_M('category')->where('category_id='.$cat_id)->find();
		//AAA($catInfo);
		foreach($catInfo as $key=>$val){
			
			$this->assign('list_'.$key,$val);
		}
		
		//AAA($catInfo);
		//echo $_SESSION['tplPath'];exit;
		//echo getCurrentTpl();exit;
		$this->showTpl(getCurrentTpl());
	}
	
	
	public function show(){
		
		//根据ID获取记录并循环非配
		$id 	= empty($_REQUEST['id'])?'0':$_REQUEST['id'];
		$cat_id = empty($_REQUEST['cat_id'])?'0':$_REQUEST['cat_id'];
		
		if($id==0){
			
			throw new Exception('AtricleId Error!!');
		}
		
		$top_parent_id = top_parent($cat_id);
		
		$origin_cat= CMS_M('category')->where('category_id='.$cat_id)->find();
		$this->assign('origin_cat',$origin_cat);
		
		$current_cat= CMS_M('category')->where('category_id='.$cat_id)->find();
		if(!empty($current_cat['module_id'])){
			$module_id = $current_cat['module_id'];
			
		}else{
			$top_parent_cat= CMS_M('category')->where('category_id='.$top_parent_id)->find();
			$module_id = $top_parent_cat['module_id'];
			
		}
		
		
		$table  = CMS_M('cm_table')->where('table_id='.$module_id)->find();
		$m  = CMS_M($table['table_name']);
		$pk = $m->getPk();
		
		$res = $m->where($pk.'='.$id)->find();
		
		foreach($res as $key=>$val){
			
			$this->assign('show_'.$key,$val);
		}
		
		
		//获取上一条与下一条记录
		
		$cat_id = $res['cat_id'];
		
		$preNextInfo = $m->where('cat_id='.$cat_id)->select();
		
		$pre    = '';
		$next   = '';
		
		
		
		foreach($preNextInfo as $key=>$val2){
			
			
			$positionNow = -1;
		
			foreach($preNextInfo  as $keyPos=>$rowPos){
				
				if($rowPos[$pk]==$res[$pk]){
					
					$positionNow = $keyPos;
				}	
			}
			
			if( ($val2[$pk]==$res[$pk]) && ($positionNow)>0 ){
				
				$pre = '<a class="pull-left" href="'.CON_PATH.'/'.$preNextInfo[$key-1][$pk].'/cat_id/'.$cat_id.'">上一条：'.str_cut($preNextInfo[$key-1]['title'],38,'...').'</a>';
			}
			
			if( ($val2[$pk]==$res[$pk]) && $positionNow<(count($preNextInfo)-1) ){
				
				$next = '<a class="pull-right" href="'.CON_PATH.'/'.$preNextInfo[$key+1][$pk].'/cat_id/'.$cat_id.'">下一条:'.str_cut($preNextInfo[$key+1]['title'],38,'...').'</a>';
			}
			
		}
		
		$pre=$pre==''?'<a  class="pull-left" href="#">第一条</a>':$pre;
		$next=$next==''?'<a class="pull-right" href="#">最后一条</a>':$next;
		
		
		$this->assign('pre',$pre);
		$this->assign('next',$next);
		
		addHits($id,$cat_id);
		//addFavor($id,$cat_id);
		
		//echo getCurrentTpl();echo '---';exit;
		$this->showTpl(getCurrentTpl());
	}
	
	
	public function showTpl($tpl){
		
		$this->display($_SESSION['tplPath'].'/'.$tpl);
		
	}
	
	
	
	
	
	public function module2actions(){
		
		$category_id = I('category_id');
		$top_parent_id = top_parent($category_id);
		$top_parent_id = $category_id;
		$cateInfo  = CMS_M('category')->where('category_id='.$top_parent_id)->find();
		$module_id = $cateInfo['module_id'];
		
		$table  = CMS_M('cm_table')->where('table_id='.$module_id)->find();
		$table_name = $table['table_name'];
		$con_m 	= CMS_M($table_name);
		$pk 	= $con_m->getPk();
		//AAA(ucfirst($table_name).'/'.ucfirst($table_name).'_list');
		//$dir = U(ucfirst($table_name).'/'.ucfirst($table_name).'_list',array('category_id'=>$category_id,'module_id'=>$module_id));
		$this->redirect(ucfirst($table_name).'/'.ucfirst($table_name).'_list',array('category_id'=>$category_id,'module_id'=>$module_id));
	}
	
	public function search(){
		
		$module_id 	= $_REQUEST['module_id'];
		$search_key = $_REQUEST['search_key'];
		
		$table  = CMS_M('cm_table')->where('table_id='.$module_id)->find();
		$table_name = $table['table_name'];
		
		$con_m 	= CMS_M($table_name);
		
		
		$res = $con_m->where('title LIKE "%'.$search_key.'%"')->select();
		
		$this->assign('result',$res);
		
		$this->showTpl('search');
	}
	
	
	
	public function is_phone(){
		
		$useragent=isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] :''; 

		$useragent_commentsblock=preg_match('|\(.*?\)|',$useragent,$matches)>0?$matches[0]:'';    

		function CheckSubstrs($substrs,$text){ 

			foreach($substrs as $substr) 
			
				if(false!==strpos($text,$substr)){ 
				
				return true; 
			} 
			return false; 
		}

		$mobile_os_list		=	array('Google Wireless Transcoder','Windows CE','WindowsCE','Symbian','Android','armv6l','armv5','Mobile','CentOS','mowser','AvantGo','Opera Mobi','J2ME/MIDP','Smartphone','Go.Web','Palm','iPAQ');
		$mobile_token_list	=	array('Profile/MIDP','Configuration/CLDC-','160×160','176×220','240×240','240×320','320×240','UP.Browser','UP.Link','SymbianOS','PalmOS','PocketPC','SonyEricsson','Nokia','BlackBerry','Vodafone','BenQ','Novarra-Vision','Iris','NetFront','HTC_','Xda_','SAMSUNG-SGH','Wapaka','DoCoMo','iPhone','iPod'); 

		$found_mobile=CheckSubstrs($mobile_os_list,$useragent_commentsblock) || CheckSubstrs($mobile_token_list,$useragent); 



		if ($found_mobile){ 

			return true; 

		}else{ 

			return false; 

		} 
	}
	
	
	public function search_business(){
		
		$business_name = I('business_name');
		$province_id   = I('province_id');
		
		$where = true;
		
		
		if($province_id!=0){
			
			$where = 'province_id='.$province_id;
		}
		
		if(!empty($business_name)){
			
			$where.=' AND title LIKE "%'.$business_name.'%"';
		}
		
		
		$this->assign('result',$res);
		
		$this->showTpl('search');
	}
	
	
	public function add_mess(){
		
		$title 	 = I('title');
		$tell 	 = I('tell');
		$content = I('content');
		$ip		 = get_client_ip();
		
		$res = CMS_M('others')->where('hits_ips="'.$ip.'"')->find();
		if(count($res)>0){
			$this->alert_back('您已经提交过留言了，谢谢您的参与！');
		}
		
		$data['title'] 	=	$title;
		$data['tell'] 	=	$tell;
		$data['content']=	$content;
		$data['hits_ips'] 	=	$ip;
		$data['siteid'] =	1;
		
		if(CMS_M('others')->add($data)){
			
			$this->alert_back('提交留言成功，谢谢您的参与！');
		}else{
			$this->alert_back('提交留言失败！！');
		}
		
	}
	
	
	public function gen_index(){
		$dir = './Admin/Tpl/Index/index.html';
		if(file_exists($dir)){
			
			unlink($dir);
		}	
		$con = $this->fetch('Templates/default/index_full');
		file_put_contents($dir,$con);
		
		$this->alert_back('清除缓存成功\n生成首页成功！');
	}
	
	
	public function getbatpros(){
		
		$cat_id = I('cat_id');
		
		$pros = M('product')->where('cat_id='.$cat_id)->select();
		
		die(json_encode($pros));
	}
	
	
	public function doSearchBat(){
		
		$cat_id 		= empty($_REQUEST['cat_id'])?0:$_REQUEST['cat_id'];
		$general_no 	= empty($_REQUEST['general_no'])?'':$_REQUEST['general_no'];
		$title 			= empty($_REQUEST['title'])?0:$_REQUEST['title'];
		//$type 			= empty($_REQUEST['type'])?0:$_REQUEST['type'];
		//print_r($_REQUEST);exit;
		$condition = array();
		
		if($cat_id!=0){
			/*$subCatArr = sub_cat(top_parent( $cat_id ));
			$subCatIdArr = array();
			
			foreach($subCatArr as $val){
				$subCatIdArr[]=$val['category_id'];
			}*/
			//$condition['cat_id'] = $cat_id;
			$condition['cat_id'] = $cat_id;
		}
		
		if($general_no!=''){
			
			$condition['general_no'] = array('like','%'.$general_no.'%');
		}
		
		if($title!=''){
			
			$condition['title'] = array('like','%'.$title.'%');
		}
		
		$list = M('product')->where($condition)->select();
		
		$this->assign('list',$list);
		$this->assign('cat_id',$cat_id);
		
		$this->showTpl('search_bat_pro');
		
	}
	
	public function search_product(){
		$condition = array();
		if(!empty($_REQUEST['search_keywords'])){
		
			$condition['title'] = array('like','%'.trim($_REQUEST['search_keywords']).'%');
		}
		$product_list = M('product')->where($condition)->select();//AAA($product_list);
		$this->assign('product_list',$product_list);
		$this->showTpl('search_product');	
		
	}
	
	
	//帅选产品
	public function product_list(){
	//AAA($_REQUEST['condition']);
		$URI = $_REQUEST['condition'];
		$category_id = $URI['cat_id'];
		$top_parent_id = top_parent($category_id);
		$top_parent_id = $category_id;
		$cateInfo  = CMS_M('category')->where('category_id='.$top_parent_id)->find();
		$module_id = $cateInfo['module_id'];
		
		$table  = CMS_M('cm_table')->where('table_id='.$module_id)->find();
		$table_name = $table['table_name'];
		
	
		$this->assign('search_condition_index',$_REQUEST['search_condition_index']);
	
		$condition = array();
		if(!empty($_REQUEST['cat_id'])){
			$condition['cat_id'] = $_REQUEST['cat_id'];
		}
		
		$condition['_string']  =' 1  ';
		foreach($URI as $uKey=>$val){
			
			if(!empty($val)){
				$valArr = array_filter(explode('---',$val));
				if(!empty($valArr)){
					$condition['_string'] .=' AND ( ';
					
				}
				foreach($valArr as $one_key=> $one){
					if($one_key==0){
						$condition['_string'] .='  FIND_IN_SET("'.$one.'", '.$uKey.') ';
					}else{
						$condition['_string'] .=' OR FIND_IN_SET("'.$one.'", '.$uKey.') ';
						
					}
					
					if($one_key==(count($valArr)-1) ){
						
						$condition['_string'] .='  ) ';
					}
					
				}
				
				
			}
			
		}
		//AAA($condition);
		
		$list = M($table_name)->where($condition)->select();
		//AAA($list);
		$this->assign('list',$list);
		$rs  = $this->fetch('Templates/default/select_product_li');
		echo $rs;
		//AAA($list);
	
		//AAA($_REQUEST);
	}
	
	
	
	
	
}
?>