<?php

// 树形
class TreeAction extends MythinkAction {



	
	public function get_area_info()
	{
		$pid = $_REQUEST['pid'];
		$condition  =array();
		$condition ['item_parent'] = $pid;
		$condition ['list_id'] = 13;;
		//AAA($condition);
		$list  =M('cm_list_item')->where($condition)->order(' item_sort asc ')->select();
		
		$result = array();
		$result['list'] = $list;
		die(json_encode($result));
	}
	
	
	//获取客服树形 普通客服
	public function admin_list()
	{
		$condition = array();
		$condition['role_id'] = array('in','5,6,7,11,12');
		$rs = M('admin')->where($condition)->select();
		
		$group = $this->factory_group();
		
		foreach($rs as $key=>$val){
			
			$rs[$key]['name'] = $val['nickout'].' '.$group[$val['group_id']]['name'];
			
			
			$rs[$key]['id'] = $val['id'];
			$rs[$key]['pId'] = 0;
			$rs[$key]['isParent'] = 'false';
	
		}
		
		die(json_encode($rs));		
	}
	
	
	//获取客服树形 渠道客服
	public function worker_admin_list()
	{
		$condition = array();
		$condition['role_id'] = array('in','8,9,10');
		$rs = M('admin')->where($condition)->select();
		
		$group = $this->factory_group();
		
		foreach($rs as $key=>$val){
			
			$rs[$key]['name'] = $val['nickout'].' '.$group[$val['group_id']]['name'];
			
			
			$rs[$key]['id'] = $val['id'];
			$rs[$key]['pId'] = 0;
			$rs[$key]['isParent'] = 'false';
	
		}
		
		die(json_encode($rs));		
	}
	
	//获取标签树形
	public function worker_label_list()
	{	
		$condition = array();
		$condition['list_id'] = 49;
		$rs = M('cm_list_item')->where($condition)->select();
				
		
		foreach($rs as $key=>$val){
			
			$rs[$key]['name'] = $val['item_desc'];
			
			
			$rs[$key]['id'] = $val['list_item_id'];
			$rs[$key]['pId'] = 0;
			$rs[$key]['isParent'] = 'false';
	
		}
		die(json_encode($rs));		
	}
	
	
	
	//获取树形厂家列表信息
	public function get_factory_list()
	{
		$rs = M('factory')->field('factory_id AS id, factory_full_name AS name ,0 AS pId,"false" AS isParent')->select();
		
		
		die(json_encode($rs));		
	}
	//获取厂家菜单厂家列表
	public function get_search_factory_list()
	{
		$key_words = trim($_REQUEST['key_words']);
		$condition = array();
		
		$condition['factory_full_name'] = array('like', '%'.$key_words.'%');
		
		$rs = M('factory')->where($condition)->select();
		
		$all_ids = array();
		
		foreach($rs as $key=>$val){
			$all_ids[]=array($val['factory_id']);
		}
		
		die(json_encode($all_ids));
	}
	//获取菜单信息
	public function getJSON(){
		
		$list_model =  M('cm_list_item');
		$operation = $_REQUEST['operation'];
		//AAA($operation);
		switch($operation){
			case 'list':								//列表
				
				$condition = array();
				$condition['item_parent'] = empty($_REQUEST['pid']) ? 0 : $_REQUEST['pid'];
				$condition['list_id'] = $_REQUEST['list_id'];
				
				$rs = $list_model->where($condition)->order('item_sort asc')->select();
				
				foreach($rs as $key=>$val){
					$rs[$key]['name'] = $val['item_desc'];
					$rs[$key]['id'] = $val['list_item_id'];
					$rs[$key]['pId'] = $val['item_parent'];
					
					$count = $list_model->where('item_parent='.$val['list_item_id'])->order('item_sort asc')->count();
					if($count>0){
						$rs[$key]['isParent'] = 'true';
						//是否开启父级菜单 选中功能
						if( $_REQUEST['is_parent_check'] == 'false' && $_REQUEST['lv']!=1 ){
							$rs[$key]['nocheck'] = 'true';
						}
						
					}else{
						$rs[$key]['isParent'] = 'false';				
					}	
				}
				
				die(json_encode($rs));
				
				
				break;
				
			case 'add':								//添加
				$result = array();
				$data  =  array();
				$data['item_parent'] = $_REQUEST['pid'];
				$data['item_desc']   = trim($_REQUEST['name']);
				$data['list_id']     = $_REQUEST['list_id'];
				//AAA($data);
				$rs = $list_model->add($data);
				if($rs){
					$result['status']=1;
					$result['id']=$rs;
					$result['msg']='添加成功';
				}else{
					$result['status']=0;
					$result['msg']='添加失败';
				}
				die(json_encode($result));
				
				
				break;
			case 'edit':
			
				$data = array();
				$data  =  array();
				$data['list_item_id'] = $_REQUEST['id'];
				$data['item_desc']   = trim($_REQUEST['name']);
				$data['list_id']     = $_REQUEST['list_id'];
				//AAA($data);
				$rs = $list_model->save($data);
				if($rs){
					$result['status']=1;
					
					$result['msg']='修改成功';
				}else{
					$result['status']=0;
					$result['msg']='修改失败';
				}
			
			
				die(json_encode($result));
				break;
				
			case 'delete':
			
				$condition = array();
				
				$condition['list_item_id'] = $_REQUEST['id'];
				
				$condition['list_id']     = $_REQUEST['list_id'];
				//AAA($condition);
				$rs = $list_model->where($condition)->delete($condition);
				if($rs){
					$result['status']=1;
					
					$result['msg']='删除成功';
				}else{
					$result['status']=0;
					$result['msg']='删除失败';
				}
			
			
				die(json_encode($result));
				break;
				
			case 'sort':
			
				$editPidNodes= $_REQUEST['editPidNodes'];
				foreach($editPidNodes as $oneEdit){
					$oneUpdate = array();
					$oneUpdate['list_item_id'] = $oneEdit['id'];
					$oneUpdate['item_parent']    = $oneEdit['pid'];
					$list_model->save($oneUpdate);
				}
			
				$sortData = $_REQUEST['sortArr'] ; 
				$result = array();
				foreach($sortData as $oneData){
					$oneUpdate = array();
					$oneUpdate['list_item_id'] = $oneData['id'];
					$oneUpdate['item_sort'] = $oneData['sort'];
					$list_model->save($oneUpdate);
				}
				$result['status']=1;
				die(json_encode($result));
				break;
				
		}
		
		
		
	}
	//获取匹配的项 并向上递归
	public function getJSON_search()
	{
	
		if($_REQUEST['list_id']==13){
			//$this->get_search_area();
			//$this->redirect(U('Tree/get_search_area'));
		}
	
		$key_words = trim($_REQUEST['key_words']);
		
		$condition = array();
		$condition['list_id'] = $_REQUEST['list_id'];
		$condition['item_desc'] = array('like', '%'.$key_words.'%');
		
		$rs = M('cm_list_item')->where($condition)->select();
		
		$all = array();
		$allids = array();
		foreach($rs as $key=>$val){
			$one = array();
			$one = $this->get_parent99($val);
			$one[]=$val;
									
			$all[]=$one;
			
			$oneid = array();
			foreach($one as $key2 =>$val2){
				$oneid[]=$val2['list_item_id'];
			}
			$allids[]=$oneid;
		}
		//AAA($allids);
		//$result = array('31','34','40','50');
		//$result2 = array('31','38','98','99');
		//$result3 = array('31','35','90','91');
		
		//$all[]=$result;
		//$all[]=$result2;
		//$all[]=$result3;
		
		die(json_encode($allids));
	}
	
	
	public function test(){
		$item = array();
		$item['item_parent']=40;
		
		$rs = $this->get_parent99($item);
		
		AAA($rs );
	}
	//地区excel导入
	public function Daoru(){
	
		$rs = $this->FILE_UPLOAD();
		vendor('Form.Excel.PHPExcel');
		
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');//use Excel2007 for 2007 format 
        $objPHPExcel = $objReader->load('.'.$rs['url']); 
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow();           //取得总行数 
        $highestColumn = $sheet->getHighestColumn(); //取得总列数
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		
		$excelData = array();
		for ($row = 1;$row <= $highestRow;$row++){
			for ($col = 0;$col < $highestColumnIndex;$col++){
				$excelData[$row][] =(string)$sheet->getCellByColumnAndRow($col, $row)->getValue();
			}
		}
		
		
		foreach($excelData as $key=>$row){
			foreach($row as $key2=>$cell){
			
				$cellinfo = trim($cell);
				
				$condition = array();
				$condition['item_desc'] = array('like','%'.$cellinfo.'%');					
				$record  = M('cm_list_item2')->where($condition)->find();
				if($record){
					continue;
				}
			
				$data = array();
				$data['list_id']  =13;
				$data['item_desc']  =$cellinfo;
				if($key2==0){
					$data['item_parent']  =0;					
				}else{
					$where = array();
					$where['item_desc'] =  array('like','%'.$row[$key2-1].'%');			
					$parent = M('cm_list_item2')->where($where)->find();
					$data['item_parent']  =$parent['list_item_id'];	
				}
				M('cm_list_item2')->add($data);
			}
		}
	}
	//获取地区信息
	public function get_area()
	{
		$pid = empty($_REQUEST['pid']) ? 0 : $_REQUEST['pid'];
		
		$condition = array();
		$condition['PARENT_ID'] = $pid;		
		
		$rs = M('area')->where($condition)->select();
		//AAA($rs);
		foreach($rs as $key=>$val){
			$rs[$key]['name'] = $val['NAME'];
			$rs[$key]['id'] = $val['ID'];
			$rs[$key]['pId'] = $val['PARENT_ID'];
			//$rs[$key]['isHidden'] = 'true';
			
			$count = M('area')->where('PARENT_ID='.$val['ID'])->count();
			if($count>0){
				$rs[$key]['isParent'] = 'true';
				if( $_REQUEST['is_parent_check'] == 'false' ){
					$rs[$key]['nocheck'] = 'true';
				}
				
			}else{
				$rs[$key]['isParent'] = 'false';
				//$rs[$key]['isParent'] = 'false';
			}
			
			
		}
		
		die(json_encode($rs));
	}
	
	public function get_search_area(){
	
		$key_words = trim($_REQUEST['key_words']);
		
		$condition = array();
		
		$condition['NAME'] = array('like', '%'.$key_words.'%');
		
		$rs = M('area')->where($condition)->select();
		
		$all = array();
		$allids = array();
		foreach($rs as $key=>$val){
			$one = array();
			$one = $this->get_area_parent99($val);
			
			$one[]=$val;
									
			$all[]=$one;
			
			$oneid = array();
			foreach($one as $key2 =>$val2){
				$oneid[]=$val2['ID'];
			}
			$allids[]=$oneid;
		}
		//AAA($allids);
		//$result = array('31','34','40','50');
		//$result2 = array('31','38','98','99');
		//$result3 = array('31','35','90','91');
		
		//$all[]=$result;
		//$all[]=$result2;
		//$all[]=$result3;
		
		die(json_encode($allids));
	}
	
	
	public function getJSON_worker(){
		
		if($_REQUEST['list_id']==13){
			//$this->get_area();
			//$this->redirect(U('Tree/get_area'));
		}
		
		$pid = empty($_REQUEST['pid']) ? 0 : $_REQUEST['pid'];
		
		
		$condition['item_parent'] = $pid;
		$condition['list_id'] = $_REQUEST['list_id'];
		
		$rs = M('cm_list_item')->where($condition)->order('item_sort asc')->select();
		
		foreach($rs as $key=>$val){
			$rs[$key]['name'] = $val['item_desc'];
			$rs[$key]['id'] = $val['list_item_id'];
			$rs[$key]['pId'] = $val['item_parent'];
			
			$count = M('cm_list_item')->where('item_parent='.$val['list_item_id'])->order('item_sort asc')->count();
			if($count>0){
				$rs[$key]['isParent'] = 'true';
				if( $_REQUEST['is_parent_check'] == 'false' && $_REQUEST['lv']!=1 ){
					$rs[$key]['nocheck'] = 'true';
				}
				
			}else{
				$rs[$key]['isParent'] = 'false';
				//$rs[$key]['isParent'] = 'false';
			}
			$rs[$key]['isParent'] = 'false';
			
		}
		
		die(json_encode($rs));
		
	}

	

}

?>