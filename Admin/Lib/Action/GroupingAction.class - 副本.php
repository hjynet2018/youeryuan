<?php

class GroupingAction extends CommonAction {
	
	public $cmTableName = 'Grouping';
	
	public function _initialize(){
		
		parent::_initialize();
	}
	
    //列表
	public function Grouping_list(){
		
		Vendor('Form.TableList');
		
		$list	 =	  new TableList();		
		
		
		//获取搜索列表条件
		$where = $list->get_search_data($this->cmTableName);
		
		//根据条件获取列表
		$rs = $list->auto_gen_list($this->cmTableName,$where);
		
		//分配列表信息
		$this->assign('list',$rs['table_list']);
		//分配搜索表单信息
		$this->assign('searchForm',$rs['search_form']);
		
		
		
		//获取并分配添加按钮
		$add_btn = $list->gen_add_button($this->cmTableName);
		
		$this->assign('add_btn',$add_btn);
		
		
		//获取并分配Excel按钮
		$excel_btn = $list->get_excel_btn($this->cmTableName);
		
		$this->assign('excel_btn',$excel_btn);
		
		
		$this->display($this->cmTableName.'/list');
	}
	
	
	//添加
	public function Grouping_add(){
		
		//获取添加表单并且分配
		$this->assign('form',D($this->cmTableName)->form->_getForm());
		
		$this->display($this->cmTableName.'/add');
	}
	
	//处理添加的数据（入库）
	public function Grouping_addok(){
		
		//根据表名获取提交过来的数组
		$data=D($this->cmTableName)->form->recordToBuildData('add');
		
		$this->ADD_ONE($this->cmTableName,$data,U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']));
		
	}
	
	
	//编辑页面
	public function Grouping_edit(){
		
		//获取表格主建
		$pk	=	M($this->cmTableName)->getPk();
		
		$pkVal	=	$_REQUEST[$pk];
		
		//根据主键获取编辑的表单并分配
		$res=M($this->cmTableName)->where($pk.'='.$pkVal)->find();
		
		$this->assign('form',D($this->cmTableName)->form->setAction('__APP__/'.$this->cmTableName.'/'.$this->cmTableName.'_editok')->_getEditorForm($res));
		
		$this->display($this->cmTableName.'/edit');	
	}
	
	
	//处理编辑的数据（入库）
	public function Grouping_editok(){
		
		$pk=M($this->cmTableName)->getPk();
		
		//根据表名称获取编辑的数据数组并且入库
		$data=D($this->cmTableName)->form->recordToBuildData('edit');
		
		$this->EDIT_ONE($this->cmTableName,$data,U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']));	
		
	}
	
	
	//根据主建删除一条记录
	public function Grouping_del(){
	
		$pk=M($this->cmTableName)->getPk();
		
		$this->DELETE_ONE($this->cmTableName,$_REQUEST[$pk],U($this->cmTableName.'/'.$this->cmTableName.'_list'));
	}
	
	
	//批量删除
	public function Grouping_batch_del(){
		
		$ids = I('ids'); 
		
		$idsArr = explode(',',$ids);
		
		//根据主键数组批量删除记录
		foreach($idsArr as $val){
		
			$this->CM(strtolower($this->cmTableName))->delete($val);
		}
		
		$this->alert_jump('删除成功',U($this->cmTableName.'/'.$this->cmTableName.'_list'));
	}
	
	
	//模型Excel
	public function Grouping_excel(){
		
		Vendor('Form.TableList');
		
		$list	 =	  new TableList();
		
		
		//获取Excel上传表单并且分配
		$excel_btn = $list->gen_excel_form($this->cmTableName);
		$this->assign('excel_btn',$excel_btn);
		
		//组将回删按钮并分配
		$back_del_btn = '<a href="'.U('Common/del_last_excel',array('table_name'=>$this->cmTableName)).'">回删记录</a>';
		$this->assign('back_del_btn',$back_del_btn);
		
		
		//根据表名获取上次Excel导入记录，拆分数组并且分配
		$last_excel = file_get_contents('./Public/reportData/'.ucfirst($this->cmTableName).'.txt');
		
		$last_excelArr = unserialize($last_excel);
		$this->assign('table_name',$last_excelArr['table_name']);
		$this->assign('last_time',$last_excelArr['last_time']);
		$this->assign('successRows',$last_excelArr['successRows']);
		$this->assign('errorEmptyRows',$last_excelArr['errorEmptyRows']);
		$this->assign('errorRepeatRows',$last_excelArr['errorRepeatRows']);
		$this->assign('errorInsertRows',$last_excelArr['errorInsertRows']);
		$this->assign('errorDataMapRows',$last_excelArr['errorDataMapRows']);
		
		$this->display($this->cmTableName.'/excel');
	}
	
	
	public function mess_index(){
		
		$groupInfo = M('grouping')->where(true)->select();
		
		foreach($groupInfo as $key=>$val){
			
			$count = M('student')->where('group_class='.$val['grouping_id'])->count();
			
			$groupInfo[$key]['nums'] =  $count;
		}
		
		$zcount = M('student')->where('group_class=0')->count();
		
		$this->assign('zcount',$zcount);
		$this->assign('groupList',$groupInfo);
		
		$this->display();
	}
	
	Public function multiMess(){
		
		$groups = empty($_REQUEST['groups'])?array():$_REQUEST['groups'];
		
		$allStudent = array();
		
		$groupStr = '';
		foreach($groups as $key=>$group){
			
			$stuRow  =  array();
			
			$stuRow  = M('student')->where('group_class='.$group)->select();
			
			$allStudent[] = $stuRow;
			
			$stuRow  =  array();
			
			if($key==0){
				
				$groupStr.= ' ( group_class='.$group;
			}else{
				
				$groupStr.= ' OR group_class='.$group;
			}
			
			
			
			if($key==count($groups)-1){
				
				$groupStr.= ' ) ';
			}
		}
		
		
		$addArr = array();
		$addArr['phone']    = 'multi';
		$addArr['datetime'] = time();
		$inser_id = M('message')->add($addArr);
		
		
		$sql = '';
		$sql = 'select * from student where '.$groupStr;
		$stuList = M()->query($sql);
		
		$messStr = '';
		$content = '你好123';
		foreach($stuList as $keyMes=>$val){
			
			if($val['add_phone']!=''){
				
				if($keyMes==0){
					
					$messStr .= $val['add_phone'].','.$content;
				}else{
					
					$messStr .= '|'.$val['add_phone'].','.$content;
				}
			}
		}
		
		$messStr = $messStr;
		//AAA($messStr);
		
		//初始化
		$curl = curl_init();
		//设置抓取的url
		curl_setopt($curl, CURLOPT_URL, 'http://120.24.1.202/xn/sms/sendmultisms');
		//设置头文件的信息作为数据流输出
		curl_setopt($curl, CURLOPT_HEADER, 1);
		//设置获取的信息以文件流的形式返回，而不是直接输出。
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		//设置post方式提交
		curl_setopt($curl, CURLOPT_POST, 1);
		//设置post数据
		$post_data = array(
			'username'   => 	'sddsxx',
			'key'        => 	'6ae4f5bb2a134a97a8ab570709f324e5',
			'bnum'       => 	$inser_id,
			'timestamp'  => 	date("YmdHis"), 
			//'type'	     =>		'0',
			//'phone'	     =>		'13702487989',
			//'content'	     =>  urlencode('小肥羊'),
			'extend'	 =>	     0,
			'sendinfo'   => urlencode($messStr)
		); 
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
		//执行命令
		$data = curl_exec($curl);
		//关闭URL请求
		curl_close($curl);
		AAA($data);
		//显示获得的数据
		print_r($data);
	}
	
	
	public function addMuMes(){
		
		include ('/aliyun-dysms-php-sdk/api_demo/SmsDemo.php');
		
		$code   =   'SMS_130925643';
		
		$phoneArr   =   array();
		$phoneArr[] = '13425663554';
		$phoneArr[] = '13702487989';
		
		$signArr = array();
		$signArr[] = '德胜学校';
		$signArr[] = '德胜学校';
		
		$mobanArr = array();
		$mobanArr[] =   array(
                "activity"=>"报名活动1",
				"time"=>"2018-10-30",
				"location"=>"8号楼"
        );
		$mobanArr[] =   array(
                "activity"=>"报名活动2",
				"time"=>"2018-10-30",
				"location"=>"8号楼"
        );
		
		$response = SmsDemo::sendBatchSms( $code,$phoneArr,$signArr,$mobanArr );
	}
	
	
	
	
	
	
	
	
}