<?php

class StudentApiAction extends MythinkAction {
	
	public $cmTableName = 'Student';
	
	//public $APPID     = 'wxb52637c9bfcfd0c8';
	//public $APPSECRET = '3d548b6b5e433d98bee540350decc35b';
	
	//public $APPID     = 'wx2db696f196d4f4e8';
	//public $APPSECRET = 'b271a5ec2029341b8cd555491f457fe2';

	//public $REDIRECT_URI = 'http://t.ihecha.me/index.php/StudentApi/handelCode';  
	//public $REDIRECT_URI = '';   
	
	
	public function _initialize(){
		//AAA($_SERVER);
		//$this->REDIRECT_URI=$_SERVER['HTTP_HOST'].'/index.php/StudentApi/handelCode';
		$this->REDIRECT_URI= C('REDIRECT_URI');
		$this->APPID  = C('APPID');
		$this->APPSECRET  = C('APPSECRET');
		parent::_initialize();
		
		$condition = array();
		$condition['add_phone'] = $_COOKIE["user"];
		$student = M('student')->where($condition)->find();
		$this->assign('studentInfo',$student);
		
		
		$actInfo = M('activity')->where('status!=3')->find();//AAA($actInfo);
		$this->assign('actInfo',$actInfo);
		
		
		$titleInfo  =  M('web_baseinfo')->where('web_baseinfo_id=1')->find();
		
		$this->assign('titleInfo',$titleInfo);
	}
	
    public function test(){
		
		//$url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$this->APPID."&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
		$phone = empty($_REQUEST['phone']) ? '' :  $_REQUEST['phone'];
		$url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$this->APPID."&redirect_uri=".$this->REDIRECT_URI."&response_type=code&scope=snsapi_userinfo&state=".$phone."#wechat_redirect";
		//AAA($url);
		header('Location:'.$url);
	
	}
	
	public function handelCode(){ 
		//AAA($_REQUEST);
		$code = $_REQUEST['code'];
		$phone = empty($_REQUEST['state'])? '': $_REQUEST['state'];
		
		$url="https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$this->APPID."&secret=".$this->APPSECRET."&code=".$code."&grant_type=authorization_code";
		
		//$url='https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$_GET['code'].'&grant_type=authorization_code';
		//AAA($phone);
		$access_token = httpsGet($url);
		
		//$access_token = json_decode($access_token,true);
		//AAA($access_token['openid']);
		if($access_token['access_token']){
			
			$infoURL = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token['access_token'].'&openid='.$access_token['openid'].'&lang=zh_CN';
			$userInfo = httpsGet($infoURL,array());
			
			//AAA($userInfo);
			
			if($phone=='00000000000'){
				//AAA('123');
				//setcookie('ggg','df',7200,'/');
				setcookie("open_id", $userInfo['openid'], time()+3600*24*7);
				setcookie("wx_info", json_encode($userInfo), time()+3600*24*7);
				$condition = array();
				$condition['add_openid'] = $userInfo['openid'];
				//AAA($condition);
				$userInfo2 = M('student')->where($condition)->find();
				
				if( !empty($userInfo2['add_phone'])){
					setcookie("user", $userInfo2['add_phone'], time()+3600*24*7);
					//AAA($userInfo2);
					$this->redirect('StudentApi/activity', array(), 0, '页面跳转中...');
					exit;
				}else{
					$this->redirect('StudentApi/index', array(), 0, '页面跳转中...');
				}
				//$this->redirect('StudentApi/index', array(), 0, '页面跳转中...');
				exit;
			}
			
			
			$where = array();
			$where['phone'] = $phone;
			//$where['open_id'] = $userInfo['openid'];
			
			$res = M('weixin_to_phone')->where($where)->find();
			
			
			
			if(empty($res)){
				
				$dataAdd = array();
				$dataAdd['phone']   = $phone;
				$dataAdd['open_id'] = $userInfo['openid'];
				$dataAdd['ws_info'] = json_encode($userInfo);
				$dataAdd['datetime'] = time();
				
				$resCall = M('weixin_to_phone')->add($dataAdd);
				
			}else{
				
				$dataSave = array();
				$dataSave['weixin_to_phone_id']  =  $res['weixin_to_phone_id'];
				$dataSave['phone']  			 =  $phone;
				$dataSave['open_id']  			 =  $userInfo['openid'];
				$dataSave['ws_info']             =  json_encode($userInfo);
				$dataSave['datetime']            =  time();
				
				$resCall = M('weixin_to_phone')->save($dataSave);
			}
			
			
			if($resCall){
				
				$this->assign('msg','绑定微信成功!');
				$this->assign('desc','您的手机:'.$phone.'</br>已经成功绑定微信:'.$userInfo['nickname']);
				$this->display('Flat:success');
				exit;
				//echo $phone.'--'.$userInfo['nickname'];
			}else{
				
				echo 'error';
			}
			
		}else{	
			AAA('绑定失败！');
		}

	}
	
	public function index(){
		
		//如果是微信 ，且是openid空
		//AAA($_COOKIE);
		//setcookie("open_id","",time()-1);
		//AAA($_COOKIE);
		
		$res = CMS_M('web_baseinfo')->where($where)->select();
		
		foreach($res as $key=>$val){
			
			if($val['info_key']=='title'){
				
				$title = $val['info_val'];	
				$id 	= id();
				$cat_id = cat_id();
				
				if(!empty($cat_id)){
					$all_parent = array();
					get_single_parent($cat_id,$all_parent);
					krsort($all_parent);
					foreach($all_parent as $single_Parent){
						if($single_Parent['title']==''){
							$title = $single_Parent['cat_name'].' - '.$title;
						}else{
							$title = $single_Parent['title'].' - '.$title;
						}
					}
					
				}
				
				if(!empty($cat_id) && !empty($id)){
					
					$top_parent_id = top_parent($cat_id);
					$res = CMS_M('category')->where('category_id='.$top_parent_id)->find();
				
					$table = CMS_M('cm_table')->where('table_id='.$res['module_id'])->find();
					$table_name = $table['table_name'];
					
					$table_m = CMS_M($table_name);
					$table_m_pk = CMS_M($table_name)->getPk();
					$art = CMS_M($table_name)->where($table_m_pk.'='.$id)->find();
					if($art['title']!=''){
						$title = $art['title'].' - '.$title;
					}
				}
				
				$this->assign('seo_'.$val['info_key'],$title);
			}else{
				
				$this->assign('seo_'.$val['info_key'],$val['info_val']);
			}
			
		}
		
		/*
		if(isWeixin() ){ // && 
			if(empty($_COOKIE["open_id"])  ||  empty($_COOKIE["wx_info"] )){
				$phone = '00000000000';
				//AAA('hhhh');
				$url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$this->APPID."&redirect_uri=".$this->REDIRECT_URI."&response_type=code&scope=snsapi_userinfo&state=".$phone."#wechat_redirect";
				//AAA($url);
				header('Location:'.$url);
				
				exit;
			}else{
				//AAA('iooo');
				$this->display("Flat:login");
				//$this->redirect('StudentApi/activity', array(), 0, '页面跳转中...');
			}
		}else{	  */
			
			if(empty($_COOKIE["user"])){
				$this->display("Flat:login");
			}else{
				
				
				$this->redirect('StudentApi/activity', array(), 0, '页面跳转中...');
			}
			
		/*} */
		
	}
	
	public function reg(){ 
		if(!empty($_REQUEST['acid'])){
			
			$this->assign('acid',$_REQUEST['acid']);
			
		}
		
		if($_REQUEST['add_type']==1){
			
			$this->display('Flat:form1');
		}
		if($_REQUEST['add_type']==2){
			
			$this->display('Flat:form3');
		}
		
		if($_REQUEST['add_type']==4){
			
			$this->display('Flat:form2');
		}
	}
	
	public function resshow(){
		
		$student_id =$_REQUEST['student_id'];
		
		$stuInfo = M('student')->where('student_id='.$student_id)->find();
		
		
		$actInfo = M('activity')->where('status!=1')->find();
		//AAA($actInfo);
		$this->assign('actInfo',$actInfo);
		
		$this->assign('stuInfo',$stuInfo);
		
		$this->display('Flat:resshow');
	}
	
	
	public function activity(){//AAA($_COOKIE);
		if(  empty($_COOKIE["user"]) ){ // && 
			
			//$this->redirect('StudentApi/index', array(), 0, '页面跳转中...');
			//exit;
		}
		
		$condition = array();
		$condition['add_phone'] = $_COOKIE["user"];
		$student = M('student')->where($condition)->find();
		$this->assign('studentInfo',$student);
		
		$already = M('acti_stus')->where("student_id=".$student['student_id'])->select();
		$ids = array();
		
		
		
		// status 0-可以报名 1-不显示 2-未开放
		$condition2 = array();
		$condition2['status'] = array('neq',1);
		if($_REQUEST['type'] =='isbaoming'){
			//$condition2['activity_id'] = array('in',$ids);
		}
		
		$acList = M('activity')->where($condition2)->select();
		
		
		$studentHasIn = array_filter(explode(',',$student['activitys']));
		
		$acCondition = array();
		$acCondition['student_id'] = $student['student_id'];
		$inActivity = M('acti_stus')->where($acCondition)->select();
		
		foreach($inActivity as $one){
			$ids[]=$one['activity_id'];
		}
		
		//AAA($inActivity);
		foreach($acList as $key=> $one){
			if(in_array($one['activity_id'],$ids)){
				$acList[$key]['is_baoming']=1;	

				
			}else{
				$acList[$key]['is_baoming']=0;	
				if($_REQUEST['type'] =='isbaoming' || $_REQUEST['type'] =='isscore' ){
				
					unset($acList[$key]);
				}
			}
			
			
		}
		//AAA($acList);
		$type = empty($_REQUEST['type'])? '': $_REQUEST['type'];
		
		
		
		$this->assign('acList',$acList);
		
		
		$actAci = M('activity')->where(' ( status=0 OR status=3 ) AND is_zhaosheng=1')->order('activity_id DESC')->find();
		
		$this->assign('actAci',$actAci);
		
		$isbao = 0;
		$nums = M('acti_stus')->where('student_id='.$student['student_id'].' AND activity_id=1')->select();
		
		if(  count($nums)>0  ){
			
			$isbao = 1;
		}
		
		$this->assign('isbao',$isbao);
		
		$actInfo = M('activity')->where('status!=1')->find();
		//AAA($actInfo);
		
		//AAA($actAci);
		if($type=='isscore'){

			
		
			
			
			$this->display('Flat:text2');
		}else{
			$this->assign('studentInfo',$student);
			$this->assign('actInfo',$actInfo);
			$this->display('Flat:activity');
		}
	}
	
	
	

	public function text(){
		
		$acid      = empty($_REQUEST['acid'])? 0: $_REQUEST['acid'];
		$volunteer = empty($_REQUEST['volunteer'])? 0: $_REQUEST['volunteer'];
		
		$info = M('activity')->where('activity_id='.$acid)->find();
		
		
		$this->assign('status',$info['status']);
		if($volunteer==1){
			
			$this->assign('content',$info['content']);
		}elseif($volunteer==2){
			
			$this->assign('content',$info['content']);
		}else{
			
			$this->assign('content',$info['content']);
		}
		
		$this->assign('actInfo',$info);
		
		$this->display('Flat:text');
	}

	
	
	
	
	
	
	
}

?>