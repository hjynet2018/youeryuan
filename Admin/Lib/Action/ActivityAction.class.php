<?php

class ActivityAction extends MythinkAction {
	
	public $cmTableName = 'Activity';
	//public $APPID     = 'wxb52637c9bfcfd0c8';
	//public $APPSECRET = '3d548b6b5e433d98bee540350decc35b';
	//public $REDIRECT_URI = 'http://t.ihecha.me/index.php/StudentApi/handelCode';
	
	public function _initialize(){
		$this->REDIRECT_URI= C('REDIRECT_URI');
		$this->APPID  = C('APPID');
		$this->APPSECRET  = C('APPSECRET');
		parent::_initialize();
	}
	
    //列表
	public function Activity_list(){
		
		Vendor('Form.TableList');
		
		$list	 =	  new TableList();		
		
		
		//获取搜索列表条件
		$where = $list->get_search_data($this->cmTableName);
		
		//根据条件获取列表
		$rs = $list->auto_gen_list($this->cmTableName,$where);
		
		//分配列表信息
		$this->assign('list',$rs['table_list']);
		//分配搜索表单信息
		$this->assign('searchForm',$rs['search_form']);
		
		
		
		//获取并分配添加按钮
		$add_btn = $list->gen_add_button($this->cmTableName);
		
		$this->assign('add_btn',$add_btn);
		
		
		//获取并分配Excel按钮
		$excel_btn = $list->get_excel_btn($this->cmTableName);
		
		$this->assign('excel_btn',$excel_btn);
		
		
		$this->display($this->cmTableName.'/list');
	}
	
	
	//添加
	public function Activity_add(){
		
		//获取添加表单并且分配
		$this->assign('form',D($this->cmTableName)->form->_getForm());
		
		$this->display($this->cmTableName.'/add');
	}
	
	//处理添加的数据（入库）
	public function Activity_addok(){
		
		//根据表名获取提交过来的数组
		$data=D($this->cmTableName)->form->recordToBuildData('add');
		
		$this->ADD_ONE($this->cmTableName,$data,U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']));
		
	}
	
	
	//编辑页面
	public function Activity_edit(){
		
		//获取表格主建
		$pk	=	M($this->cmTableName)->getPk();
		
		$pkVal	=	$_REQUEST[$pk];
		
		//根据主键获取编辑的表单并分配
		$res=M($this->cmTableName)->where($pk.'='.$pkVal)->find();
		
		$this->assign('form',D($this->cmTableName)->form->setAction('__APP__/'.$this->cmTableName.'/'.$this->cmTableName.'_editok')->_getEditorForm($res));
		
		$this->display($this->cmTableName.'/edit');	
	}
	
	
	//处理编辑的数据（入库）
	public function Activity_editok(){
		
		$pk=M($this->cmTableName)->getPk();
		
		//根据表名称获取编辑的数据数组并且入库
		$data=D($this->cmTableName)->form->recordToBuildData('edit');
		
		$this->EDIT_ONE($this->cmTableName,$data,U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']));	
		
	}
	
	
	//根据主建删除一条记录
	public function Activity_del(){
	
		$pk=M($this->cmTableName)->getPk();
		
		$this->DELETE_ONE($this->cmTableName,$_REQUEST[$pk],U($this->cmTableName.'/'.$this->cmTableName.'_list'));
	}
	
	
	//批量删除
	public function Activity_batch_del(){
		
		$ids = I('ids'); 
		
		$idsArr = explode(',',$ids);
		
		//根据主键数组批量删除记录
		foreach($idsArr as $val){
		
			$this->CM(strtolower($this->cmTableName))->delete($val);
		}
		
		$this->alert_jump('删除成功',U($this->cmTableName.'/'.$this->cmTableName.'_list'));
	}
	
	
	//模型Excel
	public function Activity_excel(){
		
		Vendor('Form.TableList');
		
		$list	 =	  new TableList();
		
		
		//获取Excel上传表单并且分配
		$excel_btn = $list->gen_excel_form($this->cmTableName);
		$this->assign('excel_btn',$excel_btn);
		
		//组将回删按钮并分配
		$back_del_btn = '<a href="'.U('Common/del_last_excel',array('table_name'=>$this->cmTableName)).'">回删记录</a>';
		$this->assign('back_del_btn',$back_del_btn);
		
		
		//根据表名获取上次Excel导入记录，拆分数组并且分配
		$last_excel = file_get_contents('./Public/reportData/'.ucfirst($this->cmTableName).'.txt');
		
		$last_excelArr = unserialize($last_excel);
		$this->assign('table_name',$last_excelArr['table_name']);
		$this->assign('last_time',$last_excelArr['last_time']);
		$this->assign('successRows',$last_excelArr['successRows']);
		$this->assign('errorEmptyRows',$last_excelArr['errorEmptyRows']);
		$this->assign('errorRepeatRows',$last_excelArr['errorRepeatRows']);
		$this->assign('errorInsertRows',$last_excelArr['errorInsertRows']);
		$this->assign('errorDataMapRows',$last_excelArr['errorDataMapRows']);
		
		$this->display($this->cmTableName.'/excel');
	}
	
	public function qrCode(){
		
		$activity_id = empty($_REQUEST['activity_id'])?0:$_REQUEST['activity_id'];
		
		vendor('phpqrcode.phpqrcode');
		
		$value = 'http://'.$_SERVER['HTTP_HOST'].U('Activity/qd',array('activity_id'=>$activity_id)); //二维码内容
		
		$url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$this->APPID."&redirect_uri=".$value."&response_type=code&scope=snsapi_userinfo&state=acsign#wechat_redirect";
		//AAA($url);
		//header('Location:'.$url);
		
		$errorCorrectionLevel = 'L';//容错级别   
		$matrixPointSize = 12;//生成图片大小   
	
		$filename = 'Admin/Runtime/Temp/product_view_'.$activity_id.'.png';
		
		if(!is_dir('Admin/Runtime/Temp/')){
			mkdir('Admin/Runtime/Temp/',0777,true);
		}
		
		//生成二维码图片   
		QRcode::png($url, $filename, $errorCorrectionLevel, $matrixPointSize, 2);   
		
		$result = array();
		$result['url'] = '/'.$filename;
		
		$this->assign('url',$filename);
		
		
		$Ainfo = M('activity')->where('activity_id='.$activity_id)->find();
		
		$this->assign('Ainfo',$Ainfo);
		
		$this->display();
	}
	
	public function qd(){
		
		$code = $_REQUEST['code'];
		$phone = empty($_REQUEST['state'])? '': $_REQUEST['state'];
		
		$url="https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$this->APPID."&secret=".$this->APPSECRET."&code=".$code."&grant_type=authorization_code";
		
		//$url='https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$_GET['code'].'&grant_type=authorization_code';
		//AAA($phone);
		$access_token = httpsGet($url);
		$openid = $access_token['openid'];
		
		$activity_id = $_REQUEST['activity_id'];
		
		$open_id = $openid;
		
		$where = array();
		$where['add_openid'] = $open_id;
		
		$stuInfo = M('student')->where($where)->find();
	
		if($stuInfo){
			
			$student_id = $stuInfo['student_id'];
			
			$where = array();
			$where['activity_id']    =  $activity_id;
			$where['student_id']     =  $student_id;
			
			$actiInfo = M('acti_stus')->where($where)->find();
			if(empty($actiInfo)){
				//AAA('123');
				$this->assign('msg','您未报名该活动');
				$this->assign('desc','请先到活动中心报名');
				$this->assign('errorStatus','warn');
				$this->display('Flat:success');
				exit;
			}	
			$dataSave = array();
			$dataSave['acti_stus_id'] = $actiInfo['acti_stus_id'];
			$dataSave['is_qd']        = 1;
			
			$res = M('acti_stus')->save($dataSave);
			
			
			$actiInfo = M('activity')->find($activity_id);
			$this->assign('msg','签到成功');
			$this->assign('desc',$actiInfo['title']);
			
			if($res){
				
				$this->display('Flat:success');
				
			}else{
				$this->display('Flat:success');
			}
		}else{
			
			$this->assign('msg','您未报名该活动');
			$this->assign('desc','请先到活动中心报名');
			$this->assign('errorStatus','warn');
			$this->display('Flat:success');
			exit;
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
?>