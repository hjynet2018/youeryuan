<?php
session_start();
class StudentAction extends MythinkAction {
	
	public $cmTableName = 'Student';
	
	public function _initialize(){
		
		parent::_initialize();
	}
	
	
	public function sendMsg(){
		
		$phone = empty($_REQUEST['phone'])?'':$_REQUEST['phone'];
		
		$sql = 'select * from message where phone="'.$phone.'" AND ('. time(). '-datetime)<60 order by message_id DESC limit 1';
		
		
		$is_in = M()->query($sql);
		
		$mresult =  array();
		
		if(count($is_in)>0){
			
			$mresult['status']   = -1;
			$mresult['second']   = 60-(time()-$is_in[0]['datetime']);
			
			die(json_encode($mresult));
			
		}else{
			
			$code = generate_code();
			
			
			$dataAdd = array();
			$dataAdd['phone'] 		=  $phone;
			$dataAdd['radom_code']  =  $code;
			$dataAdd['datetime']    =  time();
			
			$inser_id = M('message')->add($dataAdd);
			
			include('./aliyun-dysms-php-sdk/api_demo/SmsDemo.php');
			
			$dataMess = array();
			$dataMess['code']   =  $code;
			
			$response = SmsDemo::sendSms($phone,'SMS_151905318',$dataMess);
			
			
			if($response->Code=='OK'){
				
				
				$mresult['status']   = 1;
				
				$where = array();
				$where['add_phone'] = $phone;
				
				$res = M('student')->where($where)->find();
				
				
				if(empty($res)){
					
					$dataAdd = array();
					$dataAdd['add_phone'] 	= $phone;
					$dataAdd['ws_info']   	= '';
					
					$student_id = M('student')->Add($dataAdd);
				}
				
			}else{
				
				$mresult['status']   = 0;
			}
			
			
			
		}
		
		
		die(json_encode($mresult));
	}
	
	// is_weixin
	public function checkMsg(){
		
		$phone 			= empty($_REQUEST['phone'])?'':$_REQUEST['phone'];
		$radom_code 	= empty($_REQUEST['radom_code'])?'':$_REQUEST['radom_code'];
		$is_weixin 		= empty($_REQUEST['is_weixin'])?'':$_REQUEST['is_weixin'];
		$wx_openid 		= empty($_REQUEST['wx_openid'])?'':$_REQUEST['wx_openid'];
		$wx_wx_info 	= empty($_REQUEST['wx_wx_info'])?'':$_REQUEST['wx_wx_info'];
		
		$where = array();
		$where['phone'] 		= $phone;
		$where['radom_code']    = $radom_code;
		
		$result = M('message')->where($where)->find();
		
		if(is_weixin==1){
			
			$dataAdd = array();
			$dataAdd['phone'] 		= 	$phone;
			$dataAdd['ws_info'] 	= 	$wx_wx_info;
			$dataAdd['open_id'] 	= 	$wx_openid;
			
			$res = M('weixin_to_phone')->add($dataAdd);
			
		}
		
		$mresult = array();
		$mresult['status'] = 0;
		
		if(!empty($result)){
			
			
			$mresult['status'] = 1;
			
			M('message')->where(where)->delete();
		}
		
		die(json_encode($mresult));
	}
	
	public function addBaoMing(){
		
		//AAA($_REQUEST);
		
		$stept 					= empty($_REQUEST['stept'])?0:$_REQUEST['stept'];
		
		$student_id 			= empty($_REQUEST['student_id'])?0:$_REQUEST['student_id'];
		$name 					= empty($_REQUEST['name'])?'':$_REQUEST['name'];
		$sex 					= empty($_REQUEST['sex'])?0:$_REQUEST['sex'];
		$identity_type 			= empty($_REQUEST['identity_type'])?'':$_REQUEST['identity_type'];
		$identity 				= empty($_REQUEST['identity'])?'':$_REQUEST['identity'];
		$birth_date 			= empty($_REQUEST['birth_date'])?'':$_REQUEST['birth_date'];
		$volunteer 				= empty($_REQUEST['volunteer'])?'':$_REQUEST['volunteer'];
		$add_class 				= empty($_REQUEST['add_class'])?'':$_REQUEST['add_class'];
		
		$area 						= empty($_REQUEST['area'])?0:$_REQUEST['area'];
		$blood 						= empty($_REQUEST['blood'])?'':$_REQUEST['blood'];
		$nationality 				= empty($_REQUEST['nationality'])?'':$_REQUEST['nationality'];
		$nation 					= empty($_REQUEST['nation'])?'':$_REQUEST['nation'];
		
		$birth_address 				= empty($_REQUEST['birth_address'])?'':$_REQUEST['birth_address'];
		$native_place 				= empty($_REQUEST['native_place'])?'':$_REQUEST['native_place'];
		$is_hukou 					= empty($_REQUEST['is_hukou'])?0:$_REQUEST['is_hukou'];
		$hukou_address 				= empty($_REQUEST['hukou_address'])?'':$_REQUEST['hukou_address'];
		$hukou_type 				= empty($_REQUEST['hukou_type'])?0:$_REQUEST['hukou_type'];
		
		$police_station 			= empty($_REQUEST['police_station'])?'':$_REQUEST['police_station'];
		$juwei 						= empty($_REQUEST['juwei'])?'':$_REQUEST['juwei'];
		$hukou_character 			= empty($_REQUEST['hukou_character'])?0:$_REQUEST['hukou_character'];
		$agri_type 					= empty($_REQUEST['agri_type'])?0:$_REQUEST['agri_type'];
		$in_time 					= empty($_REQUEST['in_time'])?'':$_REQUEST['in_time'];
		$is_single 					= empty($_REQUEST['is_single'])?0:$_REQUEST['is_single'];
		$child_num 					= empty($_REQUEST['child_num'])?0:$_REQUEST['child_num'];
		$is_incity 					= empty($_REQUEST['is_incity'])?0:$_REQUEST['is_incity'];
		$is_stay 					= empty($_REQUEST['is_stay'])?0:$_REQUEST['is_stay'];
		
		$healthy 					= empty($_REQUEST['healthy'])?'':$_REQUEST['healthy'];
		$is_dischild 				= empty($_REQUEST['is_dischild'])?0:$_REQUEST['is_dischild'];
		
		$father 					= empty($_REQUEST['father'])?'':$_REQUEST['father'];
		$father_tell 				= empty($_REQUEST['father_tell'])?'':$_REQUEST['father_tell'];
		$father_ide 				= empty($_REQUEST['father_ide'])?'':$_REQUEST['father_ide'];
		$father_job 				= empty($_REQUEST['father_job'])?'':$_REQUEST['father_job'];
		$father_resi 				= empty($_REQUEST['father_resi'])?'':$_REQUEST['father_resi'];
		
		$now_address 				= empty($_REQUEST['now_address'])?'':$_REQUEST['now_address'];
		$address 					= empty($_REQUEST['address'])?'':$_REQUEST['address'];
		
		$mother 					= empty($_REQUEST['mother'])?'':$_REQUEST['mother'];
		$mother_tell 				= empty($_REQUEST['mother_tell'])?'':$_REQUEST['mother_tell'];
		$mother_ide 				= empty($_REQUEST['mother_ide'])?'':$_REQUEST['mother_ide'];
		$mother_job 				= empty($_REQUEST['mother_job'])?'':$_REQUEST['mother_job'];
		$mother_resi 				= empty($_REQUEST['mother_resi'])?'':$_REQUEST['mother_resi'];
		$plan_in 					= empty($_REQUEST['plan_in'])?0:$_REQUEST['plan_in'];
		
		$activitys 					= empty($_REQUEST['activitys'])?0:$_REQUEST['activitys'];
		
		$photo1 					= empty($_REQUEST['photo1'])? '':$_REQUEST['photo1'];
		$photo2 					= empty($_REQUEST['photo2'])? '':$_REQUEST['photo2'];
		$photo3 					= empty($_REQUEST['photo3'])? '':$_REQUEST['photo3'];
		
		$father_nick 					= empty($_REQUEST['father_nick'])? '':$_REQUEST['father_nick'];
		$mother_nick 					= empty($_REQUEST['mother_nick'])? '':$_REQUEST['mother_nick'];
		
		$other 					= empty($_REQUEST['other'])? '':$_REQUEST['other'];
		$other_nick 			= empty($_REQUEST['other_nick'])? '':$_REQUEST['other_nick'];
		$other_tell 			= empty($_REQUEST['other_tell'])? '':$_REQUEST['other_tell'];
		$other_job 				= empty($_REQUEST['other_job'])? '':$_REQUEST['other_job'];
		
		$father_post 				= empty($_REQUEST['father_post'])? '':$_REQUEST['father_post'];
		$mother_post 				= empty($_REQUEST['mother_post'])? '':$_REQUEST['mother_post'];
		$other_post 				= empty($_REQUEST['father_post'])? '':$_REQUEST['other_post'];
		
		
		$stuInfo = M('student')->where('student_id='.$student_id)->find();   //AAA($stuInfo);
		include('./aliyun-dysms-php-sdk/api_demo/SmsDemo.php');
		$return  = array();
		
		if($stept==1){
			
			$data['student_id'] 		= 		$student_id;
			$data['name'] 				= 		$name;
			$data['sex'] 				= 		$sex;
			$data['identity_type'] 		= 		$identity_type;
			$data['identity'] 			= 		$identity;
			$data['birth_date'] 		= 		$birth_date;
			$data['volunteer'] 			= 		$volunteer;
			$data['add_class'] 			= 		$add_class;
			$data['activitys'] 			= 		$activitys;
			$data['add_type'] 			= 		1;
			
			$res = M('student')->save($data);
			
			if( $res && $stuInfo['is_sms']==0 ){
				
				
			
				$dataMess = array();
				$dataMess['add_code']   =  getFullNum($student_id);
				
				$response = SmsDemo::sendSms($stuInfo['add_phone'],'SMS_151995482',$dataMess);
				
				if($response->Code=='OK'){
					
					$smsDataSave = array();
					$smsDataSave['student_id'] 	= $student_id;
					$smsDataSave['is_sms'] 	    = 1;
					
					M('student')->save($smsDataSave);
				}
				
				
			}
		}elseif($stept==2){
			
			$dataSave = array();
			$dataSave['student_id'] 			=    	$student_id;
			$dataSave['area'] 					=    	$area;
			$dataSave['blood'] 					=    	$blood;
			$dataSave['nationality'] 			=    	$nationality;
			$dataSave['nation'] 				=    	$nation;
			$dataSave['birth_address'] 			=    	$birth_address;
			$dataSave['native_place'] 			=    	$native_place;
			$dataSave['is_hukou'] 				=    	$is_hukou;
			$dataSave['hukou_address'] 			=    	$hukou_address;
			$dataSave['native_place'] 			=    	$native_place;
			$dataSave['hukou_type'] 			=    	$hukou_type;
			
			$dataSave['police_station'] 		=    	$police_station;
			$dataSave['juwei'] 					=    	$juwei;
			$dataSave['hukou_character'] 		=    	$hukou_character;
			$dataSave['agri_type'] 				=    	$agri_type;
			$dataSave['in_time'] 				=    	$in_time;
			$dataSave['is_single'] 				=    	$is_single;
			$dataSave['child_num'] 				=    	$child_num;
			$dataSave['is_incity'] 				=    	$is_incity;
			$dataSave['is_stay'] 				=    	$is_stay;
			$dataSave['healthy'] 				=    	$healthy;
			$dataSave['is_dischild'] 			=    	$is_dischild;
			
			$dataSave['father'] 				=    	$father;
			$dataSave['father_tell'] 			=    	$father_tell;
			$dataSave['father_ide'] 			=    	$father_ide;
			$dataSave['father_job'] 			=    	$father_job;
			$dataSave['father_resi'] 			=    	$father_resi;
	
			$dataSave['now_address'] 			=    	$now_address;
			$dataSave['address'] 				=    	$address;
			//$dataSave['plan_in'] 				=    	$plan_in;
			
			$dataSave['mother'] 				=    	$mother;
			$dataSave['mother_tell'] 			=    	$mother_tell;
			$dataSave['mother_ide'] 			=    	$mother_ide;
			$dataSave['mother_job'] 			=    	$mother_job;
			$dataSave['mother_resi'] 			=    	$mother_resi;
			
			$dataSave['activitys'] 				=    	$activitys;
			
			$dataSave['photo1'] 				=    	$photo1;
			$dataSave['photo2'] 				=    	$photo2;
			$dataSave['photo3'] 				=    	$photo3;
			
			$dataSave['father_nick'] 				=    	$father_nick;
			$dataSave['mother_nick'] 				=    	$mother_nick;
			
			$dataSave['other'] 						=    	$other;
			$dataSave['other_nick'] 				=    	$other_nick;
			$dataSave['other_tell'] 				=    	$other_tell;
			$dataSave['other_job'] 					=    	$other_job;
			
			$dataSave['father_post'] 					=    	$father_post;
			$dataSave['mother_post'] 					=    	$mother_post;
			$dataSave['other_post'] 					=    	$other_post;
			
			
			$dataSave['sex'] 				= 		$sex;
			$dataSave['identity_type'] 		= 		$identity_type;
			$dataSave['identity'] 			= 		$identity;
			$dataSave['birth_date'] 		= 		$birth_date;
			
			$res = M('student')->save($dataSave);
			
			if($res){
				
				$stuInfo = M('student')->where('student_id='.$student_id)->find();
				
				$actInfo = M('activity')->where('activity_id='.$stuInfo['activitys'])->find();
				
				if($stuInfo['is_sms2']!=1){
					
					// SMS_152505080
					//include('./aliyun-dysms-php-sdk/api_demo/SmsDemo.php');
				
					$dataMess = array();
					$dataMess['zhu_time']   =  date('Y-m-d H时',$actInfo['zhu_time']);
					
					//$response = SmsDemo::sendSms($stuInfo['add_phone'],'SMS_152505080',$dataMess);
					//$response = SmsDemo::sendSms($stuInfo['add_phone'],'SMS_152514626',$dataMess);
					$response = SmsDemo::sendSms($stuInfo['add_phone'],'SMS_152549898',$dataMess);
					
					if($response->Code=='OK'){
				
				
						$mdataSa = array();
						$mdataSa['student_id'] = $student_id;
						$mdataSa['is_sms2']    = 1;
						
						M('student')->save($mdataSa);
						
					}
				}
				
				
			}
			
		}elseif($stept==3){
			
			$data['student_id'] 		= 		$student_id;
			$data['name'] 				= 		$name;
			$data['sex'] 				= 		$sex;
			$data['identity_type'] 		= 		$identity_type;
			$data['identity'] 			= 		$identity;
			$data['birth_date'] 		= 		$birth_date;
			$data['volunteer'] 			= 		$volunteer;
			$data['add_class'] 			= 		$add_class;
			$data['plan_in'] 			= 		$plan_in;
			$data['activitys'] 			= 		$activitys;
			$data['add_type'] 			= 		2;
			
			$res = M('student')->save($data);
		}
		
		
		
		if($res){
			
			$return['status'] = 1;
			
			$actInfo = M('activity')->where('activity_id='.$activitys)->find();
			
			$return['data'] = $actInfo;
			
		}else{
			
			$return['status'] = 0;
		}
		
		die(json_encode($return));
	}
	
	
	public function multiGroup(){
		
		$mt_res 			= empty($_REQUEST['mt_res'])?'':$_REQUEST['mt_res'];
		$grouping_id 	    = empty($_REQUEST['grouping_id'])?0:$_REQUEST['grouping_id'];
		$ids 	    		= empty($_REQUEST['ids'])?'':$_REQUEST['ids'];
		
		$idsArr = array();
		$idsArr = explode(',',$ids);
		
		
		foreach($idsArr as $val){
			
			$dataSave = array();
			$dataSave['grouping_id'] = $grouping_id;
			$dataSave['student_id']  = $val;
			
			M('student')->save($dataSave);
		}
		
		//$sql  =  'update student set grouping_id='.$grouping_id.' where mt_res="'.$mt_res.'"';
		//AAA($sql);
		//M()->query($sql);
		
		die('1');
		
		
	}
	
	public function identity(){
		
		$identity 				= empty($_REQUEST['identity'])?'':$_REQUEST['identity'];
		$student_id 			= empty($_REQUEST['student_id'])?0:$_REQUEST['student_id'];
		
		$where = array();
		$where['identity'] = $identity;
		
		$count = M('student')->where('identity="'.$identity.'" AND student_id!='.$student_id)->count();
		
		if($count>0){
			
			die('0');
		}else{
			
			die('1');
		}
	}
	
	
	public function getStuInfo(){
		
		$student_id 			= empty($_REQUEST['student_id'])?0:$_REQUEST['student_id'];
		
		$studentInfo = M('student')->where('student_id='.$student_id)->find();
		
		$studentInfo['fullNum'] = getFullNum($student_id);
		
		die(json_encode($studentInfo));
	}
	
	public function getInfo(){
		
		$add_phone 			= empty($_REQUEST['add_phone'])?0:$_REQUEST['add_phone'];
		
		$studentInfo = M('student')->where('add_phone="'.$add_phone.'"')->find();
		
		$studentInfo['birth_date2'] = date('Y-m-d',intval($studentInfo['birth_date2']/1000));
		
		$studentInfo['fullNum'] 	= getFullNum($studentInfo['student_id']);
		
		die(json_encode($studentInfo));
	}
	
	public function getMenu(){
		
		$list_id 			= empty($_REQUEST['list_id'])?0:$_REQUEST['list_id'];
		
		$list = M('cm_list_item')->where('list_id='.$list_id)->select();
		
		die(json_encode($list));
	}
	
	public function dayin2(){
		
		$student_id 			= empty($_REQUEST['student_id'])?0:$_REQUEST['student_id'];
		
		$studentInfo = M('student')->where('student_id='.$student_id)->find();
		
		$this->assign('studentInfo',$studentInfo);
		
		$actInfo = M('activity')->where('activity_id='.$studentInfo['activitys'])->find(); //AAA($actInfo);
		
		$this->assign('actInfo',$actInfo);
		
		$this->display('Flat:dayin');
	}
	
    //列表
	public function Student_list(){
		//AAA($_REQUEST);
		Vendor('Form.TableList');
		
		$list	 =	  new TableList();		
		
		//AAA('12');
		
		$_REQUEST['mt_res'] = urldecode($_REQUEST['mt_res']);
		//AAA($_REQUEST);
		//获取搜索列表条件
		$where = $list->get_search_data($this->cmTableName);
		//AAA($where);
		if(empty($where)){
			
			$where['add_type'] = '1';
		}
		
		if($_REQUEST['grouping_id2']==1){
			
			$where['add_type']    = '1';
			$where['grouping_id'] = '0';
		}
		
		//AAA($_REQUEST);
		//根据条件获取列表
		$rs = $list->auto_gen_list($this->cmTableName,$where);
		
		//分配列表信息
		$this->assign('list',$rs['table_list']);
		//分配搜索表单信息
		$this->assign('searchForm',$rs['search_form']);
		
		
		
		//获取并分配添加按钮
		$add_btn = $list->gen_add_button($this->cmTableName);
		
		$this->assign('add_btn',$add_btn);
		
		
		//获取并分配Excel按钮
		$excel_btn = $list->get_excel_btn($this->cmTableName);
		
		$this->assign('excel_btn',$excel_btn);
		
		//获取招生时段信息
		$where = array();
		//$where['status'] 		= 0;
		$where['is_zhaosheng']  = 1;
		
		$activity  = M('activity')->where($where)->find();
		$actiTimes = M('acti_time')->where('activity_id='.$activity['activity_id'])->order('acti_time_id DESC')->select();
		
		$this->assign('actiTimes',$actiTimes);
		
		
		$groupList = M('grouping')->select();
		
		$this->assign('groupList',$groupList);
		
		$this->display($this->cmTableName.'/list');
	}
	
	
	//添加
	public function Student_add(){
		
		//获取添加表单并且分配
		$this->assign('form',D($this->cmTableName)->form->_getForm());
		
		$this->display($this->cmTableName.'/add');
	}
	
	//处理添加的数据（入库）
	public function Student_addok(){
		
		//根据表名获取提交过来的数组
		$data=D($this->cmTableName)->form->recordToBuildData('add');
		
		$Y = date('nd',time());
		//AAA($data);
		$id = M('student')->add($data);
		//AAA($id);
		$id2 = '';
		
		if($id>=1000){ $id2 = ''+$id; }
		if($id<1000){  $id2 = '0'+''+$id; }
		if($id<100){   $id2 = '00'+''+$id;  }
		if($id<10){    $id2 = '000'+''+$id; }
		
		$dataSave = array();
		$dataSave['add_code'] = $Y.$id2;
		$dataSave['student_id'] = $id;
		
		if($id){
			
			M('student')->save($dataSave);
			
			$actyArr  = explode(',',$data['activitys']); 
			
			if(!empty($actyArr)){
			
				foreach($actyArr as $val){    
					
					$dataAdd = array();
					$dataAdd['student_id']   =   $id;
					$dataAdd['activity_id']  =   $val;
					
					$count = M('acti_stus')->where($dataAdd)->count();
					
					if( !($count>0) ){
						
						$dataAdd['datetime'] =   time();
						M('acti_stus')->add($dataAdd);
					}
				}
			}
			
			$this->alert_jump('操作成功',U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']) );
		}else{
			
			$this->alert_back('操作失败');
		}
		
		//$this->ADD_ONE($this->cmTableName,$data,U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']));
		
	}
	
	
	//编辑页面
	public function Student_edit(){
		
		//获取表格主建
		$pk	=	M($this->cmTableName)->getPk();
		
		$pkVal	=	$_REQUEST[$pk];
		
		//根据主键获取编辑的表单并分配
		$res=M($this->cmTableName)->where($pk.'='.$pkVal)->find();
		
		$this->assign('form',D($this->cmTableName)->form->setAction('__APP__/'.$this->cmTableName.'/'.$this->cmTableName.'_editok')->_getEditorForm($res));
		
		$this->display($this->cmTableName.'/edit');	
	}
	
	
	//处理编辑的数据（入库）
	public function Student_editok(){
		
		$pk=M($this->cmTableName)->getPk();
		
		//根据表名称获取编辑的数据数组并且入库
		$data=D($this->cmTableName)->form->recordToBuildData('edit');
		
		// if($data['mt_num']!=''){
			
			// $stuInfo = M('student')->where('student_id='.$data['student_id'])->find();
			
			// $sql = '';
			// $sql = 'select * from student where mt_num="'.$data['mt_num'].'" AND student_id !='.$data['student_id'];
			
			// $res = M()->query($sql);
			
			// if(!empty($res)){
				
				// $this->alert_back('编辑失败！！该面谈证号已经被使用!!');
			// }
		// }
		
		$actyArr  = explode(',',$data['activitys']); 
			
		if(!empty($actyArr)){
			
			foreach($actyArr as $val){    
				
				$dataAdd = array();
				$dataAdd['student_id']   =   $data['student_id'];
				$dataAdd['activity_id']  =   $val;
				
				$count = M('acti_stus')->where($dataAdd)->count();
				
				if( !($count>0) ){ 
					
					$dataAdd['datetime'] =   time();
					M('acti_stus')->add($dataAdd);
				}
			}
		}
		
		$this->EDIT_ONE($this->cmTableName,$data,U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']));	
		
	}
	
	
	//根据主建删除一条记录
	public function Student_del(){
	
		$pk=M($this->cmTableName)->getPk();
		
		$this->DELETE_ONE($this->cmTableName,$_REQUEST[$pk],U($this->cmTableName.'/'.$this->cmTableName.'_list'));
	}
	
	
	//批量删除
	public function Student_batch_del(){
		
		$ids = I('ids'); 
		
		$idsArr = explode(',',$ids);
		
		//根据主键数组批量删除记录
		foreach($idsArr as $val){
		
			$this->CM(strtolower($this->cmTableName))->delete($val);
		}
		
		$this->alert_jump('删除成功',U($this->cmTableName.'/'.$this->cmTableName.'_list'));
	}
	
	
	public function dayin(){
		
		
		$student_id = empty($_REQUEST['student_id'])?'0':$_REQUEST['student_id'];
		
		$studentInfo = M('student')->where('student_id='.$student_id)->find();
		
		$this->assign('studentInfo',$studentInfo);
		
		$mtInfo = M('relation')->where('student_id='.$studentInfo['student_id'])->find();
		
		$this->assign('mtInfo',$mtInfo);
		
		//活动信息 2017—2018学年初一年级自主招生
		
		$where = array();
		//$where['status'] = 0;
		$where['is_zhaosheng'] = 1;
		$actiInfo = M('activity')->where($where)->find();  
		
		$this->assign('actiInfo',$actiInfo);
		
		
		//锁定报名表
		// $dataSave = array();
		// $dataSave['is_confirm'] = 1;
		
		// $res = M('student')->where('student_id='.$student_id)->save($dataSave);
		
		if(empty($mtInfo)){
			
			//$this->alert_back('请先完善面谈凭证号信息！');
		}
		/*
		$zuInfo = M('grouping')->where('grouping_id='.$studentInfo['group_class'])->find();
		
		$this->assign('zuInfo',$zuInfo);
		
		$mtInfo = M('relation')->where('mt_num="'.$studentInfo['mt_num'].'"')->find();
		
		if(empty($mtInfo)){
			
			$sql =  '';
			$sql =  'update relation set student_id='.$student_id.' where student_id="" limit 1';
			
			$res = M()->query($sql);
			
			if($res){
				
				$mtInfo = M('relation')->where('student_id='.$student_id)->find();
			}
		}
		
		$this->assign('mtInfo',$mtInfo);
		*/
		
		$this->assign('student_id',$student_id);
		
		$this->display();
	}
	
	public function confirmData(){
		
		$student_id = empty($_REQUEST['student_id'])?'0':$_REQUEST['student_id'];
		
		if($student_id!=0){
			
			$dataSave = array();
			$dataSave['is_confirm'] = 1;
			
			
			$res = M('student')->where('student_id='.$student_id)->save($dataSave);
			
			die('1');
			
		}else{
			
			die('-1');
		}
	}
	
	
	public function mt(){
		
		
		$student_id = empty($_REQUEST['student_id'])?'0':$_REQUEST['student_id'];
		
		$studentInfo = M('student')->where('student_id='.$student_id)->find();
		
		$this->assign('studentInfo',$studentInfo);
		
		$mtInfo = M('relation')->where('student_id='.$studentInfo['student_id'])->find();
		
		if(empty($mtInfo)){
			
			$this->alert_back('请先完善分组号，面谈凭证号等信息！');
		}
		
		$mtInfo = M('relation')->where('student_id='.$studentInfo['student_id'])->find();
		
		$this->assign('mtInfo',$mtInfo);
		
		$this->display();
	}
	
	public function mt2(){
		
		
		$student_id = empty($_REQUEST['student_id'])?'0':$_REQUEST['student_id'];
		
		$studentInfo = M('student')->where('student_id='.$student_id)->find();
		
		$this->assign('studentInfo',$studentInfo);
		
		$mtInfo = M('relation')->where('student_id='.$studentInfo['student_id'])->find();
		
		if(empty($mtInfo)){
			
			$this->alert_back('请先完善分组号，面谈凭证号等信息！');
		}
		
		$mtInfo = M('relation')->where('student_id='.$studentInfo['student_id'])->find();
		
		$this->assign('mtInfo',$mtInfo);
		
		$this->display();
	}
	
	public function getStuInfo22(){
		
		$student_id = empty($_REQUEST['student_id'])?'0':$_REQUEST['student_id'];
		
		$studentInfo = M('student')->where('student_id='.$student_id)->find();
		
		$count = M('relation')->where('student_id='.$student_id)->count();
		
		if($count>0){
			
			die('1');
		}else{
			
			die('0');
		}
	}
	
	public function addBao(){
		
		header('Content-Type:text/html;charset=utf-8'); 
		
		$student_id = empty($_REQUEST['student_id'])?'0':$_REQUEST['student_id'];
		$fenzu 		= empty($_REQUEST['fenzu'])?'':$_REQUEST['fenzu'];
		$acti_time  = empty($_REQUEST['acti_time'])?'0':$_REQUEST['acti_time'];
		//AAA($acti_time);
		
		$timeStr = '';
		if($acti_time==7){
			
			$timeStr = '6月27日上午';
		}
		
		if($acti_time==2){
			
			$timeStr = '6月27日下午';
		}
		
		$dataSave = array();
		$dataSave['student_id'] = $student_id;
		$dataSave['fenzu'] 		= $fenzu;
		$dataSave['acti_time'] 	= $acti_time;
		
		$res = M('student')->save($dataSave);
		
		if($res){
			
			$actiInfo = M('acti_time')->where('acti_time_id='.$acti_time)->find();
			//AAA($actiInfo);
			$relations = M('relation')->where('mt_date="'.$actiInfo['title'].'" AND student_id=""')->select();
			//AAA($actiInfo);
			$sql = '';
			//$sql = 'update relation set student_id='.$student_id.' where mt_date="'.$actiInfo['title'].'" AND student_id=0 limit 1';
			$sql = 'update relation set student_id='.$student_id.' where mt_date="'.$timeStr.'" AND student_id=0 limit 1';
			//$sql = 'update relation set student_id='.$student_id.' where mt_date="6月27日上午" AND student_id=0 limit 1';
			//AAA($sql);
			$upRes = M()->query($sql);
			
			
			die('1');
		}else{
			
			die('0');
		}
			
			
			
		
			
			
		
	}
	
	
	public function changes(){
		
		$ids = empty($_REQUEST['ids'])? '':$_REQUEST['ids'];
		
		$idArr = explode(',',$ids);
		
		
		foreach($idArr as $val){
			
			$dataSave = array();
			$dataSave['student_id'] = $val;
			$dataSave['plan_in']    = 0;
			$dataSave['add_type']   = 1;
			
			M('student')->save($dataSave);
			
			
		}
		$this->alert_jump('操作成功',U('Student/Student_list'));
		//die('1');
		//AAA($idArr);
	}
	
	public function getNums(){
		
		header("Content-type: text/html; charset=utf-8"); 
		
		$acti_time_id = empty($_REQUEST['acti_time_id'])?'0':$_REQUEST['acti_time_id'];
		
		//获取招生时段信息
		$actiTimes = M('acti_time')->where('acti_time_id='.$acti_time_id)->find();
		
		$sql = '';
		$sql = 'select count(*) from relation where mt_date="'.$actiTimes['title'].'" AND student_id=0';
		
		$res = M()->query($sql);
		
		die($res[0]['count(*)']);
	}
	
	
	//模型Excel
	public function Student_excel(){
		
		Vendor('Form.TableList');
		
		$list	 =	  new TableList();
		
		
		//获取Excel上传表单并且分配
		$excel_btn = $list->gen_excel_form($this->cmTableName);
		$this->assign('excel_btn',$excel_btn);
		
		//组将回删按钮并分配
		$back_del_btn = '<a href="'.U('Common/del_last_excel',array('table_name'=>$this->cmTableName)).'">回删记录</a>';
		$this->assign('back_del_btn',$back_del_btn);
		
		
		//根据表名获取上次Excel导入记录，拆分数组并且分配
		$last_excel = file_get_contents('./Public/reportData/'.ucfirst($this->cmTableName).'.txt');
		
		$last_excelArr = unserialize($last_excel);
		$this->assign('table_name',$last_excelArr['table_name']);
		$this->assign('last_time',$last_excelArr['last_time']);
		$this->assign('successRows',$last_excelArr['successRows']);
		$this->assign('errorEmptyRows',$last_excelArr['errorEmptyRows']);
		$this->assign('errorRepeatRows',$last_excelArr['errorRepeatRows']);
		$this->assign('errorInsertRows',$last_excelArr['errorInsertRows']);
		$this->assign('errorDataMapRows',$last_excelArr['errorDataMapRows']);
		
		$this->display($this->cmTableName.'/excel');
	}
	
	
	public function mt_excel_in(){
		
		error_reporting(0);
		set_time_limit(0);
		ini_set("memory_limit", "10240M");
		
		$table_name = 'student';
		
		vendor('Form.Excel.PHPExcel');
		
		if(isset($_FILES['cm_excel_file_up']['name']) && $_FILES['cm_excel_file_up']['name']!='')
		{	
			$excel = $this->FILE_UPLOAD('files');
			
		}else{
			$this->alert_back('请选择文件');
		}
		
	
		$excel = '.'.$excel['url'];
		
		
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($excel);
		$objWorksheet = $objPHPExcel->getActiveSheet(0);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		
		$excelData = array();
		for ($row = 1;$row <= $highestRow;$row++){
			for ($col = 0;$col < $highestColumnIndex;$col++){
				$excelData[$row][] =(string)$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
			}
		}
		
		
		
		
		//忽略Excel的第一行，默认为表头信息
		unset($excelData[1]);
		
		
		
		//去掉所有记录两边的空格
		foreach($excelData as $key=>$row){
			$one_row = array();
			foreach($row as $key2=>$cell){
				$one_row[]=trim($cell);
			}
			
			$excelData[$key] = $one_row;
		}
		//AAA($excelData);
		
		$allRes = array();
		
		foreach($excelData as $val){
			echo $val[1].'---'.$val[2].'<br>';
			//$whereSave = array();
			//$whereSave['add_code'] = $val[1];
			
			$dataSave = array();
			$dataSave['student_id']   =   intval(findNum($val[1]));
			$dataSave['mt_res']       =   '已录取';
			
			$allRes[] = $dataSave;
			//AAA($dataSave);
			M('student')->save($dataSave);
			
		}
		AAA($allRes);
		exit;
		//$this->success('导入成功');
	}
	
	public function mt_excel_out(){
		error_reporting(0);
		set_time_limit(0);
		ini_set("memory_limit", "10240M");
		$table_name = 'student';
		
		$activity_id = empty($_REQUEST['activity_id'])?'0':$_REQUEST['activity_id'];
		
		$stuAll = M('acti_stus')->where('activity_id='.$activity_id)->select();
		
		$stuStr = '';
		
		
		
		//$where[] = 
		//$allRecord = M('student')->where($stuStr)->select();
		//$allRecord = M('student')->where($stuStr)->select();
		
		//$sql = 'SELECT a.datetime AS atime, b.* FROM `acti_stus` AS a LEFT JOIN student AS b ON a.`student_id` = b.`student_id` WHERE b.add_code!="" AND a.`activity_id` = '.$activity_id;
		
		$allRecord = M('student')->where('activitys='.$activity_id)->select();
		
		
		
		foreach($allRecord as $key=>$val){
			
			
			
			$allRecord[$key]['number'] = getFullNum($val['student_id']);
			
		}
	
		//AAA($allRecord);
		
		vendor('Form.Excel.PHPExcel');
		$objExcel  = new PHPExcel();
		$objExcel->getProperties()->setCreator("Maarten Balliauw");
		ob_clean();
		$objWriter = new PHPExcel_Writer_Excel2007($objExcel); 
		$objProps = $objExcel->getProperties ();
		$objExcel->setActiveSheetIndex( 0 );
		$objActSheet = $objExcel->getActiveSheet ();
		
		
		$index = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY',
		'AZ','BA','BB','BC','BD','BF','BG','BH','BI','BJ','BK','BL'
		);
		
		$objActSheet->setCellValue ( $index[0].'1', '报名号',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[1].'1', '幼儿姓名',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[2].'1', '性别',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[3].'1', '证件类型',PHPExcel_Cell_DataType::TYPE_STRING);
		//$objActSheet->setCellValue ( $index[4].'1', '语文',PHPExcel_Cell_DataType::TYPE_STRING);
		//$objActSheet->setCellValue ( $index[5].'1', '数学',PHPExcel_Cell_DataType::TYPE_STRING);
		//$objActSheet->setCellValue ( $index[6].'1', '英语',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[4].'1', '证件号码',PHPExcel_Cell_DataType::TYPE_STRING);
		//$objActSheet->setCellValue ( $index[8].'1', '录取线',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[5].'1', '录取结果',PHPExcel_Cell_DataType::TYPE_STRING);
		//$objActSheet->setCellValue ( $index[5].'1', '录取结果',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[6].'1', '出生日期',PHPExcel_Cell_DataType::TYPE_STRING);
		//$objActSheet->setCellValue ( $index[7].'1', '健康状况',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[7].'1', '报读志愿',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[8].'1', '报读年级',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[9].'1', '家长联系电话',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[10].'1', '港澳台华侨',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[11].'1', '血型',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[12].'1', '国籍',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[13].'1', '民族',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[14].'1', '出生所在地',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[15].'1', '籍贯',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[16].'1', '是否有户口',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[17].'1', '户口所在地',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[18].'1', '户口所属地',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[19].'1', '派出所',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[20].'1', '村居委',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[21].'1', '户口性质',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[22].'1', '非农业户口类型',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[23].'1', '家庭现住址',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[24].'1', '入园时间',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[25].'1', '是否独生子女',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[26].'1', '所属孩次',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[27].'1', '是否留守',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[28].'1', '是否进城务工人员子女',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[29].'1', '健康状况',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[30].'1', '是否残疾幼儿',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[31].'1', '监护人一姓名',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[32].'1', '监护人一称谓',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[33].'1', '监护人一电话',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[34].'1', '监护人一身份证',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[35].'1', '监护人一工作',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[36].'1', '监护人一户口',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[37].'1', '监护人一职务',PHPExcel_Cell_DataType::TYPE_STRING);
		
		
		$objActSheet->setCellValue ( $index[38].'1', '监护人二姓名',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[39].'1', '监护人二称谓',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[40].'1', '监护人二电话',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[41].'1', '监护人二身份证',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[42].'1', '监护人二工作',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[43].'1', '监护人二户口',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[44].'1', '监护人二职务',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[45].'1', '其他监护成员',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[46].'1', '其他监护人称谓',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[47].'1', '其他监护人电话',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[48].'1', '其他监护人工作单位',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[49].'1', '其他监护人职位',PHPExcel_Cell_DataType::TYPE_STRING);
		
		//$objActSheet->setCellValue ( $index[45].'1', '现居住地',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[50].'1', '报名类型',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[51].'1', '身份证或护照',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[52].'1', '户口本主页',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[53].'1', '户口本幼儿页',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[54].'1', '身份证或护照链接',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[55].'1', '户口本主页链接',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[56].'1', '户口本幼儿页链接',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[57].'1', '学生ID',PHPExcel_Cell_DataType::TYPE_STRING);
		$objActSheet->setCellValue ( $index[58].'1', '所属短信分组',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$objActSheet->setCellValue ( $index[58].'1', '锁定状态',PHPExcel_Cell_DataType::TYPE_STRING);
		
		$index =  0;
		
		$cats1 = global_cat_item(4);
		$cats2 = global_cat_item(5);
		$schs = global_cat_item(6); 		
		//AAA($schs);
		foreach($allRecord as $recordKey=>$Row){
			
			$objActSheet->setCellValue ('A'.($index+2),$Row['number'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('B'.($index+2),$Row['name'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			//$desc = $value=='1'?'男':'女';				
			//$desc = $value=='0'?'未完善':$desc;
			
			$desc = '';
			$value = $Row['sex'];
			if($value=='1'){ $desc = '男'; }
			if($value=='2'){ $desc = '女'; }
			if($value=='0'){ $desc = '未完善'; }
			
			$objActSheet->setCellValue ('C'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$desc = '';
			$value = $Row['identity_type'];
			if($value=='1'){ $desc = '居民身份证'; }
			if($value=='2'){ $desc = '香港居民身份证'; }
			if($value=='3'){ $desc = '澳门居民身份证'; }
			if($value=='4'){ $desc = '护照'; }
			
			$objActSheet->setCellValue ('D'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValueExplicit('E'.($index+2),$Row['identity'],PHPExcel_Cell_DataType::TYPE_STRING);
			//$objActSheet->setCellValue ('E'.($index+2),$Row['identity'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('F'.($index+2),$Row['mt_res'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('G'.($index+2),date('Y-m-d',$Row['birth_date']),PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$desc = '';
			$value = $Row['volunteer'];
			if($value=='1'){ $desc = '国语班'; }
			if($value=='2'){ $desc = '国际班'; }
			if($value=='3'){ $desc = '均可'; }
			
			$objActSheet->setCellValue ('H'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['add_class'];
			if($value=='1'){ $desc = '小小班'; }
			if($value=='2'){ $desc = '小班'; }
			if($value=='3'){ $desc = '中班'; }
			if($value=='4'){ $desc = '大班'; }
			
			$objActSheet->setCellValue ('I'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('J'.($index+2),$Row['add_phone'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$desc = '';
			$value = $Row['area'];
			if($value=='1'){ $desc = '香港'; }
			if($value=='2'){ $desc = '澳门'; }
			if($value=='3'){ $desc = '台湾'; }
			if($value=='4'){ $desc = '非港澳台'; }
			if($value=='5'){ $desc = '华侨'; }
			$objActSheet->setCellValue ('K'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$objActSheet->setCellValue ('L'.($index+2),$Row['blood'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('M'.($index+2),$Row['nationality'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('N'.($index+2),$Row['nation'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('O'.($index+2),$Row['birth_address'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('P'.($index+2),$Row['native_place'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['is_hukou'];
			if($value=='1'){ $desc = '是'; }
			if($value=='2'){ $desc = '否'; }
			$objActSheet->setCellValue ('Q'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('R'.($index+2),$Row['hukou_address'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['hukou_type'];
			if($value=='1'){ $desc = '广东省外'; }
			if($value=='2'){ $desc = '广东省内佛山市外'; }
			if($value=='3'){ $desc = '佛山市内顺德区外'; }
			if($value=='4'){ $desc = '顺德区内'; }
			$objActSheet->setCellValue ('S'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$objActSheet->setCellValue ('T'.($index+2),$Row['police_station'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('U'.($index+2),$Row['juwei'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['hukou_character'];
			if($value=='1'){ $desc = '农业户口'; }
			if($value=='2'){ $desc = '非农业户口'; }
			$objActSheet->setCellValue ('V'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['agri_type'];
			if($value=='1'){ $desc = '城市'; }
			if($value=='2'){ $desc = '县城'; }
			if($value=='3'){ $desc = '乡镇'; }
			$objActSheet->setCellValue ('W'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('X'.($index+2),$Row['address'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$str = '';
			$actInfo = M('activity')->where('activity_id='.$Row['activitys'])->find();
			if(!empty($actInfo)){
				$str = $actInfo['in_time'];
			}
			
			$objActSheet->setCellValue ('Y'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['is_single'];
			if($value=='1'){ $desc = '是'; }
			if($value=='2'){ $desc = '否'; }
			$objActSheet->setCellValue ('Z'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('AA'.($index+2),$Row['child_num'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['is_single'];
			if($value=='1'){ $desc = '是'; }
			if($value=='2'){ $desc = '否'; }
			$objActSheet->setCellValue ('AB'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['is_single'];
			if($value=='1'){ $desc = '是'; }
			if($value=='2'){ $desc = '否'; }
			$objActSheet->setCellValue ('AC'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('AD'.($index+2),$Row['healthy'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$desc = '';
			$value = $Row['is_dischild'];
			if($value=='1'){ $desc = '是'; }
			if($value=='2'){ $desc = '否'; }
			$objActSheet->setCellValue ('AE'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('AF'.($index+2),$Row['father'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AG'.($index+2),$Row['father_nick'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AH'.($index+2),$Row['father_tell'],PHPExcel_Cell_DataType::TYPE_STRING);
			//$objActSheet->setCellValue ('AI'.($index+2),$Row['father_ide'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValueExplicit('AI'.($index+2),$Row['father_ide'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('AJ'.($index+2),$Row['father_job'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AK'.($index+2),$Row['father_resi'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AL'.($index+2),$Row['father_post'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('AM'.($index+2),$Row['mother'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AN'.($index+2),$Row['mother_nick'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AO'.($index+2),$Row['mother_tell'],PHPExcel_Cell_DataType::TYPE_STRING);
			//$objActSheet->setCellValue ('AP'.($index+2),$Row['mother_ide'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValueExplicit('AP'.($index+2),$Row['mother_ide'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AQ'.($index+2),$Row['mother_job'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AR'.($index+2),$Row['mother_resi'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AS'.($index+2),$Row['mother_post'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('AT'.($index+2),$Row['other'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AU'.($index+2),$Row['other_nick'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AV'.($index+2),$Row['other_tell'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AW'.($index+2),$Row['other_job'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objActSheet->setCellValue ('AX'.($index+2),$Row['other_post'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			//$objActSheet->setCellValue ('AY'.($index+2),$Row['now_address'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$desc = '';
			$value = $Row['add_type'];
			if($value=='1'){ $desc = '本年'; }
			if($value=='2'){ $desc = '次年'; }
			$objActSheet->setCellValue ('AY'.($index+2),$desc,PHPExcel_Cell_DataType::TYPE_STRING);
			// 
			//$str = ;
			
			
			$str = '';
			if(!empty($Row['photo1'])){  $str = '=HYPERLINK(".'.$Row['photo1'].'","点击查看")'; }
			$objActSheet->setCellValue ('AZ'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$str = '';
			if(!empty($Row['photo2'])){  $str = '=HYPERLINK(".'.$Row['photo2'].'","点击查看")'; }
			$objActSheet->setCellValue ('BA'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$str = '';
			if(!empty($Row['photo3'])){  $str = '=HYPERLINK(".'.$Row['photo3'].'","点击查看")'; }
			$objActSheet->setCellValue ('BB'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			//$str = 'http://kids.desheng-school.cn'.$Row['photo1'];
			
			$str = '';
			if(!empty($Row['photo1'])){  $str = 'http://kids.desheng-school.cn'.$Row['photo1']; }
			$objActSheet->setCellValue ('BC'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$str = '';
			if(!empty($Row['photo2'])){  $str = 'http://kids.desheng-school.cn'.$Row['photo2']; }
			$objActSheet->setCellValue ('BD'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$str = '';
			if(!empty($Row['photo3'])){  $str = 'http://kids.desheng-school.cn'.$Row['photo3']; }
			$objActSheet->setCellValue ('BF'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objActSheet->setCellValue ('BG'.($index+2),$Row['student_id'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$str = '';
			if($Row['grouping_id']==0){
				$str = '未分组';
			}else{
				
				$groupList = M('grouping')->select();
				
				foreach($groupList as $group){
					
					if($Row['grouping_id']==$group['grouping_id']){
						
						$str = $group['title'];
					}
				}
			}
			
			$objActSheet->setCellValue ('BH'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$str = '';
			if($Row['is_confirm']==0){
				$str = '未锁定';
			}else{
				
				$str = '已确认并锁定';
			}
			
			$objActSheet->setCellValue ('BI'.($index+2),$str,PHPExcel_Cell_DataType::TYPE_STRING);
			
			$index = $index + 1;
			
			//ob_flush();
			//flush();
		}
		
		$fileName = 'Admin/Runtime/myExcel'.time(). '.xlsx';
		
		$outputFileName = iconv ( 'UTF-8', 'gb2312', $fileName );

		//直接导出文件

		$objWriter->save ( $outputFileName );
		
		
		vendor('Form.File');
		
		$file=File::getInstance();
		
		$file::downloads($fileName);
		
	}
	
	public  function exportToExcel(){ 
	
        set_time_limit(0);
		
		$activity_id = empty($_REQUEST['activity_id'])?'0':$_REQUEST['activity_id'];
		
		$stuAll = M('acti_stus')->where('activity_id='.$activity_id)->select();
		//AAA($activity_id);
		$stuStr = '';
		
		foreach($stuAll as $key=>$val){
			
			if($key==0){
				
				$stuStr = 'a.student_id='.$val['student_id'];
			}else{
				
				$stuStr.=' OR a.student_id='.$val['student_id'];
			}
		}
		
		$stuStr = ' ( '.$stuStr.' )';
		
		$sql = 'SELECT a.datetime AS atime, b.* FROM `acti_stus` AS a LEFT JOIN student AS b ON a.`student_id` = b.`student_id` WHERE b.add_code!="" AND a.`activity_id` = '.$activity_id;
		
		$allRecord = M('student')->query($sql);
		
		
		if($activity_id==0){
			
			$allRecord = M('student')->where(true)->select();
		}
		
		foreach($allRecord as $key=>$val){
			
			$mtInfo = M('relation')->where('student_id='.$val['student_id'])->find();
			
			if($mtInfo){
				
				$allRecord[$key]['mt_num'] = $mtInfo['mt_num'];
			}else{
				
				//unset($allRecord[$key]);
			}
			
		}
		
		
		$dataExcel = array();
		
		foreach($allRecord as $Row){
			
			$dataRow = array();
			
			$dataRow[] = $Row['mt_num'];
			$dataRow[] = $Row['add_code'];
			$dataRow[] = $Row['name'];
			
			$value = $Row['sex'];
			$desc = $value=='1'?'男':'女';				
			$desc = $value=='0'?'未完善':$desc;
			
			$dataRow[] = $desc;
			$dataRow[] = $Row['mt_total'];
			$dataRow[] = $Row['mt_res'];
			$dataRow[] = "\t".date('Y-m-d',$Row['birth_date']);
			
			
			$str = '未完善';
			if($Row['volunteer']==1){
				
				$str = '国内部';
			}
			if($Row['volunteer']==2){
				
				$str = '国际部';
			}
			$dataRow[] = $str;
			
			
			$value = $Row['identity_type'];
			$str = '';
			if($value=='1'){ $str = '居民身份证'; }
			if($value=='2'){ $str = '香港居民身份证'; }
			if($value=='3'){ $str = '澳门居民身份证'; }
			if($value=='4'){ $str = '护照'; }
			$dataRow[] = $str;
			
			
			
			$dataRow[] = "\t".$Row['identity'];
			
			$value = $Row['is_in_foshan'];
			$studentInfo = M('student')->where('student_id='.$Row['student_id'])->find();
			$str = '';
			
			if($value=='1'){
				
				foreach($cats1 as $val){
					if($val['list_item_id']==$studentInfo['street']){ $str = $val['item_desc'];}
				}
			}else{
				$str = $studentInfo['street_desc'];
			}
			$dataRow[] = $str;
			
			
			$str = $Row['address_desc'].$Row['address'];
			$dataRow[] = $str;
			
			
			$str = '';
			if($Row['graduate_province']=='1'){
				
				foreach($cats2 as $val){
					if($val['list_item_id']==$Row['graduate_street']){$str = $val['item_desc'];}
				}
				   
				foreach($schs as $val){
					if($val['list_item_id']==$Row['graduate_school']){ $str = $val['item_desc'];}
				}
				$str .= $Row['graduate_class'].'班';
			}else{
				
				$str = $Row['graduate_desc'].$Row['graduate_school_desc'].$Row['graduate_class'].'班';;
			}
			$dataRow[] = $str;
			
			
			
			$dataRow[] = "\t".$Row['register_num'];
			
			
			
			$dataRow[] = $Row['father'];
			$dataRow[] = "\t".$Row['father_tell'];
			$dataRow[] = $Row['father_address'];
			$dataRow[] = $Row['father_job'];
			$dataRow[] = $Row['mother'];
			$dataRow[] = "\t".$Row['mother_tell'];
			$dataRow[] = $Row['mother_address'];
			$dataRow[] = $Row['mother_job'];
			$dataRow[] = "\t".$Row['add_phone'];
			$dataRow[] = $Row['student_desc'];
			
			$dataRow[] = "\t".date('Y-m-d H:i:s',$val['atime']);
			
			
			
			$dataRow[] = "\t".$Row['add_openid'];
		
			
			
			$dataExcel[] = $dataRow;
			
		}
		//AAA($dataExcel);
		$filename = 'abc.xlsx';
		$tileArray=  array('面谈凭证','报名号','姓名','性别','总分','录取结果','出生日期','报读志愿','证件类型','证件号码','户籍所在地',
							'现居住地址','毕业学校','全国学籍号','父亲姓名','父亲联系方式','父亲工作单位','父亲职位','母亲姓名','母亲联系方式','母亲工作单位','母亲职位',
							'验证手机号码','学生自述','报名时间','OpenId');
		$dataArray=  array(array('111','222'));
		$dataArray=  $dataExcel;
		
		ini_set('memory_limit','5120M');  
        ini_set('max_execution_time',0);  
        ob_end_clean();  
        ob_start();  
        header("Content-Type: text/csv");  
        header("Content-Disposition:filename=".$filename);  
        $fp=fopen('php://output','w');  
        fwrite($fp, chr(0xEF).chr(0xBB).chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))  
        fputcsv($fp,$tileArray);  
        $index = 0;  
        foreach ($dataArray as $item) {  
            if($index==100){  
                $index=0;  
                ob_flush();  
                flush();  
            }  
            $index++;  
            fputcsv($fp,$item);  
        }  
  
        ob_flush();  
        flush();  
        ob_end_clean();  
    }
	
	
	
	
	
	
	
	
	
	
	
}

?>