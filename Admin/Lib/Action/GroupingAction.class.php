<?php

class GroupingAction extends CommonAction {
	
	public $cmTableName = 'Grouping';
	
	public function _initialize(){
		
		parent::_initialize();
	}
	
    //列表
	public function Grouping_list(){
		
		Vendor('Form.TableList');
		
		$list	 =	  new TableList();		
		
		
		//获取搜索列表条件
		$where = $list->get_search_data($this->cmTableName);
		
		//根据条件获取列表
		$rs = $list->auto_gen_list($this->cmTableName,$where);
		
		//分配列表信息
		$this->assign('list',$rs['table_list']);
		//分配搜索表单信息
		$this->assign('searchForm',$rs['search_form']);
		
		
		
		//获取并分配添加按钮
		$add_btn = $list->gen_add_button($this->cmTableName);
		
		$this->assign('add_btn',$add_btn);
		
		
		//获取并分配Excel按钮
		$excel_btn = $list->get_excel_btn($this->cmTableName);
		
		$this->assign('excel_btn',$excel_btn);
		
		
		$this->display($this->cmTableName.'/list');
	}
	
	
	//添加
	public function Grouping_add(){
		
		//获取添加表单并且分配
		$this->assign('form',D($this->cmTableName)->form->_getForm());
		
		$this->display($this->cmTableName.'/add');
	}
	
	//处理添加的数据（入库）
	public function Grouping_addok(){
		
		//根据表名获取提交过来的数组
		$data=D($this->cmTableName)->form->recordToBuildData('add');
		
		$this->ADD_ONE($this->cmTableName,$data,U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']));
		
	}
	
	
	//编辑页面
	public function Grouping_edit(){
		
		//获取表格主建
		$pk	=	M($this->cmTableName)->getPk();
		
		$pkVal	=	$_REQUEST[$pk];
		
		//根据主键获取编辑的表单并分配
		$res=M($this->cmTableName)->where($pk.'='.$pkVal)->find();
		
		$this->assign('form',D($this->cmTableName)->form->setAction('__APP__/'.$this->cmTableName.'/'.$this->cmTableName.'_editok')->_getEditorForm($res));
		
		$this->display($this->cmTableName.'/edit');	
	}
	
	
	//处理编辑的数据（入库）
	public function Grouping_editok(){
		
		$pk=M($this->cmTableName)->getPk();
		
		//根据表名称获取编辑的数据数组并且入库
		$data=D($this->cmTableName)->form->recordToBuildData('edit');
		
		$this->EDIT_ONE($this->cmTableName,$data,U($this->cmTableName.'/'.$this->cmTableName.'_list',$data['foreignKey']));	
		
	}
	
	
	//根据主建删除一条记录
	public function Grouping_del(){
	
		$pk=M($this->cmTableName)->getPk();
		
		$this->DELETE_ONE($this->cmTableName,$_REQUEST[$pk],U($this->cmTableName.'/'.$this->cmTableName.'_list'));
	}
	
	
	//批量删除
	public function Grouping_batch_del(){
		
		$ids = I('ids'); 
		
		$idsArr = explode(',',$ids);
		
		//根据主键数组批量删除记录
		foreach($idsArr as $val){
		
			$this->CM(strtolower($this->cmTableName))->delete($val);
		}
		
		$this->alert_jump('删除成功',U($this->cmTableName.'/'.$this->cmTableName.'_list'));
	}
	
	
	//模型Excel
	public function Grouping_excel(){
		
		Vendor('Form.TableList');
		
		$list	 =	  new TableList();
		
		
		//获取Excel上传表单并且分配
		$excel_btn = $list->gen_excel_form($this->cmTableName);
		$this->assign('excel_btn',$excel_btn);
		
		//组将回删按钮并分配
		$back_del_btn = '<a href="'.U('Common/del_last_excel',array('table_name'=>$this->cmTableName)).'">回删记录</a>';
		$this->assign('back_del_btn',$back_del_btn);
		
		
		//根据表名获取上次Excel导入记录，拆分数组并且分配
		$last_excel = file_get_contents('./Public/reportData/'.ucfirst($this->cmTableName).'.txt');
		
		$last_excelArr = unserialize($last_excel);
		$this->assign('table_name',$last_excelArr['table_name']);
		$this->assign('last_time',$last_excelArr['last_time']);
		$this->assign('successRows',$last_excelArr['successRows']);
		$this->assign('errorEmptyRows',$last_excelArr['errorEmptyRows']);
		$this->assign('errorRepeatRows',$last_excelArr['errorRepeatRows']);
		$this->assign('errorInsertRows',$last_excelArr['errorInsertRows']);
		$this->assign('errorDataMapRows',$last_excelArr['errorDataMapRows']);
		
		$this->display($this->cmTableName.'/excel');
	}
	
	
	public function mess_index(){
		
		$groupInfo = M('grouping')->where(true)->select();
		
		foreach($groupInfo as $key=>$val){
			
			$count = M('student')->where('grouping_id='.$val['grouping_id'].' AND add_type=1')->count();
			
			$groupInfo[$key]['nums'] =  $count;
		}
		
		$zcount = M('student')->where('grouping_id=0 AND add_type=1')->count();
		
		$this->assign('zcount',$zcount);
		$this->assign('groupList',$groupInfo);
		
		$actInfo = M('activity')->where('status!=3')->find();
		$this->assign('actInfo',$actInfo);
		
		$this->display();
	}
	
	Public function multiMess(){
		
		$groups   = empty($_REQUEST['groups'])?array():$_REQUEST['groups'];
		$temps    = empty($_REQUEST['temps'])?0:$_REQUEST['temps'];
		
		$allStu = array();
		
		foreach($groups as $val){
			
			
			$stuList = array();
			$stuList = M('student')->where('grouping_id='.$val)->select();
			
			foreach($stuList as $row){
				
				$allStu[] = $row;
			}
			
		}
		
		//AAA($allStu);
		
		
		$actInfo = M('activity')->where('status!=1')->find();
		//AAA($temps);
		
		include('./aliyun-dysms-php-sdk/api_demo/SmsDemo.php');
		
		
		$totalSuccess = 0;
		
		foreach($allStu as $row){
			
			
			if($temps==1){
				
				$dataMess =  array();
				$dataMess['add_code'] 		= getFullNum($row['student_id']);
				$dataMess['name'] 			= $row['name'];
				$dataMess['actYear'] 		= $actInfo['year'];
				$dataMess['actSeason'] 		= $actInfo['time_type']==1? '春':'秋';
				$dataMess['actDate'] 		= $actInfo['act_time'];
				$dataMess['classTime1'] 	= $actInfo['act_time1'];
				$dataMess['classTime2'] 	= $actInfo['act_time2'];
				$dataMess['classTime3'] 	= $actInfo['act_time3'];
				$dataMess['classTime4'] 	= $actInfo['act_time4'];
				//AAA($dataMess);
				//$response = SmsDemo::sendSms($row['add_phone'],'SMS_151990733',$dataMess);//AAA($response);
				//$response = SmsDemo::sendSms($row['add_phone'],'SMS_152855242',$dataMess);//AAA($response);
				$response = SmsDemo::sendSms($row['add_phone'],'SMS_160855146',$dataMess);//AAA($response);
			}
			
			//print_r($dataMess);   act_time2
			if($temps==2){
				
				$dataMess =  array();
				$dataMess['add_code'] 		= getFullNum($row['student_id']);
				$dataMess['name'] 			= $row['name'];
				$dataMess['year'] 			= $actInfo['year'];
				
				$time_type 		= $actInfo['time_type']==1? '春季':'秋季';
				$volunteer 		= '';
				$add_class      = '';
				
				if($row['volunteer']==1){
					
					$volunteer = '国语班';
				}
				
				if($row['volunteer']==2){
					
					$volunteer = '国际班';
				}
				
				if($row['volunteer']==3){
					
					$volunteer = '国语班/国际班';
				}
				
				if($row['add_class']==1){
					
					$add_class = '小小班';
				}
				
				if($row['add_class']==2){	
					
					$add_class = '小班';
				}
				
				if($row['add_class']==3){		
					
					$add_class = '中班';
				}
				
				if($row['add_class']==4){		
					
					$add_class = '大班';
				}
				
				$dataMess['info_desc'] 			= $time_type.$volunteer.$add_class;
				$dataMess['date'] 				= date('Y/m/d',$actInfo['bu_time']);
				
				//$response = SmsDemo::sendSms($row['add_phone'],'SMS_152289935',$dataMess);
				$response = SmsDemo::sendSms($row['add_phone'],'SMS_153881055',$dataMess);
			}
			
				
			if($response->Code=='OK'){
				
				echo $row['name'].' - <font color="green">发送成功</font>&nbsp;&nbsp;&nbsp;&nbsp;';
			}else{
				
				echo $row['name'].' - <font color="red">发送失败</font>&nbsp;&nbsp;&nbsp;&nbsp;';
				print_r($response);
			}
		}
		exit;
		
	}
	
	
	public function addMuMes(){
		
		include ('/aliyun-dysms-php-sdk/api_demo/SmsDemo.php');
		
		$code   =   'SMS_130925643';
		
		$phoneArr   =   array();
		$phoneArr[] = '13425663554';
		$phoneArr[] = '13702487989';
		
		$signArr = array();
		$signArr[] = '德胜学校';
		$signArr[] = '德胜学校';
		
		$mobanArr = array();
		$mobanArr[] =   array(
                "activity"=>"报名活动1",
				"time"=>"2018-10-30",
				"location"=>"8号楼"
        );
		$mobanArr[] =   array(
                "activity"=>"报名活动2",
				"time"=>"2018-10-30",
				"location"=>"8号楼"
        );
		
		$response = SmsDemo::sendBatchSms( $code,$phoneArr,$signArr,$mobanArr );
	}
	
	
	
	
	
	
	
	
}

?>