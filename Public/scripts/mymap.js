function myMap(mapId,searchInputId,resultPanel,lng,lat){
		
	var _This=this;
	this.initLng           =  lng;
	this.initLat           =  lat;
	this.initMapId         =  mapId;
	this.initSearchInputId =  searchInputId;
	this.searchResultPanel =  resultPanel;
	
	
	this.init = function(){	
		
		function showInfo(e){
			//alert(e.point.lng + "," + e.point.lat);
			
			
			var marker = new BMap.Marker(new BMap.Point(e.point.lng, e.point.lat)); // 创建点
			//alert($('#'+_This.searchResultPanel).length);
			$('#'+_This.searchResultPanel).val(e.point.lng + "," + e.point.lat);
			//document.getElementById(_This.searchResultPanel).innerText =e.point.lng + "," + e.point.lat;
			map.clearOverlays();
			map.addOverlay(marker);    //增加点
		}
		
		
		var map = new BMap.Map(_This.initMapId);
		map.addEventListener("click", showInfo);
		
		
		//map.enableScrollWheelZoom();
		var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
		map.addControl(ctrl_nav);
		
		//设置默认中心点
		if(_This.initLng && _This.initLat){ 
			var center_point = new BMap.Point(_This.initLng, _This.initLat);
			
			var marker = new BMap.Marker(new BMap.Point(_This.initLng, _This.initLat)); // 创建点
			map.centerAndZoom(center_point,18);
			map.addOverlay(marker);
			//map.centerAndZoom("北京",12);  				
		}else{
			map.centerAndZoom("北京",12);                   // 初始化地图,设置城市和地图级别。
		}
		

		var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
			{"input" : _This.initSearchInputId
			,"location" : map
		});
		
		
		
		function setPlace(){
			map.clearOverlays();    //清除地图上所有覆盖物
			function myFun(){
				var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
				map.centerAndZoom(pp, 18);
				map.addOverlay(new BMap.Marker(pp));    //添加标注
				$('#'+_This.searchResultPanel).val(pp.lng + "," + pp.lat);
				//document.getElementById(_This.searchResultPanel).innerText =pp.lng + "," + pp.lat;
			}
			
			var local = new BMap.LocalSearch(map, { //智能搜索
			  onSearchComplete: myFun
			});
			local.search(myValue);
		}
		
		
		var myValue;
		ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
		var _value = e.item.value;
			myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
			//document.getElementById('searchResultPanel').innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;
			
			setPlace();
		});
		
		
		
	}
	
	
	
	_This.init();
}