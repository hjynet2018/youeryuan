// 设置卡片图片长宽比
function setCardImgScale(obj) {
    var scale = 235 / 380;
    var cardWidth = obj.find('.img').outerWidth();
    obj.find('.img').css('height', cardWidth * scale + 'px');
}
