if(window.location.host == 'yry.fsbohan.com') {
    parameter = {
        // 验证码发送
        messageCodeSend: 'http://yry.fsbohan.com/index.php/Student/sendMsg',
        // 输入验证码验证
        messageCodeGet: 'http://yry.fsbohan.com/index.php/Student/checkMsg',
        // 提交报名信息表单
        userInfoSend: 'http://yry.fsbohan.com/index.php/Student/addBaoMing',
        //获取入学年限
        getSchoolYear: 'http://yry.fsbohan.com/index.php/Student/getMenu/list_id/8',
        // 获取区内学校列表
        getSchoolList: 'http://118.178.234.34:30003/student/getList?item_id=6',
        // 获取区内镇街列表
        getStreetList: 'http://118.178.234.34:30003/student/getList?item_id=5',
        // 获取市内户籍列表
        getHometownList: 'http://118.178.234.34:30003/student/getList?item_id=4',
        // 获取表单信息 表单回填 参数：add_phone
        userInfoGet: 'http://yry.fsbohan.com/index.php/Student/getInfo',
        // 获取活动列表
        getActivityList: 'http://118.178.234.34:30003/student/getAvticyList',
        // 获取活动详情
        getActivityInfo: 'http://118.178.234.34:30003/student/getAvticyInfo',
        // 活动报名
        signActivity: 'http://118.178.234.34:30003/student/addSignAvticy',
        // 身份证查重
        getIdentityExist: 'http://yry.fsbohan.com/index.php/Student/identity',
        // 打印
        printBaoming: 'http://t.ihecha.me/index.php/Student/dayin/student_id/',
        // 查看面谈凭证
        pingzheng: 'http://t.ihecha.me/index.php/Student/mt2/student_id/',

        qrURL: 'http://t.ihecha.me',  //'http://bm.desheng-school.cn';
        appid: 'wxb52637c9bfcfd0c8',  //'wx2db696f196d4f4e8';
    }
} else {
    parameter = {
        // 验证码发送
        messageCodeSend: 'http://kids.desheng-school.cn/index.php/Student/sendMsg',
        // 输入验证码验证
        messageCodeGet: 'http://kids.desheng-school.cn/index.php/Student/checkMsg',
        // 提交报名信息表单
        userInfoSend: 'http://kids.desheng-school.cn/index.php/Student/addBaoMing',
        //获取入学年限
        getSchoolYear: 'http://kids.desheng-school.cn/index.php/Student/getMenu/list_id/8',
        // 获取区内学校列表
        getSchoolList: 'http://118.178.234.34:30003/student/getList?item_id=6',
        // 获取区内镇街列表
        getStreetList: 'http://118.178.234.34:30003/student/getList?item_id=5',
        // 获取市内户籍列表
        getHometownList: 'http://118.178.234.34:30003/student/getList?item_id=4',
        // 获取表单信息 表单回填 参数：add_phone
        userInfoGet: 'http://kids.desheng-school.cn/index.php/Student/getInfo',
        // 获取活动列表
        getActivityList: 'http://118.178.234.34:30003/student/getAvticyList',
        // 获取活动详情
        getActivityInfo: 'http://118.178.234.34:30003/student/getAvticyInfo',
        // 活动报名
        signActivity: 'http://118.178.234.34:30003/student/addSignAvticy',
        // 身份证查重
        getIdentityExist: 'http://kids.desheng-school.cn/index.php/Student/identity',
        // 打印
        printBaoming: 'http://t.ihecha.me/index.php/Student/dayin/student_id/',
        // 查看面谈凭证
        pingzheng: 'http://t.ihecha.me/index.php/Student/mt2/student_id/',

        qrURL: 'http://t.ihecha.me',  //'http://bm.desheng-school.cn';
        appid: 'wxb52637c9bfcfd0c8',  //'wx2db696f196d4f4e8';
    }
}
