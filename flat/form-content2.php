<style media="screen">
    .fixed-block {
        background: #fff;
        max-width: 640px;
    }
    .form-top {
        padding-bottom: 15px;
    }
    .form-top h4 {
        font-size: 20px;
        margin-bottom: 5px;
    }
    .notice {
        border: 1px solid #0036AD;
        margin-bottom: 20px;
    }
    .step-hud {
        padding: 10px 0;
    }
    .step-hud .progress {
        width: 85%;
        margin: 0 auto;
    }
    .form-block {
        padding-bottom: 15px;
    }
    .form-block .form-title{
        border-left: 4px solid #3498DB;
        padding-left: 5px;
        margin: 10px 0px 5px 0;
        line-height: 23px;
        font-size: 19px;
    }
    .form-block .form-sub-title {
        margin-bottom: 5px;
        font-size: 14px;
    }
    .form-block .form-sub-title span {
        line-height: 23px;
    }
    .date-birthday .form-control, #home-address1, #school3 {
        border-color: #bdc3c7;
        color: #34495e;
        opacity: 1;
    }
    .form-group {
        margin-bottom: 10px;
    }

    .select.form-control, .select.select2-search input[type=text] {
        border: 2px solid #bdc3c7;
    }

    .form-group select {
        border: 2px solid #bdc3c7;
        height: 35px!important;
        min-width: unset;
    }
    .form-group .input-group-addon {
        padding: 5px;
        background: none;
        color: #666;
    }
    .border-red {
        border-color: red!important;
        background-color: #ffcdcd!important;
    }
</style>

<div>

    <div class="notice row fixed-block hide">
        <div class="col-xs-12"><div class="text-center"><small>请确认您的信息正确无误并参加活动。</small></div></div>
    </div>

    <div class="form-top row fixed-block">
        <div class="col-xs-12"><h4 class="text-center">2018-2019学年初一年级自主招生报名登记表</h4></div>
        <div class="col-xs-12"><div class="text-center"><small class="text-danger">(注：红色字体部分为必填项目)</small></div></div>
    </div>
    <hr class="fixed-block">

    <div class="step-hud fixed-block hide">
        <div class="step-info text-center">
            第1步（共4步）
        </div>
        <div class="progress mbn">
            <div class="progress-bar" style="width: 25%;"></div>
        </div>
    </div>

    <div class="step step1 form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">报名人个人信息</h6></div>

        <div class="col-xs-12 col-sm-4">
            <p class="text-danger form-sub-title">姓名<span class="fui-alert-circle hide"></span></p>
            <div class="form-group">
                <input id="user-name" type="text" value="" placeholder="" class="form-control input-sm">
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <p class="text-danger form-sub-title">性别</p>
            <div class="form-group">
                <select id="user-sex" class="form-control select select-default select-block">
                <option value="1">男</option>
                <option value="2">女</option>
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <p class="text-danger form-sub-title">出生日期</p>
            <div class="form-group">
                <div class="date-birthday input-group date" data-date="" data-date-format="dd MM yyyy" data-link-field="user-birthday" data-link-format="yyyy-mm-dd">
                    <input class="form-control input-sm" size="16" type="text" value="" readonly>
                    <span class="input-group-addon input-sm"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <input type="hidden" id="user-birthday" value="" />
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <p class="text-danger form-sub-title">健康状况</p>
            <div class="form-group">
                <input id="user-health" type="text" value="" placeholder="" class="form-control input-sm">
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <p class="text-danger form-sub-title">身份证号码</p>
            <div class="form-group">
                <input id="user-id" type="text" value="" placeholder="" class="form-control input-sm">
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <p class="text-danger form-sub-title">户籍所在地（以户口薄为准）</p>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <select id="hometown-in-foshan" class="form-control select select-default select-block">
                        <option value="1">佛山市内</option>
                        <option value="2">佛山市外</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <select id="hometown1" class="form-control select select-default select-block">
                            <option value="1">顺德大良</option>
                            <option value="2">顺德容桂</option>
                            <option value="3">顺德伦教</option>
                            <option value="4">顺德勒流</option>
                            <option value="5">顺德北滘</option>
                            <option value="6">顺德龙江</option>
                            <option value="7">顺德乐从</option>
                            <option value="8">顺德陈村</option>
                            <option value="9">顺德杏坛</option>
                            <option value="10">顺德均安</option>
                            <option value="11">佛山南海</option>
                            <option value="12">佛山禅城</option>
                            <option value="13">佛山高明</option>
                            <option value="14">佛山三水</option>
                        </select>
                    </div>
                    <div class="form-group" style="display: none;">
                        <input id="hometown2" type="text" value="" placeholder="请输入详细住址" class="form-control input-sm">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <p class="text-danger form-sub-title">现居住地址（尽量详细）</p>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <input id="home-address1" type="text" value="" placeholder="请选择所在地区" class="form-control input-sm">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <input id="home-address2" type="text" value="" placeholder="请输入详细地址" class="form-control input-sm">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <hr class="step step1 fixed-block hide">

    <div class="step step2 form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">报名人学校信息</h6></div>
        <div class="col-xs-12">
            <p class="text-danger form-sub-title">毕业学校</p>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <select id="school1" class="form-control select select-default select-block">
                        <option value="1">顺德区内</option>
                        <option value="2">顺德区外</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <div>
                            <select id="school2" class="form-control select select-default select-block">
                                <option value="1">顺德大良</option>
                                <option value="2">顺德容桂</option>
                                <option value="3">顺德伦教</option>
                                <option value="4">顺德勒流</option>
                                <option value="5">顺德北滘</option>
                                <option value="6">顺德龙江</option>
                                <option value="7">顺德乐从</option>
                                <option value="8">顺德陈村</option>
                                <option value="9">顺德杏坛</option>
                                <option value="10">顺德均安</option>
                            </select>
                        </div>
                        <!-- 地区选择器picker -->
                        <div style="display: none;">
                            <input id="school3" type="text" value="" placeholder="请选择所在地区" class="form-control input-sm">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <div>
                            <select id="school5" class="form-control select select-default select-block">
                                <option value="">请选择学校</option>
                            </select>
                        </div>
                        <div style="display: none;">
                            <input id="school4" type="text" value="" placeholder="请输入学校全名" class="form-control input-sm">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">六年</span>
                                <input id="school6" type="tel" class="form-control input-sm" placeholder="" />
                                <span class="input-group-addon input-sm">班</span>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            <p class="text-danger form-sub-title">全国学籍号</p>
            <div class="form-group">
                <input id="school7" type="text" value="" placeholder="" class="form-control input-sm">
            </div>
        </div>
    </div>
    <hr class="step step2 fixed-block hide">

    <div class="step step3 form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">报名人家庭信息</h6></div>
        <div class="col-xs-12">
            <p class="text-danger form-sub-title">父亲/监护人</p>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 姓 &nbsp; 名 &nbsp; </span>
                            <input id="father-name" type="text" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm">联系电话</span>
                            <input id="father-tel" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm">工作单位</span>
                            <input id="father-job" type="text" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <p class="form-sub-title">母亲/监护人</p>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 姓 &nbsp; 名 &nbsp; </span>
                            <input id="mother-name" type="text" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm">联系电话</span>
                            <input id="mother-tel" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm">工作单位</span>
                            <input id="mother-job" type="text" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="step step3 fixed-block hide">


    <div class="score step step4 form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">学生成绩</h6></div>
        <div class="col-xs-12">
            <p class="form-sub-title">五年级上学期成绩</p>
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 语 &nbsp; 文 &nbsp; </span>
                            <input id="five1-yuwen" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 数 &nbsp; 学 &nbsp; </span>
                            <input id="five1-math" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 英 &nbsp; 语 &nbsp; </span>
                            <input id="five1-english" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm">总分排名</span>
                            <input id="five1-rank" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <p class="form-sub-title">五年级下学期成绩</p>
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 语 &nbsp; 文 &nbsp; </span>
                            <input id="five2-yuwen" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 数 &nbsp; 学 &nbsp; </span>
                            <input id="five2-math" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 英 &nbsp; 语 &nbsp; </span>
                            <input id="five2-english" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm">总分排名</span>
                            <input id="five2-rank" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <p class="form-sub-title">六年级上学期成绩</p>
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 语 &nbsp; 文 &nbsp; </span>
                            <input id="six1-yuwen" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 数 &nbsp; 学 &nbsp; </span>
                            <input id="six1-math" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm"> &nbsp; 英 &nbsp; 语 &nbsp; </span>
                            <input id="six1-english" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon input-sm">总分排名</span>
                            <input id="six1-rank" type="tel" class="form-control input-sm" placeholder="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="score step step4 fixed-block hide">

    <!-- <div class="step step4 form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">您对学校的办学理念</h6></div>
        <div class="col-xs-12">
            <div class="form-group">
                <select id="school-idea" class="form-control select select-default select-block">
                <option value="1">非常认同</option>
                <option value="2">认同</option>
                <option value="3">不认同</option>
                </select>
            </div>
        </div>
    </div>
    <hr class="step step4 fixed-block hide">

    <div class="step step4 form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">报读志愿</h6></div>
        <div class="col-xs-12 col-sm-6">
            <p class="text-danger form-sub-title">第一志愿</p>
            <div class="form-group">
                <select id="choice1" class="form-control select select-default select-block">
                    <option value="">选择第一志愿</option>
                    <option value="1">国内部</option>
                    <option value="2">国外部</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p class="form-sub-title">第二志愿</p>
            <div class="form-group">
                <select id="choice2" class="form-control select select-default select-block">
                    <option value="">选择第二志愿</option>
                    <option value="1">国内部</option>
                    <option value="2">国外部</option>
                </select>
            </div>
        </div>
    </div>
    <hr class="step step4 fixed-block hide"> -->


    <div class="award step step4 form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">近3年所获荣誉</h6></div>
        <div class="col-xs-12">
            <p class="form-sub-title">学科类</p>
            <textarea id="award-subject" class="form-control" rows="3"></textarea>
            <br>
            <p class="form-sub-title">才艺类</p>
            <textarea id="award-talent" class="form-control" rows="3"></textarea>
            <br>
            <p class="form-sub-title">其他</p>
            <textarea id="award-other" class="form-control" rows="3"></textarea>
        </div>
    </div>
    <hr class="award step step4 fixed-block hide">


    <div class="step step4 form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">学生自述（非必选）</h6></div>
        <div class="col-xs-12">
            <textarea id="introduce" class="form-control" rows="3"></textarea>
        </div>
    </div>
    <hr class="step step4 fixed-block hide">

    <div class="qrcode form-block row fixed-block hide">
        <div class="col-xs-12"><h6 class="form-title">微信扫码绑定账号</h6></div>
        <div class="col-xs-12">
            <div class="content text-center">

            </div>
        </div>
    </div>
    <hr class="qrcode fixed-block hide">

    <div class="form-block row fixed-block text-center">
        <small>本人保证已如实填写报名信息，如有虚假或造假情况，自愿承担所有后果。</small>
    </div>

    <div class="form-block row fixed-block" style="padding-bottom: 100px;">
        <div class="col-xs-12 text-center">
            <button id="prev-step" class="btn btn-sm btn-primary hide">
              上一步
            </button>
            <button id="next-step" class="btn btn-sm btn-primary hide">
              下一步
            </button>
            <button id="submit" class="btn btn-sm btn-primary hide">
              提交
            </button>
            <div id="submit-loading" class="weui-loadmore hide">
                <i class="weui-loading"></i>
                <span class="weui-loadmore__tips">正在提交</span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        var pass = true;
        var studentId = null;
        // 是否正式报名
        var isSignUp = false;

        // 字段对应表单id
        var mainInfo = {
            'name': '#user-name',
            'sex': '#user-sex',
            'birth_date': '#user-birthday',
            'healthy': '#user-health',
            'identity': '#user-id',
            // 籍贯是否在佛山
            'is_in_foshan': '#hometown-in-foshan',
            // 籍贯在佛山
            'street': '#hometown1',
            // 籍贯不在佛山
            'street_desc': '#hometown2',
            // 现居地所在省市区
            'address_desc': '#home-address1',
            // 现居地详细地址
            'address': '#home-address2',
            // 毕业学校是否在顺德内
            'graduate_province': '#school1',
            // 毕业学校在顺德内 镇
            'graduate_street': '#school2',
            // 毕业学校不顺德内 地区
            'graduate_desc': '#school3',
            // 毕业学校在顺德内 学校名
            'graduate_school': '#school5',
            // 毕业学校不顺德内 学校名
            'graduate_school_desc': '#school4',
            // 班级
            'graduate_class': '#school6',
            // 学籍号
            'register_num': '#school7',

            'father': '#father-name',
            'father_tell': '#father-tel',
            'father_address': '#father-job',

            'mother': '#mother-name',
            'mother_tell': '#mother-tel',
            'mother_address': '#mother-job',


            'five1_yuwen': '#five1-yuwen',
            'five1_math': '#five1-math',
            'five1_english': '#five1-english',
            'five1_rank': '#five1-rank',

            'five2_yuwen': '#five2-yuwen',
            'five2_math': '#five2-math',
            'five2_english': '#five2-english',
            'five2_rank': '#five2-rank',

            'six1_yuwen': '#six1-yuwen',
            'six1_math': '#six1-math',
            'six1_english': '#six1-english',
            'six1_rank': '#six1-rank',

            'award_subject': '#award-subject',
            'award_talent': '#award-talent',
            'award_other': '#award-other',
            // 'school_idea': $('#school-idea').val(),

            // 'first_choice': $('#choice1').val(),
            // 'second_choice': $('#choice2').val(),

            'student_desc': '#introduce',
        };

        // 生日选择器
        $('.date-birthday').datetimepicker({
            language:  'zh-CN',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn:  0,
    		autoclose: 1,
    		todayHighlight: 1,
    		startView: 4,
    		minView: 2,
    		forceParse: 0
        });

        // 户籍选择器
        function hometownPicker() {
            $("#home-address1").cityPicker({
                title: "请选择详细地址"
            });
        }
        // hometownPicker();

        // 学校所在地址选择器
        function schoolAddressPicker() {
            $("#school3").cityPicker({
                title: "请选择所在地址"
            });
        }
        // schoolAddressPicker();

        // 户籍是否在佛山切换
        function hometownInFoshanSwitch() {
            switch ($('#hometown-in-foshan').val()) {
                case '1':
                    $('#hometown1').parent('.form-group').show();
                    $('#hometown2').parent('.form-group').hide();
                    break;
                case '2':
                    $('#hometown1').parent('.form-group').hide();
                    $('#hometown2').parent('.form-group').show();
                    break;
            }
        }
        $('#hometown-in-foshan').on('change', function() {
            hometownInFoshanSwitch();
        });

        // 学校地址是否在顺德区内的切换
        function schoolInShundeSwitch() {
            switch ($('#school1').val()) {
                case '1':
                    $('#school2').parent('div').show();
                    $('#school5').parent('div').show();
                    $('#school3').parent('div').hide();
                    $('#school4').parent('div').hide();
                    break;
                case '2':
                    $('#school2').parent('div').hide();
                    $('#school5').parent('div').hide();
                    $('#school3').parent('div').show();
                    $('#school4').parent('div').show();
                    break;
            }
        }
        $('#school1').on('change', function() {
            schoolInShundeSwitch();
        });

        // 判断是不是从活动入口进来
        if (getHerfParameter('activity') != null) {
            $('.notice').removeClass('hide');
        }

        // 获取区内学校列表
        $.get(parameter.getSchoolList, function(data) {
            var json = JSON.parse(data);
            var text = '';
            for(var i in json) {
                text += '<option value="' + json[i].list_item_id + '">' + json[i].item_desc + '</option>';
            };
            $('#school5').append(text);
        });

        // 表单回填
        $.post(parameter.userInfoGet, {add_phone: getCookie('user')}, function(data) {
            var json = JSON.parse(data);
            studentId = json.student_id;

            if (json.name == '') {
                // console.log('第一次填写');
                hometownPicker();
                schoolAddressPicker();
                hometownInFoshanSwitch();
                schoolInShundeSwitch();
            } else {
                for (var i in mainInfo) {
                    if (i == 'birth_date') {
                        // 生日
                        var birthday = (new Date(json.birth_date * 1000)).format('yyyy-MM-dd');
                        // console.log(birthday);
                        $('#user-birthday').val(birthday);
                        $('.date-birthday .form-control').val(birthday);
                        $('.date-birthday').datetimepicker('update');
                    } else {
                        $(mainInfo[i]).val(json[i]);
                    }
                }
                hometownPicker();
                schoolAddressPicker();
                hometownInFoshanSwitch();
                schoolInShundeSwitch();
            }
        });


        // 验证
        function check(obj, target) {
            if ($(obj).val() == '') {
                if (target) {
                    $(target).addClass('border-red');
                } else {
                    $(obj).addClass('border-red');
                }
                pass = false;
            } else {
                if (target) {
                    $(target).removeClass('border-red');
                } else {
                    $(obj).removeClass('border-red');
                }
            }
        }

        function check1() {
            // 第一步
            check('#user-name');
            check('#user-sex');
            check('#user-birthday', '.date-birthday .form-control');
            check('#user-health');
            check('#user-id');
            check('#hometown-in-foshan');
            if ($('#hometown-in-foshan').val() == 1) {
                check('#hometown1');
            } else {
                check('#hometown2');
            }
            check('#home-address1');
            check('#home-address2');
        }
        function check2() {
            // 第二部
            check('#school1');
            if ($('#school1').val() == 1) {
                check('#school2');
                check('#school5');
            } else {
                check('#school3');
                check('#school4');
            }
            check('#school6');
            check('#school7');
        }
        function check3() {
            // 第三部
            check('#father-name');
            check('#father-tel');
            check('#father-job');
            // check('#mother-name');
            // check('#mother-tel');
            // check('#mother-job');
        }
        function check4() {
            // 第四部
            // check('#school-idea');
            // check('#choice1');
        }




        // 提价按钮
        $('#submit').on('click', function() {
            pass = true;
            // 信息字段
            var info = {
                'student_id': studentId,
                'add_phone': getCookie('user'),
            }
            for (var i in mainInfo) {
                if (i == 'birth_date') {
                    info[i] = parseInt(Date.parse($('#user-birthday').val()) / 1000).toString();
                } else {
                    info[i] = $(mainInfo[i]).val();
                }
            }
            // console.log(info);

            check1();
            check2();
            check3();
            check4();
            if (pass == false) {
                alert('请正确填写红框内容');
                return;
            }

            // 提交表单
            $('#submit').addClass('disabled');
            $('#submit-loading').removeClass('hide');

            $.post(parameter.userInfoSend, info, function(data) {
                $('#submit').removeClass('disabled');
                $('#submit-loading').addClass('hide');
                var json = JSON.parse(data);
                switch (json.status) {
                    case 1:
                        var actId = getHerfParameter('actid');
                        if (actId == null) {
                            $.toast("提交成功", function() {
                                // window.location.href = './activity';
                            });
                        } else {
                            // 报名活动
                            $.post(parameter.signActivity, {activity_id: actId, 'student_id': studentId}, function(data) {
                                $.toast('报名成功', function() {
                                    // window.location.href = './activity';
                                });
                            });
                        }
                        break;
                    case -1:
                        $.toast("请扫码二维码", 'forbidden', function() {
                            console.log("失败");
                        });
                        break;
                }
            });

        });

        // 手机版 进度模式显示

        // 改变步骤
        function changeStep(stepIndex) {
            $('.step-hud .step-info').html('第' + stepIndex + '步(共4步)');
            $('.step-hud .progress-bar').css('width', stepIndex / 4 * 100 + '%');
            switch (stepIndex) {
                case 1:
                    $('.step, #prev-step, #next-step, #submit').addClass('hide');
                    $('.step' + stepIndex + ', #next-step').removeClass('hide');
                    break;
                case 4:
                    $('.step, #prev-step, #next-step, #submit').addClass('hide');
                    $('.step' + stepIndex + ', #prev-step, #submit').removeClass('hide');
                    break;
                default:
                    $('.step, #prev-step, #next-step, #submit').addClass('hide');
                    $('.step' + stepIndex + ', #prev-step, #next-step').removeClass('hide');
                    break;
            }
        }


        if (isMobile()) {
            var stepIndex = 1;
            $('.step-hud').removeClass('hide');
            changeStep(stepIndex);
            $('#prev-step').on('click', function() {
                stepIndex --;
                changeStep(stepIndex);
            });
            $('#next-step').on('click', function() {
                pass = true;
                switch (stepIndex) {
                    case 1:
                        check1();
                        break;
                    case 2:
                        check2();
                        break;
                    case 3:
                        check3();
                        break;
                    case 4:
                        check4();
                        break;
                }
                if (pass == false) {
                    alert('请正确填写红框内容');
                    return;
                }
                stepIndex ++;
                changeStep(stepIndex);
            });
        } else {
            // 二维码
            $('.qrcode').removeClass('hide');
            setQr('.qrcode .content');

            $('.step, #submit').removeClass('hide');
        }

        function toggleSignUpForm() {
            // 是否正式报名
            if (isSignUp) {
                $('.score, .award').remove();
            } else {
                if (isMobile() == false) {
                    $('.score, .award').removeClass('hide');

                }
            }
        }
        toggleSignUpForm();

    });
</script>
