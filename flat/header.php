<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>招生</title>

    <!-- Loading Bootstrap -->
    <link href="./css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI -->
    <link href="./css/flat-ui.min.css" rel="stylesheet">

    <!-- jquery weui -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/weui/1.1.3/style/weui.min.css">
    <link rel="stylesheet" href="https://cdn.staticfile.org/jquery-weui/1.2.1/css/jquery-weui.min.css">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

    <link href="./css/public.css?v=11" rel="stylesheet">
    <link href="./css/main.css" rel="stylesheet">

    <style media="screen">
        body {
            background: url(./img/bg1.jpg);
            background-size: cover;
            background-attachment: fixed;
        }
    </style>

    <!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
    <script src="./js/vendor/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./js/vendor/video.js"></script>
    <script src="./js/flat-ui.min.js"></script>
    <script src="https://cdn.staticfile.org/jquery-weui/1.2.1/js/jquery-weui.min.js"></script>
    <script src="https://cdn.staticfile.org/jquery-weui/1.2.1/js/city-picker.min.js"></script>
    <script type="text/javascript" src="http://validform.rjboy.cn/Validform/v5.3.2/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/jquery.qrcode.min.js" charset="UTF-8"></script>
    <script src="./js/setting.js"></script>
    <script src="./js/public.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


  </head>
