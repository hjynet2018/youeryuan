<?php include('./header.php') ?>

<?php 

?>
<script>
window.onload = function(){
	
	alert('document.cookie');
	alert(document.cookie)
}
</script>
<style media="screen">
    .login-head {
        text-align: center;
        max-width: 600px;
        margin: 0 auto;
        margin-top: 20px;
        margin-top: 5vh;
    }
    .login-head img {
        width: 100%;
    }
    .login-head h1 {
        font-size: 28px;
        font-weight: normal;
        line-height: 37px;
    }
    .login-container {
        max-width: 400px;
        margin: 0 auto;
        margin-top: 20px;
        margin-top: 5vh;
        padding: 10px;
        background: #fff;
        box-shadow: 0 0 10px #ddd;
        border-radius: 3px;
    }
    .login-container .title {
        text-align: center;
        margin-bottom: -10px;
    }
    .login-container hr {}
    .login-container #get-code-btn {
        width: 100%;
        min-width: unset;
    }
    .login-container .btn-area{
        margin: 0 5px 30px 5px;
    }
    .login-container .btn-area button {
        width: 100%;
    }
</style>

<body>
<div class="container-fluid">
    <div class="login-head">
        <img src="./img/login-logo.png" alt="">
        <h1>广东顺德德胜学校<br>2017-2018学年初一年级自主招生管理系统</h1>
    </div>
    <div class="login-container yes-login">
        <p class="title">报名系统登录</p>
        <hr>
        <div class="row" style="margin: 0;">
            <div class="col-xs-12 mbl pls prs">
                <input id="phone" type="tel" value="" placeholder="输入手机号码" class="form-control">
            </div>
            <div class="col-xs-7 pls prs">
                <input id="code" type="tel" value="" placeholder="输入手机验证码" class="form-control">
            </div>
            <div class="col-xs-5 pls prs">
                <button id="get-code-btn" class="btn btn-primary">获取验证码</button>
            </div>

        </div>
        <div class="btn-area">
            <button id="login-btn" class="btn btn-wide btn-primary mtl">登录</button>
        </div>
    </div>
    <div class="login-container no-login hide">
        <br>
        <p class="title">请使用微信登录报名系统</p>
        <br>
    </div>
</div>
</body>

<?php include('./footer.php') ?>
<script type="text/javascript">
// 手机版
if (isMobile() == true) {
    if (isWeixin() == true) {
        
    } else {
        // 非微信
        $('.yes-login').addClass('hide');
        $('.no-login').removeClass('hide');
    }

}


// 检查手机号码
function checkPhone() {
    if ($('#phone').val() == '' ) {
        $.toast("请输入手机号码获取验证码", "forbidden");
        return false;
    }

    if ($('#phone').val().length != 11) {
        $.toast("请输入正确手机号", "forbidden");
        return false;
    }

    return true;
}

$('#get-code-btn').on('click', function() {
    if(checkPhone() == false) {
        return;
    }
    $.toast('验证码已发送');
    $(this).attr('disabled', 'disabled');
    var num = 59;

    // alert($('#phone').val());
    $.get(
        parameter.messageCodeSend,
        {phone: $('#phone').val()},
        function() {
            // 1分钟内不能重复发送
            var int = setInterval(function () {
                var text = num >= 10 ? '已发送(' + num + ')' : '已发送(0' + num + ')';
                num --;
                if (num < 0) {
                    clearInterval(int);
                    $('#get-code-btn').html('获取验证码').removeAttr('disabled');
                    return;
                }
                $('#get-code-btn').html(text);
            }, 1000);
        }
    );

});



$('#login-btn').on('click', function() {
    // 验证表单
    if(checkPhone() == false) {
        return;
    }

    if ($('#code').val() == '') {
        $.toast("请输入验证码", "forbidden");
        return;
    }

    // 验证验证码
    $.get(
        parameter.messageCodeGet,
        {phone: $('#phone').val(), radom_code: $('#code').val()},
        function(data) {
            switch (JSON.parse(data).status) {
                case 0:
                    $.toast("验证码错误", "forbidden");
                    break;
                case 1:
                    $.toast("登录成功");
                    login($('#phone').val());
                    window.location.href = './activity';
                    break;
            }
            // console.log(JSON.parse(data).status);
        }
    );


});
</script>
