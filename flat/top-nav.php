<style media="screen">
.top-nav {
    position: relative;
    z-index: 9;
}
.top-nav .navbar {
    border-radius: 0;
    background: #024ea1;
}
.top-nav .navbar-brand {
    background: url(./img/top-nav2.jpg) 50% 50% no-repeat;
    background-color: #024ea1;
    background-size: contain;
}
.top-nav .navbar-brand:hover, .top-nav .navbar-brand:focus {
    background-color: #024ea1;
}
.top-nav .navbar-toggle::before {
    color: #fff;
}
.top-nav .big-screen li a {
    color: #fff;
    font-weight: normal;
}
@media (max-width: 768px) {
    .top-nav .navbar {
        background: #024ea1;
    }
    .top-nav .navbar-brand {
        width: 73%;
    }
    .top-nav .navbar-brand {
        background-position: 0 50%;
    }
    #navbar-collapse-02 {
        background: #f1f1f1;
    }
}
@media (min-width: 768px) {
    .top-nav .navbar-header {
        width: 30%;
    }
    .top-nav .navbar-brand {
        width: 100%;
    }
    .navbar-collapse.collapse {

    }
    #navbar-collapse-02 {
        display: none!important;
    }
}
</style>
<div class="top-nav">
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-02">
                <span class="sr-only">Toggle navigation</span>
            </button>
            <a class="navbar-brand" href="javascript:"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-01">
            <ul class="big-screen nav navbar-nav pull-right">
                <li class="user"><a href="javascript:">用户:<span></span></a></li>
                <!-- <li class="help"><a href="javascript:">帮助</a></li> -->
                <li class="logout"><a href="javascript:">退出登录</a></li>
            </ul>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-02">
            <ul class="small-screen nav navbar-nav">
                <li class="text-center active"><a href="form">修改资料</a></li>
                <li class="text-center"><a href="activity?type=isscore">面试成绩</a></li>
                <li class="text-center"><a href="activity">所有活动</a></li>
                <li class="text-center"><a href="activity?type=isbaoming">已报名活动</a></li>
                <li style="margin: 0 10%;border-bottom: 1px solid #ddd;"></li>
                <li class="user text-center"><a href="javascript:">用户:<span></span></a></li>
                <!-- <li class="help text-center"><a href="javascript:">帮助</a></li> -->
                <li class="logout text-center"><a href="javascript:">退出登录</a></li>
            </ul>
        </div>
    </nav>
</div>
<script type="text/javascript">
    $('.top-nav .user span').html(getCookie('user'));
    $('.top-nav .logout').on('click', function() {
        setCookie('user', '', -1);
        $.toast('退出成功');
        window.location.href = './login';
    });
</script>
