parameter = {
    // 验证码发送
    messageCodeSend: 'http://118.178.234.34:30003/student/sendMsg',
    // 输入验证码验证
    messageCodeGet: 'http://118.178.234.34:30003/student/checkMsg',
    // 提交报名信息表单
    userInfoSend: 'http://118.178.234.34:30003/student/addSign',
    // 获取区内学校列表
    getSchoolList: 'http://118.178.234.34:30003/student/getList?item_id=6',
    // 获取表单信息 表单回填
    userInfoGet: 'http://118.178.234.34:30003/student/getRowSign',
    // 获取活动列表
    getActivityList: 'http://118.178.234.34:30003/student/getAvticyList',
    // 获取活动详情
    getActivityInfo: 'http://118.178.234.34:30003/student/getAvticyInfo',
    // 活动报名
    signActivity: 'http://118.178.234.34:30003/student/addSignAvticy',
}
