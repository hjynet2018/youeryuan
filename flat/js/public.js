// 是否手机端
function isMobile() {
    var ua = navigator.userAgent;
    var ipad = ua.match(/(iPad).*OS\s([\d_]+)/),
    isIphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/),
    isAndroid = ua.match(/(Android)\s+([\d.]+)/),
    isMo = isIphone || isAndroid;
    if(isMo) {
        return true;
    } else {
        return false;
    }
}

// 是否微信
function isWeixin() {
    var ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == 'micromessenger') {
        // 是微信
        return true;
    } else {
        // 不是微信
        return false;
    }
}

// 获取地址栏查询参数的值
function getHerfParameter(key) {
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)", "i");
    var r = decodeURI(window.location.search).substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}

// 设置cookie
// days 为-1时即可清楚cookie
function setCookie(name, value, days) {
	var d = new Date;
	d.setTime(d.getTime() + 24*60*60*1000*days);
	window.document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}

// 获取cookie
function getCookie(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
　　 return (arr=document.cookie.match(reg))?unescape(arr[2]):null;
}

// 登录
function login(user) {
    setCookie('user', user, 1);
}

// 登出
function logout() {
    setCookie('user', '', -1);
}

// 检查登录
function checkLogin() {
    if (getCookie('user') != null) {
        // 有登录
        return true;
    } else {
        // 没登录
        return false;
    }
}

// 设置二维码
function setQr(obj) {
    $(obj).qrcode({
    render: 'canvas', //table、canvas方式
    width: 150, //宽度
    height:150, //高度
    text: 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxb52637c9bfcfd0c8&redirect_uri=http://t.ihecha.me/index.php/StudentApi/handelCode&response_type=code&scope=snsapi_userinfo&state=' + getCookie('user') + '#wechat_redirect'
    });
}

// 日期
Object.assign(Date.prototype, {
	format(format) {
		var o = {
			'M+': this.getMonth() + 1, //month
			'd+': this.getDate(), //day
			'h+': this.getHours(), //hour
			'm+': this.getMinutes(), //minute
			's+': this.getSeconds(), //second
			'q+': Math.floor((this.getMonth() + 3) / 3), //quarter
			'S': this.getMilliseconds() //millisecond
		}

		if(/(y+)/.test(format)) {
			format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
		}

		for(var k in o) {
			if(new RegExp('(' + k + ')').test(format)) {
				format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
			}
		}
		return format;
	}
});
