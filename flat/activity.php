<?php include('./header.php') ?>
<style media="screen">
.activity {
    border: 1px solid #024ea1;
    border-top-width: 8px;
    border-radius: 4px;
    padding: 5px;
    margin: 0 0 15px;
    background: #fff;
    box-shadow: 0 2px 2px rgba(82, 82, 82, 0.15);
    font-size: 14px;
}

.activity.not {
    border-color: #BDC3C7;
}

.activity.now {
    border-color: #1ABC9C;
}

.activity.sign {

}

.activity .title {
    text-align: center;
    font-size: 15px;
    padding: 20px 0 30px 0;
    min-height: 98px;
}
.activity .time {

}
.activity .status {
    text-align: right;
}
.activity .btn-container {
    padding: 10px 0;
    text-align: center;
}
.activity .btn-container .btn {
    width: 90%;
}
.activity .btn-container.detail {

}
.activity .btn-container.info {
    display: none;
}
.activity.sign .btn-container.info {
    display: block;
}

.activity .btn-container.checkin {

}
.activity.sign .btn-container.checkin {
    display: none;
}
</style>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 pln prn">
            <?php include('./top-nav.php') ?>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-2 hidden-xs" style="margin-left: -15px;">
            <?php include('./side-nav.php') ?>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-10">
            <div class="activity-list row fixed-block">
                <!-- <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="row activity sign">
                        <div class="title col-xs-12">
                            模板1
                        </div>
                        <div class="time col-xs-6">
                            2018-03-01<br>2018-03-19
                        </div>
                        <div class="status col-xs-6">
                            <br>状态:<span>已报名</span>
                        </div>
                        <div class="btn-container detail col-xs-6">
                            <button type="button" class="btn btn-sm btn-info">活动详情</button>
                        </div>
                        <div class="btn-container info col-xs-6">
                            <button type="button" class="btn btn-sm btn-info">修改信息</button>
                        </div>
                        <div class="btn-container checkin col-xs-6">
                            <button type="button" class="btn btn-sm btn-info" disabled>我要报名</button>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="row activity now">
                        <div class="title col-xs-12">
                            模板2
                        </div>
                        <div class="time col-xs-6">
                            2018-03-01<br>2018-03-19
                        </div>
                        <div class="status col-xs-6">
                            <br>状态:<span>可报名</span>
                        </div>
                        <div class="btn-container detail col-xs-6">
                            <button type="button" class="btn btn-sm btn-info">活动详情</button>
                        </div>
                        <div class="btn-container info col-xs-6">
                            <button type="button" class="btn btn-sm btn-info">修改信息</button>
                        </div>
                        <div class="btn-container checkin col-xs-6">
                            <button type="button" class="btn btn-sm btn-info" disabled>我要报名</button>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="row activity not">
                        <div class="title col-xs-12">
                            模板3
                        </div>
                        <div class="time col-xs-6">
                            2018-03-01<br>2018-03-19
                        </div>
                        <div class="status col-xs-6">
                            <br>状态:<span>未开放</span>
                        </div>
                        <div class="btn-container detail col-xs-6">
                            <button type="button" class="btn btn-sm btn-info">活动详情</button>
                        </div>
                        <div class="btn-container info col-xs-6">
                            <button type="button" class="btn btn-sm btn-info">修改信息</button>
                        </div>
                        <div class="btn-container checkin col-xs-6">
                            <button type="button" class="btn btn-sm btn-info" disabled>我要报名</button>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>


</div>
</body>
<?php include('./footer.php') ?>
<script>
    $('.activity.now .checkin .btn').removeAttr('disabled');

    $.post(parameter.userInfoGet, {add_phone: getCookie('user')}, function(data) {
        var id = JSON.parse(data).student_id;
        getActivityList(id);
    });

    // 获取活动列表
    // status 0-可以报名 1-不显示 2-未开放
    // is_baoming 0-未报名 1-已报名
    function getActivityList(studentId) {
        $.post(parameter.getActivityList, {'student_id': studentId}, function(data) {
            var arrActivity = JSON.parse(data);
            var html = '';
            for (var i in arrActivity) {
                switch (getHerfParameter('type')) {
                    case 'isbaoming':
                        if (arrActivity[i].is_baoming != 1) {
                            continue;
                        }
                        break;
                    case 'isscore':
                        if (arrActivity[i].is_baoming != 1) {
                            continue;
                        }
                        var isScore = true;
                        break;
                }
                switch (arrActivity[i].status) {
                    case 0:
                        if (arrActivity[i].is_baoming == 0) {
                            var status = 'now';
                            var statusText = '可报名';
                        } else if (arrActivity[i].is_baoming == 1) {
                            var status = 'sign';
                            var statusText = '已报名';
                        }
                        break;
                    case 1:
                        continue;
                        break;
                    case 2:
                        var status = 'not';
                        var statusText = '未开放';
                        break;
                }
                var startTime = (new Date(arrActivity[i].date_start * 1000)).format('yyyy-MM-dd');
                var endTime = (new Date(arrActivity[i].date_end * 1000)).format('yyyy-MM-dd');
                html += '\
                    <div class="col-xs-12 col-sm-6 col-md-4">\
                        <div class="row activity ' + status + '">\
                            <div class="title col-xs-12">\
                                ' + arrActivity[i].title + '\
                            </div>\
                            <div class="time col-xs-6">\
                                ' + startTime + '<br>' + endTime + '\
                            </div>\
                            <div class="status col-xs-6">\
                                <br>状态:<span>' + statusText + '</span>\
                            </div>\
                            <div class="btn-container detail col-xs-6">\
                                <a href="./text?type=activity&id=' + arrActivity[i].activity_id + '" class="btn btn-sm btn-info">活动详情</a>\
                            </div>\
                            <div class="btn-container info col-xs-6 ' + (isScore ? 'hide' : '') + '">\
                                <a href="./form" class="btn btn-sm btn-info">修改信息</a>\
                            </div>\
                            <div class="btn-container info col-xs-6 ' + (isScore ? '' : 'hide') + '">\
                                <a href="./text" class="btn btn-sm btn-info">查看成绩</a>\
                            </div>\
                            <div class="btn-container checkin col-xs-6">\
                                <a href="./form?actid=' + arrActivity[i].activity_id + '" class="btn btn-sm btn-info" ' + (status == 'not' ? 'disabled' : '') + '>我要报名</a>\
                            </div>\
                        </div>\
                    </div>\
                ';
            }
            $('.activity-list').append(html);
        });
    }

</script>
