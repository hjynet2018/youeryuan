<?php include('./header.php') ?>
<link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<style media="screen">
.form-container {
    margin: 0px auto;
}
</style>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 pln prn">
            <?php include('./top-nav.php') ?>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-2 hidden-xs" style="margin-left: -15px;">
            <?php include('./side-nav.php') ?>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-10">
            <div class="form-container">
                <?php include('./form-content.php') ?>
            </div>
        </div>
    </div>


</div>
</body>
<?php include('./footer.php') ?>
