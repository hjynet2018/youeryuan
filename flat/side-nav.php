<style media="screen">
    .side-nav {
        position: fixed;
        top: 0;
        left: 0;
        padding: 53px 20px 0;
        height: 100vh;
        background: #fff;
        box-shadow:  1px 0 4px rgba(0, 0, 0, 0.2);
        max-width: 200px;
    }
    .side-nav dl {
        text-align: center;
    }
    .side-nav dl dt {
        padding: 15px 15px 0;
        margin-bottom: 10px;
        border-bottom: 1px solid #ccc;
        font-weight: bold;
        color: #000;
    }
    .side-nav dl dd {

    }
    .side-nav dl dd a {
        display: inline-block;
        width: 100%;
        line-height: 40px;
        color: #666;
    }
    .side-nav dl dd a:hover {
        font-weight: bold;
        color: #0036AD;
    }
</style>
<div class="side-nav">
    <dl>
        <dt>我的报名</dt>
        <dd><a href="form">修改资料</a></dd>
        <dd><a href="activity?type=isscore">面试成绩</a></dd>
        <dt>活动报名</dt>
        <dd><a href="activity">所有活动</a></dd>
        <dd><a href="activity?type=isbaoming">已报名活动</a></dd>
    </dl>
</div>
